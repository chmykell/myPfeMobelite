const express = require('express')
const usersController = require('../controllers/usersController')

const userRouter = express.Router()

userRouter.get('/', usersController.getUsers)
userRouter.get('/:id', usersController.getUserById)
userRouter.get('/bySlackId/:slackId', usersController.getUserBySlackId)
userRouter.get('/byEmail/:email', usersController.getUserByEmail)
userRouter.route('/insert').post(usersController.addUser) 
userRouter.route('/').post(usersController.addSlackUsers)
userRouter.route('/setPassword/:id').put(usersController.setUserPassword)
userRouter.route('/edit/:id').put(usersController.updateUser)

userRouter.route('/:id').delete(usersController.deleteUser) 
userRouter.route('/').delete(usersController.deleteAllUsers) 



module.exports = userRouter