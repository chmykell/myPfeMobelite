const express = require('express')
const taskController = require('../controllers/taskController')

const taskRouter = express.Router()

taskRouter.get('/', taskController.getTasks)
taskRouter.get('/:id', taskController.getTaskById)
// return list of tasks by userID
taskRouter.get('/byUser/:id', taskController.getTasksByUserId)
// returns total  logged hours tasks by projects
taskRouter.get('/hoursByProject/:id', taskController.getLoggedHoursByProjectByUser)
//return logged hours by project in certain date
taskRouter.get('/hoursByProject/:startDate/:endDate', taskController.getLoggedHoursByProjectByDate)
taskRouter.route('/').post(taskController.addTask) 
taskRouter.route('/:id').delete(taskController.deleteTask) 
taskRouter.route('/edit/:id').put(taskController.updateTask) 



module.exports = taskRouter 