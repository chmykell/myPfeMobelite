const express = require('express')
const projectController = require('../controllers/projectController')

const projectRouter = express.Router()
//returns llist of projects
projectRouter.get('/:startDate?/:endDate?', projectController.getProjects)
//return total logged hours in all projects
projectRouter.get('/loggedHours/:startDate?/:endDate?', projectController.getProjectsTotalLoggedHours)
//return project by its id
projectRouter.get('/:id', projectController.getProjectById)
//return list of projects created by followed user 
projectRouter.get('/byUser/:id', projectController.getProjectsByUserId)
//adding new project
projectRouter.route('/').post(projectController.addProject) 
// merge project into another
projectRouter.route('/:projectToMerge/:projectToMergeInto').post(projectController.mergeProjectIntoAnother) 

//updating project
projectRouter.route('/').put(projectController.updateProject) 


module.exports = projectRouter 