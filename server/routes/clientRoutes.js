const express = require('express')
const clientController = require('../controllers/clientController')

const clientRouter = express.Router()

clientRouter.get('/', clientController.getClients)
clientRouter.get('/:id', clientController.getClientById)
clientRouter.route('/').post(clientController.addClient) 
clientRouter.route('/:id').delete(clientController.deleteClient) 
clientRouter.route('/edit/:id').put(clientController.updateClient) 


module.exports = clientRouter 