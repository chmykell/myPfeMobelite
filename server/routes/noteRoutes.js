const express = require('express')
const noteController = require('../controllers/noteController')

const noteRouter = express.Router()

noteRouter.get('/:startDate?/:endDate?', noteController.getNotes)
noteRouter.get('/:id', noteController.getNoteById)
noteRouter.get('/byUser/:id', noteController.getNotesByUserId)
noteRouter.get('/byProject/:id', noteController.getNotesByProjectId)





module.exports = noteRouter 