var bodyParser = require('body-parser');
var Project = require('../../models/Project');


// get all users from dataBase
// const getProjects = (req, res) =>{
   
//     Project.find( function(err, projects) {
//         if (err)
//          res.send(err);
//         res.json(projects);
//        });    
     
// }
const getProjects = (req, res) =>{
    let startDate = req.params.startDate
    let endDate = req.params.endDate
    if(startDate != null && endDate != null){
        Project.find({createdAt: {"$gte": new Date(startDate), "$lt": new Date(endDate)}}).populate('client').populate('createdBy').populate('notes').exec(function(err, projects) {
            if (err)
             res.send(err);
            res.json(projects);
           }); 

    }
    else{
        Project.find().populate('client').populate('createdBy').populate('notes').exec(function(err, projects) {
            if (err)
             res.send(err);
            res.json(projects);
           });    
    }

   
     
}
//collecting total logged hours on all projects
const getProjectsTotalLoggedHours = (req, res) =>{
    let startDate = req.params.startDate
    let endDate = req.params.endDate
    console.log('type of startdate', typeof(startDate))
  
                  
                   Project.aggregate([
                    { 
                        $match: 
                        {
                            createdAt: {"$gte": new Date(startDate), "$lt": new Date(endDate)}
                        }
                     },
                    {
                        $group:
                            {   _id: null,
                                totalLoggedHours: { $sum: '$loggedHours' },
                                timeOffHours: { $sum: '$timeOffHours' } }
                    }]).exec(function(err, hours) {
                        if (err)
                        res.send(err);
                    res.json(hours);
                    //console.log('hours', hours)
                   console.log('else enddate != null', endDate,startDate);
                    
                    });    
        }
         
//}

// get one project by id 
const getProjectById = (req, res) =>{
    let id = req.params.id
    Project.findById(id).populate('client').populate('createdBy').populate('notes').exec(function(err, project) {
        if (err)
        res.send(err);
       res.json(project); 
});
}

const addProject = (req, res) =>{ 
    //create a new user instance
    var project = new Project();
    project.createdBy = req.body.user;
    project.createdAt = new Date();
    //load user instance with data from body
    project.projectName = req.body.projectName;
    project.client = req.body.client;
    project.type = req.body.type;
    project.estimatedDuration = req.body.estimatedDuration;
    project.loggedHours = 0;
    project.timeOffHours = 0;
    project.color = req.body.color;
    


    project.save(function (err) {
        if (err)
            res.send(err);
        res.send('Project successfully added!');
    });
}
// get notes by user id
const getProjectsByUserId = (req, res) =>{
    let id = req.params.id
    Project.$where('this.createdBy._id == this.id').populate('client').populate('notes').exec(function(err, projects) {
        if (err)
        res.send(err);
       res.json(projects); 
});
}

const mergeProjectIntoAnother = (req, res) =>{
    let projectToMerge = req.params.projectToMerge;
    let projectToMergeInto = req.params.projectToMergeInto;
    console.log('projectToMerge',projectToMerge);
    console.log('projectToMergeInto',projectToMergeInto);

   /// still on progresssssss not a good solution yet


}


const updateProject = (req, res) =>{
    // let id = req.params.id;
    // const project = {
    //     createdBy : getConnectedUser(),
    //     //load user instance with data from body
    //     projectName: req.body.projectName,
    //     client: req.body.client,
    //     type: req.body.type,
    //     estimatedDuration: req.body.estimatedDuration,
    //     upDatedAt: new Date()
        
    // };

    
 
    // Client.update({_id: id}, client, {upsert: true} ,function(err, results) {
    //     if (err)
    //       res.send(err);
    //     res.send('client successfully updated!');
    //     console.log('update controller func', results);
    // });
}





module.exports = {
    getProjects,
    getProjectById,
    addProject,
    updateProject,
    getProjectsTotalLoggedHours,
    getProjectsByUserId,
    mergeProjectIntoAnother
}



