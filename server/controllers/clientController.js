var bodyParser = require('body-parser');
var Client = require('../../models/Client');


// get all users from dataBase
const getClients = (req, res) =>{
   
    Client.find( function(err, clients) {
        if (err)
         res.send(err);
        res.json(clients);
       });    
     
}

const getClientById = (req, res) =>{
    let id = req.params.id
    Client.findById(id, function(err, client) {
        if (err)
        res.send(err);
       res.json(client); 
});
}

const updateClient = (req, res) =>{
    let id = req.params.id
    // var client = new Client();
   
    // client.firstName = req.body.firstName;
    // client.lastName = req.body.lastName;
    // client.email = req.body.email;
    // client.phoneNumber = req.body.phoneNumber;
    // client.country = req.body.country;
    // client.createdAt = new Date();
    const client = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        country: req.body.country,
        createdAt: new Date()
    };

    
 
    Client.update({_id: id}, client, {upsert: true} ,function(err, results) {
        if (err)
          res.send(err);
        res.send('client successfully updated!');
        console.log('update controller func', results);
    });
}








const addClient = (req, res) =>{
    //create a new user instance
    var client = new Client();
   
    client.firstName = req.body.firstName;
    client.lastName = req.body.lastName;
    client.email = req.body.email;
    client.phoneNumber = req.body.phoneNumber;
    client.country = req.body.country;
    client.createdAt = new Date();
    
    

    client.save(function (err) {
        if (err)
            res.send(err);
        res.send('client successfully added!');
    });
}

const deleteClient = (req, res) =>{
    var id = req.params.id;
    Client.find({_id: id}).remove().exec(function(err, client) {
        if(err)
         res.send(err)
        res.send('Client successfully deleted!');
       })

}




module.exports = {
    getClients,
    getClientById,
    addClient,
    deleteClient,
    updateClient
}



