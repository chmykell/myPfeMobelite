
var bodyParser = require('body-parser');
var User = require('../../models/User');


//get all users from dataBase
const getUsers = (req, res) =>{
   
    User.find( function(err, users) {
        if (err)
         res.send(err);
        res.json(users);
       });
        
       
    
}

const getUserById = (req, res) =>{
    let id = req.params.id
    User.findById(id, function(err, user) {
        if (err)
        res.send(err);
       res.json(user); 
});
}
const getUserBySlackId = (req, res) =>{
    let slackId = req.params.slackId
    User.findOne({slack_id: slackId},function(err, user) {
        if (err)
        res.send(err);
       res.json(user); 
});
}
const getUserByEmail = (req, res) =>{
    let email = req.params.email
    User.findOne({email: email},function(err, user) {
        if (err)
        res.send(err);
       res.json(user); 
});
}
const addUser = (req, res) =>{
    //create a new user instance
    var user = new User();

    //load user instance with data from body
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.email = req.body.email;
    user.role = req.body.role;
    user.createdAt = new Date();

    user.save(function (err) {
        if (err)
            res.send(err);
        res.send('User successfully added!');
    });
}
const addSlackUsers = (req, res) =>{
    //create a new user instance
    var user = new User();

    //load user instance with data from body
    // user._id = req.body.slack_id;
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.real_name = req.body.real_name;
    user.email = req.body.email;
    user.role = req.body.role;
    user.createdAt = new Date();
    user.slack_id = req.body.slack_id;
    user.team_id = req.body.team_id;
    user.is_admin = req.body.is_admin;
    user.image_192= req.body.image_192;
    user.phone= req.body.phone;
    user.skype = req.body.skype;
    user.title = req.body.title;
    user.tz = req.body.tz;
    user.tz_label = req.body.tz_label;
    user.tz_offset = req.body.tz_offset;

    user.save(function (err) {
        if (err)
            res.send(err);
        res.send('User successfully added!');
    });
}



const deleteUser = (req, res) =>{
    var id = req.params.id;
    User.find({_id: id}).remove().exec(function(err, user) {
        if(err)
         res.send(err)
        res.send('User successfully deleted!');
       })
}
const deleteAllUsers = (req, res) =>{
    // var id = req.params.id;
    User.remove().exec(function(err, user) {
        if(err)
         res.send(err)
        res.send('Users successfully deleted!');
       })
}
const setUserPassword = (req, res) =>{
    let paswd = req.body.paswd
    let token = req.body.token
    let id = req.params.id
  
    User.update({_id: id},{password : paswd , token: token},function(err, results) {
        if (err)
          res.send(err);
        res.send('user password successfully set!');
        
    });
}
const updateUser = (req, res) =>{
    let id = req.params.id
   
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let phone = req.body.phone;
    let skype = req.body.skype;
    let role = req.body.role;
    let image = req.body.image;
    let imageIsUpdated = req.body.imageIsUpdated;
    

    
 
    User.update({_id: id}, {first_name: first_name, last_name: last_name, phone: phone, skype: skype, role: role, image_192: image, imageIsUpdated: imageIsUpdated}, {upsert: true} ,function(err, results) {
        if (err)
          res.send(err);
        res.send('user successfully updated!');
        console.log('update controller func', results);
    });
}







module.exports = {
    getUsers,
    getUserById,
    addUser,
    deleteUser,
    addSlackUsers,
    deleteAllUsers,
    getUserBySlackId,
    setUserPassword,
    getUserByEmail,
    updateUser
}



