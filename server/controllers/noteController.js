var bodyParser = require('body-parser');

var Note = require('../../models/Note');


// get all users from dataBase
// const getProjects = (req, res) =>{
   
//     Project.find( function(err, projects) {
//         if (err)
//          res.send(err);
//         res.json(projects);
//        });    
     
// }
const getNotes = (req, res) =>{ 
    let startDate = req.params.startDate
    let endDate  = req.params.endDate
    
    if (req.params.startDate != null) {
    // Note.aggregate([
        
    //     {
    //         $match:   {  createdAt: {"$gte": new Date(startDate), "$lte": new Date(endDate)} }  
    //     }
    //     ]).exec(function (err, notes) {
    //         if (err)
    //             res.send(err);
    //         res.json(notes);
    //     }); 
        Note.find({ createdAt: {"$gte": new Date(startDate), "$lte": new Date(endDate)} }).populate('project').populate('user').exec(function(err, notes) {
            if (err)
             res.send(err);
            res.json(notes);
           });
    }
    else{
         Note.find().populate('project').populate('user').exec(function(err, notes) {
        if (err)
         res.send(err);
        res.json(notes);
       });
    }
     
}

// get one tas by id 
const getNoteById = (req, res) =>{
    let id = req.params.id
    Note.findById(id).populate('project').populate('user').exec(function(err, note) {
        if (err)
        res.send(err);
       res.json(note); 
});
}
// get notes by user id
const getNotesByUserId = (req, res) =>{
    let id = req.params.id
    Note.find({user: id}).populate('project').populate('user').exec(function(err, notes) {
        if (err)
        res.send(err);
       res.json(notes); 
});
}
// get notes by project id
const getNotesByProjectId = (req, res) =>{
    let id = req.params.id
    Note.find({project: id}).populate('project').populate('user').exec(function(err, notes) {
        if (err)
        res.send(err);
       res.json(notes); 
});
}


module.exports = {
    getNotes,
    getNoteById,
    getNotesByUserId,
    getNotesByProjectId
}



