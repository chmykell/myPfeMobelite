var bodyParser = require('body-parser');
var Task = require('../../models/Task');
var Note = require('../../models/Note');
var Project = require('../../models/Project');
const getTasks = (req, res) =>{
   
    Task.find().populate('project').populate('notes').populate('createdBy').exec(function(err, tasks) {
        if (err)
         res.send(err);
        res.json(tasks);
       });    
     
}

// get one tas by id 
const getTaskById = (req, res) =>{
    let id = req.params.id
    Task.findById(id).populate('project').populate('notes').populate('createdBy').exec(function(err, task) {
        if (err)
        res.send(err);
       res.json(task); 
});
}

//adding new task
const addTask = (req, res) =>{
    //create a new user instance
    var task = new Task();
    task.project = req.body.project;
    task.createdBy = req.body.user;
    task.createdAt = new Date();
    task.hours = req.body.hours;
    console.log('req body hours', req.body.hours);
    //load user instance with data from body
    task.startDate = req.body.startDate;
  
  

    // create a note object to store in project after
    var note = new Note();
    note.user = req.body.user;
    note.createdAt = new Date();
    note.noteMessage = req.body.notes;
    note.project = req.body.project;
    //note created 
    var noteId = '';
    note.save(function (err, doc) {
        if (err)
            res.send(err);
        console.log('note created', doc._id);
        noteId = doc._id;
    });
    Project.update({ _id: req.body.project },
        {
          $inc: { loggedHours: req.body.hours }
         
        }).exec(function(err, response) {
            if (err)
            res.send(err);
           console.log('res update', response) ;
    });

    setTimeout(function afterOneSeconds() {
        task.notes = noteId;
        
        task.save(function (err) {
            if (err)
                res.send(err);
            res.send('task successfully added!');
        });
      }, 1000)
   
    
   
}
// get tasks by user id
const getTasksByUserId = (req, res) =>{
    let id = req.params.id
    Task.find({createdBy : id}).populate('project').populate('notes').populate('user').exec(function(err, notes) {
        if (err)
        res.send(err);
       res.json(notes); 
});
}
// get total logged hours on each project by user id
var ObjectID = require("mongodb").ObjectID;
const getLoggedHoursByProjectByUser = (req, res) =>{
    let id = req.params.id
    Task.aggregate([
        
        {
            $match: { "createdBy":  {"$in": [ ObjectID(id) ] }}
        },
        {
            $group:
                {
                    _id: '$project',
                    totalLoggedHours: { $sum: '$hours' }
                   
                }
        }
        ]).exec(function (err, notes) {
            if (err)
                res.send(err);
            res.json(notes);
        });
}

const getLoggedHoursByProjectByDate = (req, res) =>{
    let projectId = req.params.id
    let startDate = req.params.startDate
    let endDate  = req.params.endDate 
    Task.aggregate([
        
        {
            $match:   {  createdAt: {"$gte": new Date(startDate), "$lte": new Date(endDate)} }  
        },
       
        {
            $group:
                {
                    _id: '$project',
                    totalLoggedHours: { $sum: '$hours' }
                   
                }
        }
        ]).exec(function (err, notes) {
            if (err)
                res.send(err);
            res.json(notes);
        });
}







const updateTask = (req, res) =>{
    // let id = req.params.id;
    // const task = {
    //     createdBy : getConnectedUser(),
    //     //load user instance with data from body
    //     projectName: req.body.projectName,
    //     client: req.body.client,
    //     type: req.body.type,
    //     estimatedDuration: req.body.estimatedDuration,
    //     upDatedAt: new Date()
        
    // };

    
 
    // Task.update({_id: id}, client, {upsert: true} ,function(err, results) {
    //     if (err)
    //       res.send(err);
    //     res.send('client successfully updated!');
    //     console.log('update controller func', results);
    // });
}
const deleteTask = (req, res) =>{
    var id = req.params.id;
    Task.find({_id: id}).remove().exec(function(err, task) {
        if(err)
         res.send(err)
        res.send('Task successfully deleted!');
       })
}




module.exports = {
    getTasks,
    getTaskById,
    addTask,
    updateTask,
    deleteTask,
    getTasksByUserId,
    getLoggedHoursByProjectByUser,
    getLoggedHoursByProjectByDate
}



