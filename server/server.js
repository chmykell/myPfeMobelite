var express = require('express');
var router = require('./routes/routes.js')
var userRouter = require('./routes/usersRoutes')
var projectRouter = require('./routes/projectRoutes')
var clientRouter = require('./routes/clientRoutes')
var taskRouter = require('./routes/taskRoutes')
var noteRouter = require('./routes/noteRoutes')
var path = require('path');
var bodyParser = require('body-parser');

var app = express();
var mongoose = require('mongoose');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../client'));
app.use(express.static(path.join(__dirname, '../client')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
mongoose.connect('mongodb://localhost/teamBoard');
//mongoose.connect('mongodb://sambak:92036987nN@ds241869.mlab.com:41869/mypfeapp');

app.use('/', router);
app.use('/users', userRouter);
app.use('/projects', projectRouter);
app.use('/clients', clientRouter);
app.use('/tasks', taskRouter);
app.use('/notes', noteRouter);
module.exports=app;


