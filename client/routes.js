import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from './App';
import LoginContainer from './containers/loginContainer/loginContainer';
import SignUpContainer from './containers/signUpContainer/signUpContainer';
import HomePage from './pages/HomePage/homePage';
import BlankPage from './pages/blankPage/blankPage';
import AllProjectsPage from './pages/allProjectsPage/allProjectsPage';
import TeamPage from './pages/teamPage/teamPage';
import TasksPage from './pages/tasksPage/tasksPage';
import MyTasksPage from './pages/myTasksPage/myTasksPage';
import ClientsPage from './pages/clientsPage/clientsPage';
import TeamOverviewPage from './pages/teamOverviewPage/teamOverviewPage';
import ProjectsOverviewPage from './pages/ProjectOverviewPage/ProjectOverviewPage';
import SlackPage from './pages/slackPage/slackPage';
import TeamSettingsPage from './pages/teamSettings/teamSettingsPage';
import MySettingsPage from './pages/mySettingsPage/mySettingsPage';
import SingleProfilePage from './pages/SingleProfilePage/singleProfilePage';
import SingleProjectDetailsPage from './pages/singleProjectDetailsPage/singleProjectDetailsPage';
import PageNotFoundComponent from './components/PageNotFoundComponent/PageNotFoundComponent'

 const Routes = () => (
    <Switch>
      <Route exact path='/' component={App} />
      <Route  path='/login' component={LoginContainer} />
      <Route  path='/signup' component={SignUpContainer} />
      <Route  path='/home' component={HomePage} />
      <Route  path='/blank' component={BlankPage} />
      <Route  path='/allProjects' component={AllProjectsPage} />
      <Route  path='/team' component={TeamPage} />
      <Route  path='/allTasks' component={TasksPage} />
      <Route  path='/myTasks' component={MyTasksPage} />
      <Route  path='/allClients' component={ClientsPage} />
      <Route  path='/teamOverview' component={TeamOverviewPage} />
      <Route  path='/projectsOverview' component={ProjectsOverviewPage} />
      <Route  path='/slack' component={SlackPage} />
      <Route  path='/settings' component={TeamSettingsPage} />
      <Route  path='/mySettings' component={MySettingsPage} />
      <Route path="/profile/:id" component={SingleProfilePage} />
      <Route path="/project/:id" component={SingleProjectDetailsPage} />
      <Route path="*" component={PageNotFoundComponent} />
      
      
    </Switch>
);
export default Routes;