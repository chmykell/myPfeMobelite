import React from 'react';
import AsideContainer from '../../containers/asideContainer/asideContainer';
import TopNaviguation from '../../components/topNaviguation/topNaviguation';
import { withRouter } from 'react-router';




import TeamSettingsContainer from '../../containers/teamSettingsContainer/teamSettingsContainer';


 


class TeamSettingsPage extends React.Component{

   componentWillMount(){
       if(localStorage.getItem('token') == null){
        this.props.history.push('/login');
       }
   }

    render(){
       
            return(
                <div className="container body" style={{ margin: '0', maxWidth: '100%' }}>
                    <div className="main_container" style={{ margin: '0' }}>
                    
                    {/* <AsideContainer />
                    <TopNaviguation/> */}
                    <TeamSettingsContainer />
                    </div>
                    </div>  
                                
            )
        
       
    }
}
export default withRouter(TeamSettingsPage);