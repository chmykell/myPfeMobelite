import React from 'react';
import AsideContainer from '../../containers/asideContainer/asideContainer';
import TopNaviguation from '../../components/topNaviguation/topNaviguation';
import { withRouter } from 'react-router';




import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/google-code-prettify/bin/prettify.min.css';
import '../../assets/build/css/custom.min.css';


import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css';
import '../../assets/jqvmap/dist/jqvmap.min.css';
import '../../assets/bootstrap-daterangepicker/daterangepicker.css';


 


class BlankPage extends React.Component{

   
    componentWillMount(){
        if(localStorage.getItem('token') == null){
         this.props.history.push('/login');
        }
    }
    render(){
        return(
            
            <div className="container body" style={{ margin: '0', maxWidth: '100%' }}>
                <div className="main_container" style={{ margin: '0' }}>
                
                
                {/* <AsideContainer />
                <TopNaviguation/> */}
                <div className="right_col" role="main">
                            <div className="">
                                <div className="page-title">
                                    <div className="title_left">
                                        <h3>Plain Page</h3>
                                    </div>

                                    <div className="title_right">
                                        <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                            <div className="input-group">
                                                <input type="text" className="form-control" placeholder="Search for..." />
                                                <span className="input-group-btn">
                                                    <button className="btn btn-default" type="button">Go!</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="clearfix"></div>

                                <div className="row">
                                    <div className="col-md-12 col-sm-12 col-xs-12">
                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Plain Page</h2>
                                                <ul className="nav navbar-right panel_toolbox">
                                                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li className="dropdown">
                                                        <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                        <ul className="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                Add content to the page ...
                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </div>
                
            
                            
        )
    }
}
export default withRouter(BlankPage);