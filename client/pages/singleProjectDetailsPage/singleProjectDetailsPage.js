import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import SingleProjectDetailsContainer from '../../containers/SingleProjectDetailsContainer/SingleProjectDetailsContainer'

// import '../../assets/bootstrap/dist/css/bootstrap.min.css';
// import '../../assets/font-awesome/css/font-awesome.min.css';
// import '../../assets/nprogress/nprogress.css';
// import '../../assets/google-code-prettify/bin/prettify.min.css';
// import '../../assets/build/css/custom.min.css';


// import '../../assets/iCheck/skins/flat/green.css';
// import '../../assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css';
// import '../../assets/jqvmap/dist/jqvmap.min.css';
// import '../../assets/bootstrap-daterangepicker/daterangepicker.css';


class SingleProjectDetailsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedProject: ''
        }

       

    }
    

    componentWillMount() {

        if (localStorage.getItem('token') == null) {
            this.props.history.push('/login');
        }

     


        // console.log('parameters props users', this.props.match.params.id );

        // this.setState({selectedUser: this.props.history.location.user})

    }

    render() {
        return (

            <div className="container body" style={{ margin: '0', maxWidth: '100%' }}>
                <div className="main_container" style={{ margin: '0' }}>


                    {/* <AsideContainer />
                    <TopNaviguation /> */}
                    <SingleProjectDetailsContainer projectId={this.props.match.params.id} />
                </div>
            </div>



        )
    }
}
export default withRouter(SingleProjectDetailsPage);