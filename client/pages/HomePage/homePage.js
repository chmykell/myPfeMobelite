import React from 'react';
import AsideContainer from '../../containers/asideContainer/asideContainer';
import TopNaviguation from '../../components/topNaviguation/topNaviguation';
import { withRouter } from 'react-router';
import axios from 'axios';

import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/build/css/custom.min.css';
import { LoadScript } from '../../common/loadScript';
import AllProjectStatsPie from '../../components/allProjectStatsPie/allProjectStatsPie';
import AllProjectStatsGraphe from '../../components/AllProjectStatsGraphe/AllProjectStatsGraphe';
import TopGeneralStatsContainer from '../../containers/topGeneralStatsContainer/topGeneralStatsContainer';
import TopClientsContainer from '../../containers/topClientsContainer/topClientsContainer';


// const getUserToken = (req, res) =>{
//     let code = req.params.code
//     axios.get('https://slack.com/api/oauth.access?code='+ code)
//       .then(function(response) {
//         console.log('response is ', response);
//       });

//     res.json(
//         response
//     )
// }



class HomePage extends React.Component {

    // //Charger les script js nécessaires au bon fonctionnnemnt de cette page.
    // componentDidMount () {
    //     var loadScript = function(src) {
    //         var tag = document.createElement('script');
    //         tag.async = false;
    //         tag.src = src;
    //         document.body.appendChild(tag);
    //       }
    //     loadScript('../../assets/jquery/dist/jquery.min.js');
    //     loadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
    //     loadScript('../../assets/fastclick/lib/fastclick.js');
    //     loadScript('../../assets/nprogress/nprogress.js');
    //     loadScript('../../assets/build/js/custom.min.js');
    // }
    constructor(props) {
        super(props);
        this.state = (
            {
                name: '',
                email: '',
                avatar: '',
                startDate: '1992-05-21T14:08:01+01:00',
                endDate: '2992-05-21T14:08:01+01:00',
                selectedDate: {start: '1992-05-21T14:08:01+01:00', end: '2992-05-21T14:08:01+01:00'}
            }
        );
    }


    componentWillMount(){
        if(localStorage.getItem('token') == null){
         this.props.history.push('/login');
        }
        

    }
 
 
    render() {
        return (


            <div className="main_container" style={{ margin: '0' }}>

                {/* <AsideContainer />
                <TopNaviguation /> */}

                <div className="right_col" role="main">
                    <div className="">
                        <div className="page-title">
                            <TopGeneralStatsContainer />

                           
                        </div>

                        <div className="clearfix"></div>

                      {/* <AllProjectStatsPie selectedDate={this.state.selectedDate} type="bar"/> */}
                        <div className="col-md-4 col-sm-4 col-xs-12">
                          <AllProjectStatsGraphe  selectedDate={this.state.selectedDate} type="Radar" />
                         </div>
                        <div className="col-md-4 col-sm-4 col-xs-12">     
                           <AllProjectStatsGraphe  selectedDate={this.state.selectedDate} type="Pie" />
                         </div>
                        
                              <TopClientsContainer />
                        
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <AllProjectStatsGraphe  selectedDate={this.state.selectedDate} type="Bar" />
                    </div>
                     
                    </div>
                </div>

            </div>





        )
    }
}
export default withRouter(HomePage);