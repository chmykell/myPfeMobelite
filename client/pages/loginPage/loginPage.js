import React from 'react';
import { Redirect } from 'react-router-dom';
import { withRouter } from 'react-router';
import axios from 'axios';
import "../../assets/bootstrap/dist/css/bootstrap.min.css";
import "../../assets/font-awesome/css/font-awesome.min.css";
import "../../assets/nprogress/nprogress.css";
import "../../assets/animate.css/animate.min.css";
import "../../assets/build/css/custom.min.css";
var passwordHash = require('password-hash');



class LoginPage extends React.Component{
  
  constructor(props){
    super(props);
    this.state= {
      email: '',
      password: '',
      loading: false,
      searchedUser: '',
      passwordStatus: '',
      emailStatus: ''

    };

    this.logIn = this.logIn.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.getUserByCredentials =this.getUserByCredentials.bind(this);
  }
  componentWillMount(){
    //testing if the user is already logged in from local storage
    if(localStorage.getItem("token") != null){
      this.props.history.push('/home');
    }
    console.log("route", this.props.location.pathname)
    if(this.props.location.pathname == "/login"){
      //unmount aside and top naviguation menu
    }
  }


 handleTextChange(e){
  if (e.target.name == "email") {
    this.setState({
        email: e.target.value
    });
}

 } 
 handlePasswordChange(e){
  if (e.target.name == "password") {
    this.setState({
        password: e.target.value
    });
}
 }
 getUserByCredentials(ev) {
  // getting user from id passed as parameter
  axios.get('/users/byEmail/' + this.state.email)
    .then(function (response) {
      ev.setState({ searchedUser: response.data });
      console.log('response from byEmail api', response.data);

    });
}

  logIn(){
   // console.log("history", this.props);
  //  this.props.history.push('/home');
   console.log('state', this.state);
   this.setState({loading: true});
   this.getUserByCredentials(this);
   setTimeout(() => {
     console.log('state user', this.state.searchedUser)
 
    //test if email is true or not 
    if(this.state.searchedUser != null){
      if(passwordHash.verify(this.state.password, this.state.searchedUser.password)){
        localStorage.setItem("token",this.state.searchedUser.token);
        // this.props.history.push('/');
        window.location.reload(); 
       }else{
         this.setState({passwordStatus: 'red',
                        emailStatus: 'green'});
  
       }
    }else{
      this.setState({emailStatus: 'red'})
    }

   }, 600);
   

  }

    render(){
        return(
          <div className="login">
          <div>
      
            <div className="login_wrapper">
              <div className="animate form login_form">
                <section className="login_content">
               
                  <form>
                   

                    <h1>Login Form</h1>

                    <div>
                      <input type="text" className="form-control" placeholder="Email" required="" name="email" onChange={this.handleTextChange} style={{color: this.state.emailStatus}}/>
                    </div>
                    <div>
                      <input type="password" className="form-control" placeholder="Password" required="" name="password" onChange={this.handlePasswordChange} style={{color: this.state.passwordStatus}}/>
                    </div>
                    <div>
                    {/* {this.state.loading ? <i className="fa fa-spinner fa-spin fa-3x"></i>: <a className="btn btn-default submit"  onClick={this.logIn}>Log in</a>} */}
                    <a className="btn btn-default submit"  onClick={this.logIn}>Log in</a>
                      {/* <a className="reset_pass" href="#">Lost your password?</a> */}
                      <a href="https://slack.com/oauth/authorize?scope=identity.basic,identity.email,identity.avatar&client_id=12121543780.331637189936"><img alt="Connectez-vous avec Slack" height="40" width="172" src="https://platform.slack-edge.com/img/sign_in_with_slack.png" srcSet="https://platform.slack-edge.com/img/sign_in_with_slack.png 1x, https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x" /></a>
                    </div>
                    
      
                    <div className="clearfix"></div>
                    
                    <div className="separator">
                      <p className="change_link">New to site?
                        <a href="#signup"  onClick={ this.sendToSignUp }> Create Account </a>
                      </p>
      
                      <div className="clearfix"></div>
                      <br />
                     
                      <div>
                      <img src="../../imgs/logo-mobelite.png"/>
                        <h1> Team Board</h1>
                        <p>©2018 All Rights Reserved. </p>
                      </div>
                    </div>
                  </form>
                </section>
              </div>
      
              
            </div>
          </div>
        </div>
       
        )
    }
}
export default withRouter(LoginPage);