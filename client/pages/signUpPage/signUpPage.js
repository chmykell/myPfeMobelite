import React from 'react';
import { Redirect } from 'react-router';
import { withRouter } from 'react-router'
import "../../assets/bootstrap/dist/css/bootstrap.min.css";
import "../../assets/font-awesome/css/font-awesome.min.css";
import "../../assets/nprogress/nprogress.css";
import "../../assets/animate.css/animate.min.css";
import "../../assets/build/css/custom.min.css";


class SignUpPage extends React.Component{
    
  componentWillMount(){
    if(localStorage.getItem('token') == null){
     this.props.history.push('/login');
    }
}

    render(){
        return(
          <div className="login">
          <div>
            
            <a className="hiddenanchor" id="signin"></a>
      
            <div className="login_wrapper">
            <div id="register">
          <section className="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" className="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" className="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" className="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a className="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div className="clearfix"></div>

              <div className="separator">
                <p className="change_link">Already a member ?
                  <a href="#login" className="to_register"> Log in </a>
                </p>

                <div className="clearfix"></div>
                <br />

                <div>
                <img src="../../imgs/logo-mobelite.png"/>
                        <h1> Team Board</h1>
                  <p>©2018 All Rights Reserved</p>
                </div>
              </div>
            </form>
          </section>
        </div>
        </div>
        </div>
        </div>
        )
    }


}
export default withRouter(SignUpPage);