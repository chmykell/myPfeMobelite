import React from 'react';
import AsideContainer from '../../containers/asideContainer/asideContainer';
import TopNaviguation from '../../components/topNaviguation/topNaviguation';
import { withRouter } from 'react-router';




import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/google-code-prettify/bin/prettify.min.css';
import '../../assets/build/css/custom.min.css';


import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css';
import '../../assets/jqvmap/dist/jqvmap.min.css';
import '../../assets/bootstrap-daterangepicker/daterangepicker.css';
import MyTasksContainer from '../../containers/myTasksContainer/myTasksContainer';


 


class MyTasksPage extends React.Component{

    componentWillMount(){
        if(localStorage.getItem('token') == null){
         this.props.history.push('/login');
        }
    }


    render(){
        return(
            
            <div className="container body" style={{ margin: '0', maxWidth: '100%' }}>
                <div className="main_container" style={{ margin: '0' }}>
                
                
                {/* <AsideContainer />
                <TopNaviguation/> */}
                <MyTasksContainer />
                </div>
                </div>
                
            
                            
        )
    }
}
export default withRouter(MyTasksPage);