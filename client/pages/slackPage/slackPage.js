import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';

class SlackPage extends React.Component{
    constructor() {
        super();
        this.state = {
           userData: []
        };

        this.getUrlVars = this.getUrlVars.bind(this);
        this.getUserCredentials = this.getUserCredentials.bind(this);
    }
    
   componentDidMount(){
    //    console.log('parameter!!!!!!!!!!!', window.location.href);
    //    var url_string = window.location.href; 
    //    var url = new URL(url_string);
    //    var code = url.searchParams.get("code");
    this.getUrlVars();
    this.getUserCredentials(this);
    setTimeout(() => {
        if(this.state.userData != null){
            window.location.reload(); 
           this.props.history.push('/home');
        //  window.location.reload();
            
        }
    }, 100);

      
   }
    getUserCredentials(ev) {
        var code = this.getUrlVars()["code"];
        axios.get(' https://slack.com/api/oauth.access?client_id=12121543780.331637189936&client_secret=3492940825438a6afc233bbd7627ba81&code='+code)
        .then(function (response) {
            console.log('response is', response);
             ev.setState({ userData: response.data });
             localStorage.setItem("token",JSON.stringify(response.data))
           console.log('userData json:',JSON.stringify(response.data))
    
        }); 
    }
    getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


    render(){
        return(
         
            <i className="fa fa-spinner fa-spin fa-3x" style={{ marginTop: '20%',marginLeft: '40%', marginBottom: '50%', fontSize: '200px'}}></i>
            
                            
        )
    }
}
export default withRouter(SlackPage);