import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import Routes from './routes';
import registerServiceWorker from '../src/registerServiceWorker';
import AsideContainer from './containers/asideContainer/asideContainer';
import TopNaviguation from './components/topNaviguation/topNaviguation';
import LoginContainer from './containers/loginContainer/loginContainer';
var renderCondition ;

if(localStorage.getItem('token')!= null){
 renderCondition = true;
}else{
renderCondition = false;
}
ReactDOM.render(
  <HashRouter>
    <div className="container body" style={{ margin: '0', maxWidth: '100%' }}>
      <div className="main_container" style={{ margin: '0' }}>
       {renderCondition &&  <AsideContainer />} 
       {renderCondition &&  <TopNaviguation />} 
       {!renderCondition && <LoginContainer/> }
       <Routes />
       {/* {renderCondition &&  <Routes />}  */}
      </div>
    </div>
  </HashRouter>, document.getElementById('root')
);
registerServiceWorker();
