import React from 'react';
import { Link } from 'react-router';
import Modal from 'react-modal';
import { LoadScript } from '../../common/loadScript';
import DatePicker from 'react-datepicker';
import {FormatDate} from '../../common/formatIsoStringDateToDate';
import moment from 'moment';


var querystring = require('querystring');
import axios from 'axios';
import 'react-datepicker/dist/react-datepicker.css';

import '../../assets/select2/dist/css/select2.min.css';
import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';

    
   
     
 




const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};


class MyTasksContainer extends React.Component {

    constructor() {
        super();


        this.state = {
            modalIsOpen: false,
            project: '',
            hours: '',
            notes: '',
            messageFromServer: '',
            tasksData: [],
            projectsData: [],
            startDate: moment(),
            userId: '',
            dateObjToStore: moment().toObject()
        };


        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleAddTaskClick = this.handleAddTaskClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getTodayDate = this.getTodayDate.bind(this);
        this.getTasks = this.getTasks.bind(this);
        this.getProjects = this.getProjects.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.getConnectedUser = this.getConnectedUser.bind(this);


    }

    componentDidMount() {
        // LoadScript('../../assets/jquery/dist/jquery.min.js');
        // LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
        // LoadScript('../../assets/fastclick/lib/fastclick.js');
        // LoadScript('../../assets/nprogress/nprogress.js');
        LoadScript('../../assets/iCheck/icheck.min.js');
        LoadScript('../../assets/datatables.net/js/jquery.dataTables.min.js');
        LoadScript('../../assets/datatables.net-bs/js/dataTables.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/dataTables.buttons.min.js');
        LoadScript('../../assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.flash.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.html5.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.print.min.js');
        LoadScript('../../assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
        LoadScript('../../assets/datatables.net-keytable/js/dataTables.keyTable.min.js');
        LoadScript('../../assets/datatables.net-responsive/js/dataTables.responsive.min.js');
        LoadScript('../../assets/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        LoadScript('../../assets/datatables.net-scroller/js/dataTables.scroller.min.js');
        LoadScript('../../assets/jszip/dist/jszip.min.js');
        LoadScript('../../assets/pdfmake/build/pdfmake.min.js');
        LoadScript('../../assets/pdfmake/build/vfs_fonts.js');
        LoadScript('../../assets/build/js/custom.min.js');
        
  
    

    }
    componentWillMount(){
        this.getProjects(this);
        
        this.getConnectedUser(this);
        setTimeout(() => {
            this.getTasks(this);
        }, 100);
        
        // console.log('parse data', new Date("2018-03-21T22:48:31.000Z"));
        // console.log('parse data', new Date("2018-03-21T22:48:31.000Z").getMonth());
    }
   
    // getting system date
    getTodayDate() {
        var currentdate = new Date();
        var todayDate = " " + currentdate.getDate() + "/" +
            (currentdate.getMonth() + 1) + "/" +
            currentdate.getFullYear();

        return todayDate;
    }
    //getting system date and time
    getCurrentTime() {
        var currentdate = new Date();
        var createdAt = {
            year: currentdate.getFullYear(),
            mounth: (currentdate.getMonth() + 1),
            day: currentdate.getDate(),
            hours: currentdate.getHours(),
            minutes: currentdate.getMinutes(),
            seconds: currentdate.getSeconds()
        };
        //  console.log('my new object !!', createdAt);
        //  this.setState({createdAt: createdAt });
        return createdAt;

    }
    getTasks(ev) {
        axios.get('/tasks/byUser/'+this.state.userId)
            .then(function (response) {
                console.log('response', response);
                ev.setState({ tasksData: response.data });
             //   console.log('clientDclientsDataata')


            });
    }
    getProjects(ev) {
        axios.get('/projects')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ projectsData: response.data });
             //   console.log('clientDclientsDataata')

            });
    }
    openModal() {
        this.setState({ modalIsOpen: true });

    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }

    handleTextChange(e) {
        if (e.target.name == "hours") {
            this.setState({
                hours: e.target.value
            });
        }
        if (e.target.name == "notes") {
            this.setState({
                notes: e.target.value
            });
        }


    }

    handleSelectChange(e) {
        // this.setState({ project: e.target.value });
        if (e.target.name == "project") {
            for (let node of e.target.children) {
                if (node.value === e.target.value) {
                    console.log("data idddd::::",node.getAttribute('data-id'));
                    var dataId = node.getAttribute('data-id');
                    console.log('inside dataid', dataId);
                    this.setState({
                        project: dataId
                    });
                    return;
                   
                }
            }
          
        }
    }
    handleDateChange(date) {
       
         var selectedDate = date;
        this.setState({
          startDate: selectedDate,
          dateObjToStore: selectedDate.toObject()
        });
        console.log('selected ', selectedDate)
        
      }

    handleAddTaskClick(e) {
        e.preventDefault();
        this.addTask(this);


    }
    getConnectedUser(ev){
     
        var token =JSON.parse(localStorage.getItem('token'));
        console.log('token', token);
        var connectedUserId = token.user.id;
        console.log('connectedUserId', connectedUserId);
    
        axios.get('/users/bySlackId/'+connectedUserId)
        .then(function (response) {
            console.log('id of user by', response);
            ev.setState({ userId: response.data._id });
           console.log('userID state',response.data._id);
    
        });
        
    }
    
    addTask(e) {
        var outside = this;
        var toSend= this.state.startDate.toISOString();
        
        axios.post('/tasks',
          querystring.stringify({
            project: e.state.project,
            startDate: toSend,
            user: e.state.userId,
            hours: e.state.hours,
            notes: e.state.notes
          }), {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            }
          }).then(function(response) {
            console.log('dateObjtoStore ', e.state.dateObjToStore)
          e.setState({
            messageFromServer: response.data
          });
          outside.getTasks(e);
        });
        

        console.log('my state', this.state);
        console.log('my state', this.state.dateObjToStore);
        //  console.log('my state date', this.state.startDate._d.toObject());

        // // console.log('current time', this.getCurrentTime())


    }
    // //takes isoString format date and change it to readable date
    // formatDate(dateIsoString){
    //     var customDate = new Date(dateIsoString);
    //     console.log('custom date', customDate);
    //     var formattedDate = " " + customDate.getFullYear() + "/" +
    //         (customDate.getMonth() + 1) + "/" +
    //         customDate.getDate()+"@"+ customDate.getHours()+":"+customDate.getMinutes();
    //     return formattedDate;
    //     console.log('custom date', formattedDate);
    // }


    render() {

        return (
            <div className="right_col" role="main">
            <div className="">
                <div className="page-title">
                    <div className="title_left">
                        <h3>Tasks List</h3>
                    </div>

                    <div className="title_right">
                        <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search for..." />
                                <span className="input-group-btn">
                                    <button className="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="clearfix"></div>
                <div className="row">
                    <div className="x_content">
                                            {/* add project modal begins */}
                                            {this.state.messageFromServer == '' ?
                                                <Modal
                                                    isOpen={this.state.modalIsOpen}
                                                    onAfterOpen={this.afterOpenModal}
                                                    onRequestClose={this.closeModal}
                                                    style={customStyles}
                                                    contentLabel="Example Modal"
                                                    ariaHideApp={false}
                                                >


                                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                                
                                                    <h5 style={{ textAlign: 'center' }}><i className="fa fa-calendar"></i> Today :{this.getTodayDate()}</h5>
                                                    <div className="ln_solid"></div>
                                                    <form onSubmit={this.handleAddTaskClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">
                                                        <div className="form-group">
                                                            <label htmlFor="project" className="control-label col-md-3 col-sm-3 col-xs-12" >Project <span className="required">*</span></label>
                                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                                <select className="form-control" id="project" required="required" onChange={this.handleSelectChange} name="project">
                                                                <option>Choose project</option>
                                                                {
                                                                    this.state.projectsData.map((project) => {

                                                                        return (<option key={project._id} data-id={project._id}>{project.projectName}</option>)

                                                                    })
                                                                }
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div className="form-group">
                                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name">Hours <span className="required">*</span>
                                                            </label>
                                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                                <input type="number" id="hours" name="hours" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name">Notes <span className="required">*</span>
                                                            </label>
                                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                                <input type="text" id="notes" name="notes" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name">Select Date (default today)
                                                            </label>
                                                            <div className="col-md-6 col-sm-6 col-xs-10" style={{marginTop: '4px', marginLeft: '2px'}}>

                                                                <DatePicker
                                                                    filterDate={this.isWeekday}
                                                                    todayButton={"Today"}
                                                                    selected={this.state.startDate}
                                                                    onChange={this.handleDateChange}
                                                                />


                                                            </div>
                                                        </div>
                                                        <div className="ln_solid"></div>
                                                        <div className="form-group" >
                                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3" style={{ marginLeft: '1%' }}>
                                                               <a href="#allProjects"> <button type="button" className="btn btn-round btn-default">
                                                                    <i className="fa fa-plus"></i>
                                                                    Add new Project
                                                                   </button>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div className="ln_solid"></div>
                                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3" >
                                                                <button className="btn btn-primary" type="reset">Reset</button>
                                                                <button type="submit" className="btn btn-success">Submit</button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </Modal>
                                                :
                                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong> {this.state.messageFromServer}</strong>
                                                </div>
                                            }
                                            {/* add project modal ends */}
                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                    <div className="x_panel">
                                                        <div className="x_title">
                                                        <button type="button" className="btn btn-info btn-sm" onClick={this.openModal}>Add task </button>
                                                            <ul className="nav navbar-right panel_toolbox">
                                                                <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                                                </li>
                                                                <li className="dropdown">
                                                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                                    <ul className="dropdown-menu" role="menu">
                                                                        <li><a href="#">Settings 1</a>
                                                                        </li>
                                                                        <li><a href="#">Settings 2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li><a className="close-link"><i className="fa fa-close"></i></a>
                                                                </li>
                                                            </ul>
                                                            <div className="clearfix"></div>
                                                        </div>
                                                        <div className="x_content">
                                                            {/* <p className="text-muted font-13 m-b-30">
                                                                DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                                            </p> */}
                                                            <table id="datatable" className="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Project</th>
                                                        <th>Hours</th>
                                                        <th>Task date</th>
                                                        <th>Notes</th>
                                                        <th>CreatedAt</th>
                                                        <th>Actions</th>
                                                    </tr> 
                                                </thead>


                                                <tbody>
                                                    {
                                                        this.state.tasksData.map((task) => {
                                                            
                                                            return (<tr key={task._id} style={{textAlign: 'center'}}>
                                                                <td>{task.project.projectName}<span style={{float: 'right', width:'15px', height:'15px', background: task.project.color}}></span></td>
                                                                <td>{task.hours}</td>
                                                                <td>{FormatDate(task.startDate)}</td>
                                                                <td>{task.notes.noteMessage}</td>
                                                                <td>{FormatDate(task.createdAt)}</td>
                                                                <td><i className="fa fa-align-justify" ></i></td>

                                                            </tr>)
                                                            
                                                        })
                                                        
                                                      
                                                    }
                                                    
                                                     
                                                </tbody>
                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        
                   
        )
    }
}
export default MyTasksContainer;