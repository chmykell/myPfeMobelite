import React from 'react';
import { SwatchesPicker } from 'react-color';
import Modal from 'react-modal';
var querystring = require('querystring');
import axios from 'axios';
import { withRouter, Redirect } from 'react-router';
import '../../assets/select2/dist/css/select2.min.css';
import { LoadScript } from '../../common/loadScript';
import {FormatDate}  from '../../common/formatIsoStringDateToDate'
import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';
import MenuOptions from '../../components/menuOptions/menuOptions';

const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};
// Merge modal custom styles 
const mergeCustomStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid #2a3f54',
        borderRadius: 26
    }
};


class AllProjectsContainer extends React.Component {


    constructor() {
        super();

        this.state = {
            modalIsOpen: false,
            mergeModalIsOpen: false,
            projectName: '',
            client: '',
            type: '',
            estimatedDuration: '',
            messageFromServer: '',
            mergeMessageFromServer: '',
            clientsData: [],
            projectsData: [],
            pickerIsOpen: false,
            userId: '',
            selectedProjectToMerge: '',
            sortedProjects: []
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openMergeModal = this.openMergeModal.bind(this);
        this.afterOpenMergeModal = this.afterOpenMergeModal.bind(this);
        this.closeMergeModal = this.closeMergeModal.bind(this);
        this.handleAddProjectClick = this.handleAddProjectClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getClients = this.getClients.bind(this);
        this.getProjects = this.getProjects.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleClickColorPick = this.handleClickColorPick.bind(this);
        this.getConnectedUser = this.getConnectedUser.bind(this);
        this.sortProjectsToMergeInto = this.sortProjectsToMergeInto.bind(this);
        // this.getUserById = this.getUserById.bind(this);
        // this.getClientById = this.getClientById.bind(this);
    }
    componentDidMount() {
        LoadScript('../../assets/jquery/dist/jquery.min.js');
        LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
        LoadScript('../../assets/fastclick/lib/fastclick.js');
        // LoadScript('../../assets/nprogress/nprogress.js');
        LoadScript('../../assets/iCheck/icheck.min.js');
        LoadScript('../../assets/datatables.net/js/jquery.dataTables.min.js');
        LoadScript('../../assets/datatables.net-bs/js/dataTables.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/dataTables.buttons.min.js');
        LoadScript('../../assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.flash.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.html5.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.print.min.js');
        LoadScript('../../assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
        LoadScript('../../assets/datatables.net-keytable/js/dataTables.keyTable.min.js');
        LoadScript('../../assets/datatables.net-responsive/js/dataTables.responsive.min.js');
        LoadScript('../../assets/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        LoadScript('../../assets/datatables.net-scroller/js/dataTables.scroller.min.js');
        LoadScript('../../assets/jszip/dist/jszip.min.js');
        LoadScript('../../assets/pdfmake/build/pdfmake.min.js');
        LoadScript('../../assets/pdfmake/build/vfs_fonts.js');
        // LoadScript('../../assets/build/js/custom.min.js');
        console.log('staattatattezraezre', this.state);
        
    

    }
    componentWillMount() {
        this.getClients(this);
        this.getProjects(this);
        this.getConnectedUser(this);
    }
    
    //get list of clients for modal to use
    getClients(ev) {
        axios.get('/clients')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ clientsData: response.data });
                console.log('clientDclientsDataata')

            });
    }
    //get client by id
    // getClientById(_id){
    //     let objj = this;
    //     let clientData= '';
    //     axios.get('/clients/'+_id)
    //     .then(function (response) {
    //         console.log('clientbyid', (response.data.firstName+" "+ response.data.lastName));
    //         // ev.setState({ clientsData: response.data });
    //         objj.clientData = (response.data.firstName+" "+ response.data.lastName);
    //         return objj.clientData
    //     });
       
        
    // }
    // //get user by id
    // getUserById(_id){
    //     let obj = this;
    //     let userData= '';
    //     axios.get('/users/'+_id)
    //     .then(function (response) {
    //         //console.log('response', response);
    //         obj.userData = (response.data.firstName+" "+ response.data.lastName);
    //         return userData;
            
    //     });
        
    // }

     //get list of projects for datatable to use
     getProjects(ev) {
        axios.get('/projects')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ projectsData: response.data });
             //   console.log('clientDclientsDataata')

            });
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    openMergeModal(project) {
        this.setState({ mergeModalIsOpen: true,
                        selectedProjectToMerge: project});
        console.log('selected project to merge', project);
        this.sortProjectsToMergeInto(project);
    }

    afterOpenMergeModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeMergeModal() {
        this.setState({
            mergeModalIsOpen: false,
            mergeMessageFromServer: ''
        });
        this.getProjects(this);
    }
    handleTextChange(e) {
        if (e.target.name == "projectName") {
            this.setState({
                projectName: e.target.value
            });
        }
        if (e.target.name == "estimatedDuration") {
            this.setState({
                estimatedDuration: e.target.value
            });
        }


    }

    handleSelectChange(e) {
        if (e.target.name == "client") {
            for (let node of e.target.children) {
                if (node.value === e.target.value) {
                    console.log("data idddd::::",node.getAttribute('data-id'));
                    var dataId = node.getAttribute('data-id');
                    console.log('inside dataid', dataId);
                    this.setState({
                        client: dataId
                    });
                    return;
                   
                }
            }
          
        }
        if(e.target.name == "type") {
            console.log('yhandel')
            this.setState({
        type: e.target.value
    });

}
    }
    handleColorChange(color){
        console.log('color',color )
        this.setState({
            color: color.hex,
            pickerIsOpen: false
        });
        
    }


handleAddProjectClick(e) {
    e.preventDefault();
    this.insertNewProject(this);

}
handleClickColorPick(){
    this.setState({ pickerIsOpen: true})
    console.log('state pick click', this.state)
}
 getConnectedUser(ev){
     
    var token =JSON.parse(localStorage.getItem('token'));
    console.log('token', token);
    var connectedUserId = token.user.id;
    console.log('connectedUserId', connectedUserId);

    axios.get('/users/bySlackId/'+connectedUserId)
    .then(function (response) {
        console.log('id of user by', response);
        ev.setState({ userId: response.data._id });
       console.log('userID state',response.data._id);

    });
    
}
openProjectDetails(project) {
    // console.log('user is', slackUser);
    this.props.history.push('/project/' + project._id);
    // console.log('user is', this.props.history);

}
// sort project table to delete the selected project from 
// the list so you can only merge project in other projects not the same one
// immutable object.assign
sortProjectsToMergeInto(selectedProject){
    // let listProjects = Object.assign({}, this.state.projectsData);
    let listProjects = this.state.projectsData;
    console.log('llist projects ', listProjects);
  setTimeout(() => {
    for (const i in listProjects) {
        //console.log('project', listProjects[i])
        if(listProjects[i]._id == selectedProject._id){
            console.log('true');
             listProjects.splice(i, 1);
            
            console.log('typeof', listProjects)
            console.log('result final', listProjects);
            this.setState({sortedProjects: listProjects});
            return;
        }
     }
  }, 100); 


}

insertNewProject(e) {
    let outside = this;
    axios.post('/projects',
        querystring.stringify({
            projectName: e.state.projectName,
            user: e.state.userId,
            client: e.state.client,
            type: e.state.type,
            estimatedDuration: e.state.estimatedDuration,
            color: e.state.color
        }), {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).then(function (response) {
            e.setState({
                messageFromServer: response.data.message
            });
            outside.getProjects(e);
        });
      console.log('clicked project will be added soon', this.state);
      
}


render() {
    return (
        <div className="right_col" role="main">
        <div className="">
            <div className="page-title">
                <div className="title_left">
                    <h3>Clients List</h3>
                </div>

                <div className="title_right">
                    <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Search for..." />
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="clearfix"></div>

            <div className="row">
                <div className="x_content">


                                        {/* add project modal begins */}
                                        {this.state.messageFromServer == '' ?
                                            <Modal
                                                isOpen={this.state.modalIsOpen}
                                                onAfterOpen={this.afterOpenModal}
                                                onRequestClose={this.closeModal}
                                                style={customStyles}
                                                contentLabel="Example Modal"
                                                ariaHideApp={false}
                                            >


                                                <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                                <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Add New project</h2>
                                                <div className="ln_solid"></div>
                                                <form onSubmit={this.handleAddProjectClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">

                                                    <div className="form-group">
                                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="estimatedDuration" style={{textAlign: 'center'}}>Choose project color <span className="required">*</span>
                                                            </label>
                                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                                <button className="btn btn-primary" type="reset" name="color" required="required" className="form-control col-md-7 col-xs-12" onClick={this.handleClickColorPick} style={{color: this.state.color}}>Pick <div style={{float: 'right', width: '25px', height:'20px', background: this.state.color}}></div> </button>
                                                               {this.state.pickerIsOpen && <SwatchesPicker onChange={ this.handleColorChange } />} 
                                                            </div>
                                                        </div>
                                                    <div className="form-group">
                                                        <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="projectName" style={{textAlign: 'center'}}>Project Name <span className="required">*</span>
                                                        </label>
                                                        <div className="col-md-7 col-sm-6 col-xs-12">
                                                            <input type="text" id="projectName" name="projectName" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.projectName}/>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="client" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}}>Client <span className="required">*</span></label>
                                                        <div className="col-md-7 col-sm-6 col-xs-12">
                                                            <select className="form-control" id="client" required="required" onChange={this.handleSelectChange} name="client" >
                                                                <option>Choose client</option>
                                                                {
                                                                    this.state.clientsData.map((client) => {

                                                                        return (<option key={client._id} data-id={client._id}>{client.firstName} {client.lastName}</option>)

                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                    </div> 
                                                    <div className="form-group">
                                                        <label htmlFor="type" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}} >Type <span className="required">*</span></label>
                                                        <div className="col-md-7 col-sm-6 col-xs-12">
                                                            <select className="form-control" id="type" required="required" onChange={this.handleSelectChange} name="type" >
                                                                <option>Choose type</option>
                                                                <option>Web </option>
                                                                <option>Mobile </option>
                                                                <option>web && mobile </option>
                                                                <option>Hybride</option>
                                                                <option>Design</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="estimatedDuration" style={{textAlign: 'center'}}>Estimated Duration <span className="required">*</span>
                                                        </label>
                                                        <div className="col-md-7 col-sm-6 col-xs-12">
                                                            <input type="number" id="estimatedDuration" name="estimatedDuration" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.estimatedDuration} />
                                                        </div>
                                                    </div>
                                                    

                                                        

                                                    
                                                   




                                                    <div className="ln_solid"></div>
                                                    <div className="form-group" style={{ textAlign: 'center' }}>
                                                        <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                            <button className="btn btn-primary" type="reset">Reset</button>
                                                            <button type="submit" className="btn btn-success">Submit</button>
                                                        </div>
                                                    </div>


                                                </form>
                                            </Modal>
                                            :
                                            <div className="alert alert-success alert-dismissible fade in" role="alert">
                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <strong> {this.state.messageFromServer}</strong>
                                            </div>
                                        }
                                        {/* add project modal ends */}
                                        <div className="col-md-12 col-sm-12 col-xs-12">
                                                    <div className="x_panel">
                                                        <div className="x_title">
                                                        <button type="button" className="btn btn-info btn-sm" onClick={this.openModal}>Add Project </button>
                                                            <ul className="nav navbar-right panel_toolbox">
                                                                <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                                                </li>
                                                                <li className="dropdown">
                                                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                                    <ul className="dropdown-menu" role="menu">
                                                                        <li><a href="#">Settings 1</a>
                                                                        </li>
                                                                        <li><a href="#">Settings 2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li><a className="close-link"><i className="fa fa-close"></i></a>
                                                                </li>
                                                            </ul>
                                                            <div className="clearfix"></div>
                                                        </div>
                                                        <div className="x_content">
                                                            {/* <p className="text-muted font-13 m-b-30">
                                                                DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                                            </p> */}
                                                            <table id="datatable" className="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Project Name</th>
                                                        <th>Estimated Duration</th>
                                                        <th>CreatedBy</th>
                                                        <th>Client</th>
                                                        <th>CreatedAt</th>
                                                        <th>Status</th>
                                                        <th>Actions</th>
                                                    </tr> 
                                                </thead>


                                                <tbody>
                                                    {
                                                        this.state.projectsData.map((project) => {
                                                            
                                                            return (<tr key={project._id} style={{textAlign: 'center'}}>
                                                                <td onClick={() => this.openProjectDetails(project)}>{project.projectName}<div style={{background: project.color, width:'17px', height:'21px', margin: '0 auto', float: 'right'}}></div></td>
                                                                <td>{project.estimatedDuration} (hours/else)</td>
                                                                <td>{project.createdBy.first_name+" "+project.createdBy.last_name}</td>
                                                                <td>{project.client.firstName +" "+project.client.lastName}</td>
                                                                <td>{FormatDate(project.createdAt)}</td>
                                                                <td>{!project.status && 'OnProgress'}</td>
                                                                <td style={{ width: '36px', position: 'relative' }}>
                                                                        <div style={{ position: 'absolute', top: '3px' }}>
                                                                            <div style={{ position: 'relative', whiteSpace: 'nowrap' }}>
                                                                                <button className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" type="button"><i className="fa fa-bars" aria-hidden="true"></i></button>
                                                                                <ul className="dropdown-menu">
                                                                                    <li><a href="#" className="action_edit" rel="5628f4d3e4b099d4b9d9016d"><i className="fa fa-fw fa-pencil" style={{ marginRight: '10px' }} aria-hidden="true"></i>Edit</a></li>
                                                                                    <li><a onClick={() => this.openMergeModal(project)} className="action_edit" rel="5628f4d3e4b099d4b9d9016d"><i className="fa fa-fw fa-code-fork" style={{ marginRight: '10px' }}></i>Merge</a></li>
                                                                                    <li><a href="#" className="action_archive" rel="5628f4d3e4b099d4b9d9016d"><i className="fa fa-fw fa-archive" style={{ marginRight: '10px' }}></i>Archive</a></li>
                                                                                    <li><a href="#" className="action_delete" rel="5628f4d3e4b099d4b9d9016d"><i className="fa fa-fw fa-trash-o" style={{ marginRight: '10px' }}></i>Delete</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                </td>
                                                                  

                                                            </tr>)
                                                            
                                                        })
                                                        
                                                      
                                                    }
                                                     {/* Merge project modal begins */}
                                        {this.state.mergeMessageFromServer == '' ?
                                            <Modal
                                                isOpen={this.state.mergeModalIsOpen}
                                                onAfterOpen={this.afterOpenMergeModal}
                                                onRequestClose={this.closeMergeModal}
                                                style={mergeCustomStyles}
                                                contentLabel="Merge Modal"
                                                ariaHideApp={false}
                                            >


                                             <div style={{textAlign: 'center'}}>
                                                 <img src='../../imgs/fork.png' style={{textAlign: 'center', width: '16%', borderRadius:'50%',  border:'3px solid #2a3f54', background: '#2a3f54'}} />
                                                 <h1 style={{fontSize: '40px'}}>Merge projects</h1>
                                                 <p>Project <b>Framework</b> will be deleted, all it's records will be assigned to the project you
                                                     select from the list below.</p>
                                                     <div className="ln_solid"></div>
                                                        <label className="control-label col-md-12 col-sm-12 col-xs-12" htmlFor="selectedProjectToMerge" style={{textAlign: 'center', fontSize: '20px', marginTop: '2%'}}>Project To Merge:  
                                                        </label>
                                                        <div className="col-md-12 col-sm-12 col-xs-12" style={{textAlign: 'center'}}>
                                                            <input type="text" id="selectedProjectToMerge" name="selectedProjectToMerge" required="required" className="form-control col-md-12 col-xs-12" defaultValue={this.state.selectedProjectToMerge.projectName} disabled style={{textAlign: 'center'}}/>
                                                        </div>

                                                        <img src='../../imgs/down.png'  style={{textAlign: 'center', width: '10%', borderRadius:'50%',  border:'3px solid rgb(255, 255, 255)', background: 'rgb(236, 236, 236)' , marginTop: '2%'}}/>

                                                          <label htmlFor="mergeInto" className="control-label col-md-12 col-sm-12 col-xs-12" style={{textAlign: 'center'}}>Merge Into :<span className="required">*</span></label>
                                                        <div className="col-md-12 col-sm-12 col-xs-12">
                                                            <select className="form-control" id="mergeInto" required="required" onChange={this.handleSelectChange} name="mergeInto"  >
                                                                <option style={{textAlign: 'center'}}>Choose Project</option>
                                                                {
                                                                    this.state.sortedProjects.map((project) => {

                                                                        return (<option key={project._id} data-id={project._id} style={{textAlign: 'center'}}>{project.projectName}</option>)

                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12" style={{marginTop: '2%'}}>
                                                            <button class="btn btn-default" data-dismiss="modal" onClick={this.closeMergeModal}>Cancel</button>
                                                             <button class="btn btn-primary" disabled>Merge</button>
                                                       </div>
                                                    
                                             </div>
                                            </Modal>
                                            :
                                            <div className="alert alert-success alert-dismissible fade in" role="alert">
                                                <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <strong> {this.state.mergeMessageFromServer}</strong>
                                            </div>
                                        }
                                        {/* Merge project modal ends */}
                                                    
                                                     
                                                </tbody>
                                            </table>
                                            </div>
                                                    </div>
                                                </div>
                                                {/*list clients Ends */}


                            </div>
                        </div>
                    </div>
                </div>
    )
}
}
export default withRouter(AllProjectsContainer);