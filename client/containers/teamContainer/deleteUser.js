import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DeleteAlert from '../../components/deleteAlert/deleteAlert';
import Modal from 'react-modal';

const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};
class DeleteUser extends React.Component {
    constructor() {
        super();
        this.state = { id: '', modalIsOpen: false, messageFromServer: '' };
        this.onClick = this.onClick.bind(this);
        this.delete = this.delete.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.confirmClick = this.confirmClick.bind(this);
    }
    componentDidMount() {
        this.setState({
            id: this.props.user._id

        })
    }
    openModal() {
        this.setState({ modalIsOpen: true });
        console.log('id is: :::', this.state.id)
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }

    // componentWillReceiveProps(nextProps){
    //   this.setState({
    //     id: nextProps.expense._id,
    //     month:nextProps.expense.month,
    //     year:nextProps.expense.year
    //   })
    //   }
    onClick(e) {
        // this.delete(this);
       this.openModal();
    }
    confirmClick(e){
        e.preventDefault();
        this.delete(this);
    }
    
    delete(e) {
        
        axios.delete('/users/' + e.state.id)
            .then(function (response) {
                console.log('deleted', response);
                 
                e.setState({
                    messageFromServer: response.data,
                    modalIsOpen: false
                });
            });
            
            
    }
    render() {
        return (
            <div>
                <i className="fa fa-trash" onClick={this.onClick}> </i>
                <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Example Modal"
            ariaHideApp={false}
        >


            <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
            <h3>Do you want to delete this record ??</h3>

             <div className="ln_solid"></div>
             <form onSubmit={this.confirmClick} >
                <div className="form-group" style={{ textAlign: 'center' }}>
                    <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                        <button type="submit" className="btn btn-primary">Confirm</button>
                        <button onClick={this.closeModal} className="btn btn-success">Cancel</button>
                    </div>
                </div>
                </form>

        </Modal>
            </div>
              
        )
    }
}
export default DeleteUser;