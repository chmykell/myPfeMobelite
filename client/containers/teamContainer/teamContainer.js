import React from 'react';
import { withRouter, Redirect } from 'react-router';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';
import AccessDeniedPage from '../../components/AccessDeniedPage/AccessDeniedPage'
import Modal from 'react-modal';
import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate'
import DeleteUser from '../teamContainer/deleteUser';

import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';

var querystring = require('querystring');
var validator = require("email-validator");



const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};


class TeamContainer extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            first_name: 'sam',
            last_name: '',
            email: '',
            role: '',
            usersData: [],
            messageFromServer: '',
            dataBaseUser: '',
            user: ''
        };


        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleAddUserClick = this.handleAddUserClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getUsers = this.getUsers.bind(this);
        this.getSelectedUser = this.getSelectedUser.bind(this);

    }
    componentDidMount() {
        // LoadScript('../../assets/jquery/dist/jquery.min.js');
        // LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
        // LoadScript('../../assets/fastclick/lib/fastclick.js');
        // LoadScript('../../assets/nprogress/nprogress.js');
        LoadScript('../../assets/iCheck/icheck.min.js');
        LoadScript('../../assets/datatables.net/js/jquery.dataTables.min.js');
        LoadScript('../../assets/datatables.net-bs/js/dataTables.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/dataTables.buttons.min.js');
        LoadScript('../../assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.flash.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.html5.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.print.min.js');
        LoadScript('../../assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
        LoadScript('../../assets/datatables.net-keytable/js/dataTables.keyTable.min.js');
        LoadScript('../../assets/datatables.net-responsive/js/dataTables.responsive.min.js');
        LoadScript('../../assets/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        LoadScript('../../assets/datatables.net-scroller/js/dataTables.scroller.min.js');
        LoadScript('../../assets/jszip/dist/jszip.min.js');
        LoadScript('../../assets/pdfmake/build/pdfmake.min.js');
        LoadScript('../../assets/pdfmake/build/vfs_fonts.js');
        LoadScript('../../assets/build/js/custom.min.js');
        console.log('staattatattezraezre', this.state);



    }
    componentWillMount() {
        this.getUsers(this);
        var userObject = JSON.parse(localStorage.getItem('token'));
        if (userObject != null) {
          this.setState({ user: userObject.user });
        }
        setTimeout(() => {
          this.getSelectedUser(this);
        }, 100);

    }
    getUsers(ev) {
        axios.get('/users')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ usersData: response.data });
                console.log('clientDclientsDataata')

            });
    }
    getSelectedUser(ev) {
        // getting user from id passed as parameter
        axios.get('/users/bySlackId/' + this.state.user.id)
          .then(function (response) {
            ev.setState({ dataBaseUser: response.data });
            console.log('response from byslack users api', response.data);
    
          });
      }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }

    handleTextChange(e) {
        if (e.target.name == "first_name") {
            this.setState({
                first_name: e.target.value
            });
        }
        if (e.target.name == "last_name") {
            this.setState({
                last_name: e.target.value
            });
        }
        if (e.target.name == "email") {
            let validEmail = e.target.value;
            if (validator.validate(validEmail)) {
                this.setState({
                    email: e.target.value,
                    validEmail: true
                });
            } else {
                this.setState({
                    validEmail: false
                });
            }

        }

    }

    handleSelectChange(e) {

        this.setState({ role: e.target.value });
    }

    handleAddUserClick(e) {
        e.preventDefault();
        this.insertNewUser(this);
        // if(this.state.validEmail)
        // {
        // this.insertNewUser(this);
        // }
        // else{
        //     alert('email non valid !!!')
        // }
        // console.log('state=', this.state)
    }

    insertNewUser(e) {
        let outside = this;
        axios.post('/users/insert',
            querystring.stringify({
                first_name: e.state.first_name,
                last_name: e.state.last_name,
                email: e.state.email,
                role: e.state.role
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function (response) {
                e.setState({
                    messageFromServer: response.data
                });
                ouside.getUsers(e);
            });
    }

    // validation check to properly add users
    openUserProfile(slackUser) {
        console.log('user is', slackUser);
        this.props.history.push('/profile/' + slackUser.slack_id);
         console.log('user is', this.props);

    }


    render() {
        if(this.state.dataBaseUser.is_admin =="true"){
        return (
            <div className="right_col" role="main">
                <div className="">
                    <div className="page-title">
                        <div className="title_left">
                            <h3>Users List</h3>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>

                    <div className="row">





                        <div className="x_content">


                            {/* add project modal begins */}
                            {this.state.messageFromServer == '' ?
                                <Modal
                                    isOpen={this.state.modalIsOpen}
                                    onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                    ariaHideApp={false}
                                >


                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                    <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Add User</h2>
                                    <div className="ln_solid"></div>
                                    <form onSubmit={this.handleAddUserClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name" style={{textAlign: 'center'}}>First Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="first_name" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name" style={{textAlign: 'center'}}>Last Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="last_name" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="email" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}}>Email <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input id="email" className="form-control col-md-7 col-xs-12" type="text" name="email" required="required" onChange={this.handleTextChange} />
                                            </div>

                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="role" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}} >Role <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <select className="form-control" id="role" required="required" onChange={this.handleSelectChange} name="role">
                                                    <option>Choose Role</option>
                                                    <option>Web Developer</option>
                                                    <option>Android Developer</option>
                                                    <option>Ios developer</option>
                                                    <option>Full stack developer</option>
                                                    <option>Designer</option>
                                                    <option>Tester</option>
                                                    <option>Scrum master</option>
                                                    <option>Product owner</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="ln_solid"></div>
                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                <button className="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" className="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>

                                </Modal>
                                :
                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <strong> {this.state.messageFromServer}</strong>
                                </div>
                            }
                            {/* add project modal ends */}

                            <div className="col-md-12 col-sm-12 col-xs-12">
                                <div className="x_panel">
                                    <div className="x_title">
                                        <button type="button" className="btn btn-info btn-sm" onClick={this.openModal}>Add User </button>
                                        <ul className="nav navbar-right panel_toolbox">
                                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li className="dropdown">
                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                <ul className="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="x_content">
                                        {/* <p className="text-muted font-13 m-b-30">
                                                                DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                                            </p> */}
                                        <table id="datatable" className="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Avatar</th>
                                                    <th>Full Name</th>
                                                    <th>Email</th>
                                                    <th>Role</th>
                                                    <th>CreatedAt</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>


                                            <tbody>
                                                {
                                                    this.state.usersData.map((user) => {
                                                        return (<tr key={user._id} style={{ textAlign: 'center' }}>
                                                            <td><img src={user.imageIsUpdated =="true"?'./uploads/'+user.image_192:user.image_192} className="img-circle img-responsive" style={{ width: '20%', textAlign: 'center', margin: '0 auto' }} onClick={() => this.openUserProfile(user)} /></td>
                                                            {user.real_name !="" ?<td>{user.real_name}</td>:(<td>{user.first_name} {user.last_name}</td>)}
                                                            <td>{user.email}</td>
                                                            <td>{user.role}</td>
                                                            <td>{FormatDate(user.createdAt)}</td>
                                                            <td><DeleteUser user={user} /></td>

                                                        </tr>)
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/*list clients Ends */}


                        </div>
                    </div>
                </div>
            </div>

        );
    }
        else{
            return(
                <AccessDeniedPage />
            );
        }
    }






}
export default withRouter(TeamContainer);