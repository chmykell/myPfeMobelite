import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';

import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate';
import {FormatDateToDate} from '../../common/formatIsoStringDateToDateWithoutTime';


import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';




class TopGeneralStatsContainer extends  React.Component{


    constructor(props) {
        super(props);
        this.state = {
            clientsList: [],
            usersList: [],
            projectsList: [],
            tasksList: []
           
        };
        this.getClientsList = this.getClientsList.bind(this);
        this.getUsersList = this.getUsersList.bind(this);
        this.getProjectsList = this.getProjectsList.bind(this);
        this.getTasksList = this.getTasksList.bind(this);

     
    }



    getClientsList(ev) {
      
        axios.get('/clients')
            .then(function (response) {
                ev.setState({ clientsList: response.data });
               // console.log('responseProjects', response.data)
            });
        
    }
    getProjectsList(ev) {
      
        axios.get('/projects')
            .then(function (response) {
                ev.setState({ projectsList: response.data });
                console.log('responseProjects', response.data)
            });
        
    }
    getUsersList(ev) {
      
        axios.get('/users')
            .then(function (response) {
                ev.setState({ usersList: response.data });
                console.log('responseProjects', response.data)
            });
        
    }
    getTasksList(ev) {
      
        axios.get('/tasks')
            .then(function (response) {
                ev.setState({ tasksList: response.data });
                console.log('responseProjects', response.data)
            });
        
    }
    componentWillMount(){
        this.getClientsList(this);
        this.getUsersList(this);
        this.getProjectsList(this);
        this.getTasksList(this);
    }
   


    render(){
        return(
            <div className="row top_tiles">
 <div className="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <div className="tile-stats">
     <div className="icon"><i className="fa fa-dollar" style={{color: '#21bb9c'}}></i></div>
     <div className="count">{this.state.clientsList.length}</div>
     <h3>Clients</h3>
     <p>Total number of clients.</p>
   </div>
 </div>
 <div className="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <div className="tile-stats">
     <div className="icon"><i className="fa fa-users" style={{color: '#21bb9c'}}></i></div>
     <div className="count">{this.state.usersList.length}</div>
     <h3>Employees</h3>
     <p>Working in the company.</p>
   </div>
 </div>
 <div className="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <div className="tile-stats">
     <div className="icon"><i className="fa fa-file" style={{color: '#21bb9c'}}></i></div>
     <div className="count">{this.state.projectsList.length}</div>
     <h3>Projects</h3>
     <p>Crafted with <i className="fa fa-heart" style={{color: 'red'}}></i> .</p>
   </div>
 </div>
 <div className="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <div className="tile-stats">
     <div className="icon"><i className="fa fa-check-square-o" style={{color: '#21bb9c'}}></i></div>
     <div className="count">{this.state.tasksList.length}</div>
     <h3>Worked Tasks</h3>
     <p>Logged in TeamBoard.</p>
   </div>
 </div>
</div>
        )
    }
}
export default TopGeneralStatsContainer;
 