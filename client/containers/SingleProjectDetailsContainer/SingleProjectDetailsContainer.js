import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';
import { withRouter } from 'react-router';
import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate'
import { Pie, Line, Bar } from 'react-chartjs-2';
import ProjectNotesContainer from '../projectNotesContainer/projectNotesContainer';
import SingleProjectStatsContainer from '../singleProjectStatsContainer/singleProjectStatsContainer'
import ProjectSummaryContainer from '../projectSummaryContainer/projectSummaryContainer';



class SingleProjectDetailsContainer extends React.Component {
   
    constructor(props){
        super(props);
        this.state = {
            selectedProject: []

        };
        this.getProjectById = this.getProjectById.bind(this);

    }
    componentWillMount(){
        
        console.log('projectttttt idididididid', this.props.projectId)
        this.getProjectById(this);
       
    }
    getProjectById(ev){
        axios.get('/projects/'+ this.props.projectId)
        .then(function (response) {
            ev.setState({ selectedProject: response.data });
            console.log('selectedProject', response.data)
        });
    }
   
    render(){
        return(
            <div className="right_col" role="main" >
                <div className="">
                    <div className="page-title">
                        <div className="title_left" style={{ textAlign: 'center', width: '100%' }}>
                        <div className="row">
                            <h3 style={{ textAlign: 'center' }}>{this.state.selectedProject.projectName}</h3>
                            <div className="col-md-4 col-xs-12" style={{float: 'right', marginRight: '5px', marginBottom:'15px'}}>
                                <div id="reportrange" className="pull-right" style={{marginTop: '5px', background: '#fff', cursor: 'pointer', padding: '5px 10px', border: '1px solid #E6E9ED'}}>
                                  <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                  <span>December 30, 2014 - January 28, 2015</span> <b className="caret"></b>
                                </div>
                              </div>
                        </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>

                    <div className="row">
                        <div className="x_content">
                        {this.state.selectedProject.length != 0 && <ProjectNotesContainer  project={this.state.selectedProject}/>}   
                           <SingleProjectStatsContainer project={this.state.selectedProject} />
                           <ProjectSummaryContainer project={this.state.selectedProject} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="x_content">
                           Time line goes here
                        </div>
                    </div>
                    
                </div> 
            </div>
        )
    }
}
export default SingleProjectDetailsContainer;