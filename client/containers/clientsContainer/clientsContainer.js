import React from 'react';
import axios from 'axios';

import Modal from 'react-modal';
import '../../assets/select2/dist/css/select2.min.css';
import { LoadScript } from '../../common/loadScript'
import UpdateClient from '../clientsContainer/updateClient';
import DeleteClient from '../clientsContainer/deleteClient';
import {FormatDate} from '../../common/formatIsoStringDateToDate'
import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';


var querystring = require('querystring');
var validator = require("email-validator");



const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};


class ClientsContainer extends React.Component {


    constructor() {
        super();

        this.state = {
            modalIsOpen: false, 
            firstName: 'sam',
            lastName: '',
            email: '',
            phoneNumber: '',
            country: '',
            messageFromServer: '',
            clientsData: []
        };


        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleAddClientClick = this.handleAddClientClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.getClients = this.getClients.bind(this);
    }
    //Loading javascript

    componentDidMount() {
        // LoadScript('../../assets/jquery/dist/jquery.min.js');
        // LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
        // LoadScript('../../assets/fastclick/lib/fastclick.js');
        // LoadScript('../../assets/nprogress/nprogress.js');
        LoadScript('../../assets/iCheck/icheck.min.js');
        LoadScript('../../assets/datatables.net/js/jquery.dataTables.min.js');
        LoadScript('../../assets/datatables.net-bs/js/dataTables.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/dataTables.buttons.min.js');
        LoadScript('../../assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.flash.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.html5.min.js');
        LoadScript('../../assets/datatables.net-buttons/js/buttons.print.min.js');
        LoadScript('../../assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
        LoadScript('../../assets/datatables.net-keytable/js/dataTables.keyTable.min.js');
        LoadScript('../../assets/datatables.net-responsive/js/dataTables.responsive.min.js');
        LoadScript('../../assets/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        LoadScript('../../assets/datatables.net-scroller/js/dataTables.scroller.min.js');
        LoadScript('../../assets/jszip/dist/jszip.min.js');
        LoadScript('../../assets/pdfmake/build/pdfmake.min.js');
        LoadScript('../../assets/pdfmake/build/vfs_fonts.js');
        // LoadScript('../../assets/build/js/custom.min.js');
        console.log('staattatattezraezre', this.state);
        
    

    }
    componentWillMount() {
        this.getClients(this);

    }

    getClients(ev) {
        axios.get('/clients')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ clientsData: response.data });
                console.log('clientDclientsDataata')

            });
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }

    handleTextChange(e) {
        if (e.target.name == "firstName") {
            this.setState({
                firstName: e.target.value
            });
        }
        if (e.target.name == "lastName") {
            this.setState({
                lastName: e.target.value
            });
        }
        if (e.target.name == "email") {
            let validEmail = e.target.value;
            if (validator.validate(validEmail)) {
                this.setState({
                    email: e.target.value,
                    validEmail: true
                });
            } else {
                this.setState({
                    validEmail: false
                });
            }

        }
        if (e.target.name == "phoneNumber") {
            this.setState({
                phoneNumber: e.target.value
            });
        }
        if (e.target.name == "country") {
            this.setState({
                country: e.target.value
            });
        }

    }

    handleSelectChange(e) {

        this.setState({ country: e.target.value });
    }

    handleAddClientClick(e) {
        e.preventDefault();
        this.insertNewClient(this);
        // if(this.state.validEmail)
        // {
        // this.insertNewClient(this);
        // }
        // else{
        //     alert('email non valid !!!')
        // }
        // console.log('state=', this.state)
    }

    insertNewClient(e) {
        let outside = this;
        axios.post('/clients',
            querystring.stringify({
                firstName: e.state.firstName,
                lastName: e.state.lastName,
                email: e.state.email,
                phoneNumber: e.state.phoneNumber,
                country: e.state.country
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function (response) {
                
                e.setState({
                    messageFromServer: response.data
                    
                });
                ouside.getClients(e);

            });
          //  this.forceUpdate();
    }

    // validation check to properly add users



    render() {

        return (
            <div className="right_col" role="main">
                <div className="">
                    <div className="page-title">
                        <div className="title_left">
                            <h3>Clients List</h3>
                        </div>

                        <div className="title_right">
                            <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search for..." />
                                    <span className="input-group-btn">
                                        <button className="btn btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>
                    <div className="row">
                        <div className="x_content">


                            {/* add project modal begins */}
                            {this.state.messageFromServer == '' ?
                                <Modal
                                    isOpen={this.state.modalIsOpen}
                                    onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                    ariaHideApp={false}
                                >


                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                    
                                    <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Add client</h2>
                                    <div className="ln_solid"></div>
                                    <form onSubmit={this.handleAddClientClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name" style={{textAlign: 'center'}}>First Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="firstName" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name" style={{textAlign: 'center'}}>Last Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="lastName" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name" style={{textAlign: 'center'}}>Phone Number <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="number" id="phoneNumber" name="phoneNumber" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="email" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}}>Email <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input id="email" className="form-control col-md-7 col-xs-12" type="text" name="email" required="required" onChange={this.handleTextChange} />
                                            </div>

                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="country" className="control-label col-md-3 col-sm-3 col-xs-12" style={{textAlign: 'center'}} >Country <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <select class="form-control" id="country" required="required" onChange={this.handleSelectChange} name="country">
                                                    <option>Choose Country</option>
                                                    <option>USA</option>
                                                    <option>ENG</option>
                                                    <option>France</option>
                                                    <option>England</option>
                                                    <option>Italia</option>
                                                    <option>Germany</option>
                                                    <option>SouthAfrica</option>
                                                    <option>Netherland</option>
                                                    <option>Asia</option>
                                                    <option>Austria</option>
                                                    <option>Middle East</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="ln_solid"></div>
                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                <button className="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" className="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>

                                </Modal>
                                :
                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <strong> {this.state.messageFromServer}</strong>
                                </div>
                            }
                            {/* add project modal ends */}
                           
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_title">
                                                        <button type="button" className="btn btn-info btn-sm" onClick={this.openModal}>Add client </button>
                                                            <ul class="nav navbar-right panel_toolbox">
                                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                                </li>
                                                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                                    <ul class="dropdown-menu" role="menu">
                                                                        <li><a href="#">Settings 1</a>
                                                                        </li>
                                                                        <li><a href="#">Settings 2</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                                </li>
                                                            </ul>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="x_content">
                                                            {/* <p class="text-muted font-13 m-b-30">
                                                                DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                                                            </p> */}
                                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Email</th>
                                                        <th>Tel</th>
                                                        <th>Country</th>
                                                        <th>CreatedAt</th>
                                                        <th>Actions</th>
                                                    </tr> 
                                                </thead>


                                                <tbody>
                                                    {
                                                        this.state.clientsData.map((client) => {
                                                            
                                                            return (<tr key={client._id} style={{textAlign: 'center'}}>
                                                                <td>{client.firstName}</td>
                                                                <td>{client.lastName}</td>
                                                                <td>{client.email}</td>
                                                                <td>{client.phoneNumber}</td>
                                                                <td>{client.country}</td>
                                                                <td>{FormatDate(client.createdAt)}</td>
                                                                <td><DeleteClient client={client} /><UpdateClient client={client} /></td>

                                                            </tr>)
                                                            
                                                        })
                                                        
                                                      
                                                    }
                                                    
                                                     
                                                </tbody>
                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/*list clients Ends */}


                            </div>
                        </div>
                    </div>
                </div>

                )
            }
        
        
        
        
        
        
        }
export default ClientsContainer;