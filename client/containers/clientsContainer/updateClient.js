import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DeleteAlert from '../../components/deleteAlert/deleteAlert';
import Modal from 'react-modal';
var querystring = require('querystring');
var validator = require("email-validator");
const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};
class UpdateClient extends React.Component {
    constructor() {
        super();
        this.state = { id: '', 
         modalIsOpen: false,
         messageFromServer: '',
         firstName: '',
         lastName: '',
         email:  '',
         phoneNumber: '',
         country : ''
        };
        this.onClick = this.onClick.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.confirmClick = this.confirmClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
    }
    componentDidMount() {
        this.setState({
            id: this.props.client._id,
            firstName: this.props.client.firstName, 
            lastName: this.props.client.lastName ,
            email: this.props.client.email ,
            phoneNumber: this.props.client.phoneNumber, 
            country: this.props.client.country 

        })
    }
    openModal() {
        this.setState({ modalIsOpen: true });
        console.log('id is: :::', this.state.id)
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    handleTextChange(e) {
        if (e.target.name == "firstName") {
            this.setState({
                firstName: e.target.value
            });
        }
        if (e.target.name == "lastName") {
            this.setState({
                lastName: e.target.value
            });
        }
        if (e.target.name == "email") {
            let validEmail = e.target.value;
            if (validator.validate(validEmail)) {
                this.setState({
                    email: e.target.value,
                    validEmail: true
                });
            } else {
                this.setState({
                    validEmail: false
                });
            }

        }
        if (e.target.name == "phoneNumber") {
            this.setState({
                phoneNumber: e.target.value
            });
        }
        if (e.target.name == "country") {
            this.setState({
                country: e.target.value
            });
        }

    }

    handleSelectChange(e) {

        this.setState({ country: e.target.value });
    }

    // componentWillReceiveProps(nextProps){
    //   this.setState({
    //     id: nextProps.expense._id,
    //     month:nextProps.expense.month,
    //     year:nextProps.expense.year
    //   })
    //   }
    onClick(e) {
        // this.delete(this);
       this.openModal();
    }
    confirmClick(e){
        e.preventDefault();
        this.updateClient(this);
    }
    
    updateClient(e) {
         axios.put('/clients/edit/' + e.state.id,
            querystring.stringify({
                _id: e.state.id,
                firstName: e.state.firstName,
                lastName: e.state.lastName,
                email: e.state.email,
                phoneNumber: e.state.phoneNumber,
                country: e.state.country
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function (response) {
                
                e.setState({
                    messageFromServer: response.data,
                    modalIsOpen: false
                });

            });
    }




    render() {
        return ( 
            <div>
                <i className="fa fa-edit" onClick={this.onClick} style={{float: 'left', fontSize: '20px'}}> </i>
                <Modal
                                    isOpen={this.state.modalIsOpen}
                                    onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                    ariaHideApp={false}
                                >


                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>

                                    <form onSubmit={this.confirmClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name">First Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="first-name" name="firstName" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.firstName}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name">Last Name <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="lastName" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.lastName} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name">Phone Number <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input type="number" id="phoneNumber" name="phoneNumber" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.phoneNumber}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="email" className="control-label col-md-3 col-sm-3 col-xs-12">Email <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <input id="email" className="form-control col-md-7 col-xs-12" type="text" name="email" required="required" onChange={this.handleTextChange} defaultValue={this.state.email}/>
                                            </div>

                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="country" className="control-label col-md-3 col-sm-3 col-xs-12" >Country <span className="required">*</span></label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">
                                                <select class="form-control" id="country" required="required" onChange={this.handleSelectChange} name="country" defaultValue={this.state.country}>
                                                    <option>Choose Country</option>
                                                    <option>USA</option>
                                                    <option>ENG</option>
                                                    <option>Tunisia</option>
                                                    <option>Argentine</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="ln_solid"></div>
                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                <button className="btn btn-primary" onClick={this.closeModal}>Cancel</button>
                                                <button type="submit" className="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>

                                </Modal>
            </div>
              
        )
    }
}
export default UpdateClient;