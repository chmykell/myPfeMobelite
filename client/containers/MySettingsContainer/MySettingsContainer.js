import React from 'react';

import { LoadScript } from '../../common/loadScript';
import axios from 'axios';
import { withRouter } from 'react-router';
import Modal from 'react-modal';
import Input from 'react-input-password';
import EditProfileComponent from '../../components/EditProfileComponent/EditProfileComponent';
var querystring = require('querystring');
var validator = require("email-validator");

var passwordHash = require('password-hash');

const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};

class MySettingsContainer extends React.Component {

    constructor(){
        super();
        this.state = {
            user: '',
            dataBaseUser: '',
            modalIsOpen: false,
            messageFromServer: '',
            password: '',
            passwordConfirm: '',
            confirmColor: 'red',
            passwordValidation: '',
            submitPasswordButton: false,
            hashedPassword: '',
            userHasPassowrd: false,
            oldPasswordCheck: false,
            editProfileModal: false
        };
        this.getSelectedUser = this.getSelectedUser.bind(this);
        this.handleClickSetPassword = this.handleClickSetPassword.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        // this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSubmitPasswordClick = this.handleSubmitPasswordClick.bind(this);
        this.handleTextChangeReactInput = this.handleTextChangeReactInput.bind(this);
        this.getToken = this.getToken.bind(this);
        this.handleTextChangeReactInputConfirm = this.handleTextChangeReactInputConfirm.bind(this);
        this.updateUserPassword= this.updateUserPassword.bind(this);
        this.handleOldPasswordChange = this.handleOldPasswordChange.bind(this);
        this.handleClickEditProfile = this.handleClickEditProfile.bind(this);

    }
    componentWillMount(){
        var userObject = JSON.parse(localStorage.getItem('token'));
        if (userObject != null) {
          this.setState({ user: userObject.user });
        }
        setTimeout(() => {
          this.getSelectedUser(this);
        }, 100);
    }
   
    getSelectedUser(ev) {
        // getting user from id passed as parameter
        axios.get('/users/bySlackId/' + this.state.user.id)
          .then(function (response) {
            ev.setState({ dataBaseUser: response.data });
            console.log('DataBase user', response.data);
            if(response.data.password != ''){
                ev.setState({userHasPassowrd: true});
            }else{
                ev.setState({userHasPassowrd: false, oldPasswordCheck: true});
            }
    
          });
      }
      // handling set password click from my settings
     handleClickSetPassword(){
         console.log('set passwd clicked');
         this.openModal();

     }
     openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    handleOpenModalClick(){
        this.setState({ editProfileModal: true });
    }
    handleCloseModalClick(){
        this.setState({
            editProfileModal: false,
            messageFromServer: ''
        });
    }
    // handleTextChange(e) {
    //     if (e.target.name == "password") {
    //         this.setState({
    //             password: e.target.value
    //         });
    //     }
       

    // }
    handleTextChangeReactInput(e){
        if(e.target.value.length < 5){
            this.setState({
                passwordValidation: 'Weak Password !!'
            });
        }
        if(e.target.value.length > 5){
            this.setState({
                passwordValidation: 'Good Password !!'
            });
        }
        if(this.state.passwordConfirm != '' && this.state.passwordConfirm != e.target.value){
            this.setState({
                            password: e.target.value,
                            confirmColor: 'red'

                        });
        }
        else{
            this.setState({
                password: e.target.value
                
            });

        }

    }
    handleOldPasswordChange(e){
        let hashedPassword = this.state.dataBaseUser.password;
        if(passwordHash.verify(e.target.value, this.state.dataBaseUser.password)){
            this.setState({oldPasswordCheck: true});
        }
        else{
            this.setState({oldPasswordCheck: false});
        }

    }
    handleTextChangeReactInputConfirm(e){
        if(this.state.password == e.target.value){
            this.setState({
                passwordConfirm: e.target.value,
                confirmColor: 'green',
                submitPasswordButton: true

            });
            
             }
             else{
                this.setState({
                    confirmColor: 'red',
                    submitPasswordButton: false
                });
             }
    }
    getToken(){
        var userObject = JSON.parse(localStorage.getItem('token'));
        return userObject;
    }
    handleSubmitPasswordClick(e){
        e.preventDefault();
        if(this.state.password == this.state.passwordConfirm && this.state.password.length > 5 && this.state.oldPasswordCheck){

            console.log('just submitted my  new password', this.state.passwordConfirm);
            console.log('token', this.getToken());
            //encrypt the password to store in datbase with password-hash from npm module
            //to view the original value just use  "console.log(passwordHash.verify(this.state.passwordConfirm, hashedPassword)); // true"
            var passwordHash = require('password-hash');
            var hashedPassword = passwordHash.generate(this.state.passwordConfirm);
            console.log(hashedPassword); 
            this.setState({
                hashedPassword: hashedPassword,
                token: localStorage.getItem('token')
            });
            setTimeout(() => {
                this.updateUserPassword(this);
                
            }, 100);
            
           
        }
       

    }
    confirmClick(e){
        e.preventDefault();
        this.updateClient(this);
    }
    
    updateUserPassword(e) {
       
         axios.put('/users/setPassword/'+e.state.dataBaseUser._id,
            querystring.stringify({
               paswd: e.state.hashedPassword,
               token: e.state.token
               
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function (response) {
                
                e.setState({
                    messageFromServer: response.data,
                    modalIsOpen: false
                });

            });
    }
    // open edit profile modal
    handleClickEditProfile(){
        this.setState({editProfileModal: true});
    }

   
   
    render(){
        return(
            <div className="right_col" role="main">
            <div className="">
                <div className="page-title">
                    <div className="title_left" style={{width: '100%'}}>
                        <h3 style={{textAlign: 'center'}}>My Settings</h3>
                    </div>

                    {/* <div className="title_right">
                        <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search for..." name="search" id="search"  />
                                <span className="input-group-btn">
                                    <button className="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div> */}
                </div>

                <div className="clearfix"></div>

                <div className="row">





                    <div className="x_content">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="x_panel">
                                <div className="x_title">
                                    {/* <button type="button" classNameName="btn btn-info btn-sm">Add User </button> */}
                                    {/* <h4 style={{textAlign: 'center'}}>TeamOverview</h4> */}
                                  


                                    <ul className="nav navbar-right panel_toolbox">
                                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                        </li>
                                        {/* <li className="dropdown">
                                                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                                <ul className="dropdown-menu" role="menu">
                                                                    <li><a href="#">Settings 1</a>
                                                                    </li>
                                                                    <li><a href="#">Settings 2</a>
                                                                    </li>
                                                                </ul>
                                                            </li> */}
                                        <li><a className="close-link"><i className="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="x_content">

                                    <div className="row">
                                        {/* settings test begins */}
                                        {this.state.editProfileModal && <EditProfileComponent user={this.state.dataBaseUser} modalIsOpen={this.state.editProfileModal} handleOpenModalClick={this.handleOpenModalClick.bind(this)} handleCloseModalClick={this.handleCloseModalClick.bind(this)}/> }

                                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                                                <div className="hpanel">
                                                    <div className="panel-body">

                                                    <div className="row" style={{marginLeft:'0', marginRight:'0'}}>
                                                        <div className="col-lg-10 col-md-10 col-sm-12">
                                                            <h3>Profile</h3>
                                                            <p className="m-b-lg">You can edit your profile on Mobelite TeamBoard</p>

                                                            <img className="img-circle pull-left" style={{width:'85px', height:'85px', marginRight:'20px'}} src={this.state.dataBaseUser.imageIsUpdated =="true"?'./uploads/'+this.state.dataBaseUser.image_192:this.state.dataBaseUser.image_192}/>
                                                            <p style={{fontSize: '17.5px'}}>{this.state.dataBaseUser.real_name}</p>
                                                            <p>
                                                            <small className="text-muted">TIME ZONE:</small><br/>
                                                            <span className="font-bold">{this.state.dataBaseUser.tz}</span><br/>
                                                            <small className="text-muted">TIME ZONE:</small><br/>
                                                            {this.state.dataBaseUser.email}
                                                            </p>
                                                        </div>
                                                        <div className="col-lg-2 col-md-2 col-sm-12 text-right">
                                                            <button className="btn btn-primary" type="button" onClick={this.handleClickEditProfile}><i className="fa fa-pencil"></i> Edit</button>
                                                        </div>
                                                    </div>

                                                    <div className="row" style={{marginLeft:'0', marginRight:'0'}}>
                                                        <hr/>
                                                    </div>
                                                    <div className="row" style={{marginLeft:'0', marginRight:'0'}}>
                                                     <div className="col-lg-9 col-md-9 col-sm-9" style={{paddingRight:'0'}}>
                                                     <h3>Log time</h3>
                                        

                                                  <p>MobeliteBot will ask you every day at 
                                                        <a className="edit_log_time_action disabled"><b><u>10:00 AM</u></b></a>
                                            what you worked on <a className="edit_log_time_action disabled"><b>
                                                <u>the previous day</u></b></a> 
                                        </p>
                                        <p>If you're not online at that time, Mobelite will <a className="edit_log_time_action disabled"><b><u>wait until you open Slack</u></b></a></p>


                                        

                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-3 text-right">
                                        
                                        <button className="btn btn-primary m-t-md edit_log_time_action disabled" type="button"><i className="fa fa-pencil"></i> Edit</button> 
                                        </div>
                                    </div>



                                      <div className="row" style={{marginLeft:'0', marginRight:'0'}}>
                                                        <hr/>
                                                    </div>

                                                    <div className="row" style={{marginLeft:'0', marginRight:'0'}}>
                                                     <div className="col-lg-9 col-md-9 col-sm-9" style={{paddingRight:'0'}}>
                                                     <h3>Set your Password </h3>
                                                        <p>Set your password to log in with email once your signed up with slack
                                                        
                                        </p>
                                        {/* <p>If you want to set a new password  <a className="edit_log_time_action disabled"><b><u>reset password</u></b></a></p> */}


                                        

                                        </div>
                                        <div className="col-lg-3 col-md-3 col-sm-3 text-right">
                                        
                                        {this.state.userHasPassowrd?<button className="btn btn-primary" type="button" onClick={this.handleClickSetPassword}><i className="fa fa-pencil"></i> Edit Passwd</button> :<button className="btn btn-primary" type="button" onClick={this.handleClickSetPassword}><i className="fa fa-pencil"></i> Set Passwd</button> }
                                        </div>
                                    </div>


                                                    </div>
                                                </div>
                                            </div>

                                        {/* settings test ends */}
                                         {/* setting password modal begins */}
                            {this.state.messageFromServer == '' ?
                                <Modal
                                    isOpen={this.state.modalIsOpen}
                                    onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                    ariaHideApp={false}
                                >


                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                    
                                    <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Set your password</h2>
                                    <div className="ln_solid"></div>
                                    <form onSubmit={this.handleSubmitPasswordClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">
                                   {this.state.userHasPassowrd &&
                                   <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name">Old password : <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12" style={{width: '5%'}}>
                                                {/* <input type="text" id="password" name="password" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} /> */}
                                                <Input onChange={this.handleOldPasswordChange} />
                                            </div>
                                            {(this.state.oldPasswordCheck && this.state.userHasPassowrd)?<h4 style={{textAlign: 'center', color: 'green'}}> Old Password Confirmed</h4>:''}
                                            
                                        </div>}
                                    { this.state.passwordValidation == 'Good Password !!' ? <h4 style={{color:'green', textAlign: 'center'}}>{this.state.passwordValidation}</h4>:<h4 style={{color:'red', textAlign: 'center'}}>{this.state.passwordValidation}</h4>}
                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name">New password : <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12" style={{width: '5%'}}>
                                                {/* <input type="text" id="password" name="password" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} /> */}
                                                <Input onChange={this.handleTextChangeReactInput} />
                                            </div>
                                            
                                        </div>
                                       

                                         <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name">Confirm password : <span className="required">*</span>
                                            </label>
                                            <div className="col-md-7 col-sm-6 col-xs-12" style={{width: '5%', color: this.state.confirmColor}}>
                                                {/* <input type="text" id="password" name="password" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} /> */}
                                                <Input onChange={this.handleTextChangeReactInputConfirm} />
                                            </div>
                                            {this.state.confirmColor == 'green' && <h4 style={{textAlign: 'center', color: 'green'}}>Password Confirmed</h4>}
                                        </div>
                                       

                                       
                                        <div className="ln_solid"></div>
                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                <button className="btn btn-primary" type="reset">Reset</button>
                                                {this.state.submitPasswordButton? <button type="submit" className="btn btn-success">Submit</button>:<button type="submit" className="btn btn-success disabled">Submit</button> }
                                            </div>
                                        </div>

                                    </form>

                                </Modal>
                                :
                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <strong> {this.state.messageFromServer}</strong>
                                </div>
                            }
                            {/* setting password modal ends */}
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>

    );
        
    }
}
export default withRouter(MySettingsContainer);