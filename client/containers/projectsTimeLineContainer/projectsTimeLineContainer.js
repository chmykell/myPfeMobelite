import React from 'react';
import UsersPerProjectTimeLine from '../../components/usersPerProjectTimeLine/usersPerProjectTimeLine';
import TimeLineUsersPerProject from '../../components/TimeLineUsersPerProject/TimeLineUsersPerProject';





class ProjectsTimeLineContainer extends React.Component {


    render() {
        return (
            // <div className="col-md-12 col-sm-12 col-xs-12">
            //     <div className="x_panel">
            //         <div className="x_title">
            //             {/* <button type="button" className="btn btn-info btn-sm">Add User </button> */}
            //             <span>All Projects stats</span>
            //             <ul className="nav navbar-right panel_toolbox">
            //                 <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
            //                 </li>
            //                 {/* <li className="dropdown">
            //                 <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
            //                 <ul className="dropdown-menu" role="menu">
            //                     <li><a href="#">Settings 1</a>
            //                     </li>
            //                     <li><a href="#">Settings 2</a>
            //                     </li>
            //                 </ul>
            //             </li> */}
            //                 <li><a className="close-link"><i className="fa fa-close"></i></a>
            //                 </li>
            //             </ul>
            //             <div className="clearfix"></div>
                        <div className="col-md-12 col-sm-12 col-xs-12">
<div class="hpanel">
          <div class="panel-body">
                        {/* row begins for stat projects timeline */}
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12">
                            {/* project title begins  */}
                                <h3 className="project_header">
                                    <a href="#" style={{ paddingRight: '10px' }}><i className="fa fa-folder-o" style={{ padding: '0 17px 0 8px' }}></i>Project Frango</a>
                                    <div style={{ position: 'absolute', display: 'inline-block' }}>
                                        <button className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" type="button"><i className="fa fa-bars" aria-hidden="true"></i></button>
                                        <ul className="dropdown-menu">
                                            <li><a href="#" className="action_edit" ><i className="fa fa-fw fa-pencil" style={{ marginRight: '10px' }} aria-hidden="true"></i>Edit</a></li>
                                            <li><a href="#" className="action_merge" ><i className="fa fa-fw fa-code-fork" style={{ marginRight: '10px' }}></i>Merge</a></li>
                                            <li><a href="#" className="action_archive" ><i className="fa fa-fw fa-archive" style={{ marginRight: '10px' }}></i>Archive</a></li>
                                            <li><a href="#" className="action_delete" ><i className="fa fa-fw fa-trash-o" style={{ marginRight: '10px' }}></i>Delete</a></li>
                                        </ul>
                                    </div>
                                    <div className="pull-right small" style={{ lineHeight: '26px' }}>
                                        <i className="fa fa-clock-o"></i> 492h
                                    </div>
                                    <small style={{ marginLeft: '49px', marginTop: '10px', display: 'block' }}>
                                    </small>
                                </h3>

                                         {/* Project Title ends */}
                                         {/* list users on left begins */}
                                         <UsersPerProjectTimeLine/>
                                         {/* list users on left ends */}
                                         
                                         {/* time line per user in the project begins */}
                                         <TimeLineUsersPerProject/>
                                         {/* time line per user in the project ends */}
                                         

                                         


                                    
                            </div>

                        </div>

                        {/* stat projects timeline ends */}


                    </div>
                </div>
            </div>
        )
    }
}
export default ProjectsTimeLineContainer;
