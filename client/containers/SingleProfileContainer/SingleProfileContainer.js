import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';
import { withRouter } from 'react-router';
import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate'
import { Pie, Line, Bar } from 'react-chartjs-2';

import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/bootstrap-daterangepicker/daterangepicker.css';
import '../../assets/build/css/custom.min.css';










class SingleProfileContainer extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      user: '',
      NotesByUser: [],
      notesToShow: 3,
      expanded: false,
      userProjectsFromApi: [],
      userProjectsDetails: [],
      userTotalLoggedHours: 0,
      dataToSendAsProps: [],
      generatedData: [],
      generatedLabels: []
    };



    this.showMore = this.showMore.bind(this);
    this.getSelectedUser = this.getSelectedUser.bind(this);
    this.getNotesByUser = this.getNotesByUser.bind(this);
    this.getUserProjects = this.getUserProjects.bind(this);

  } 
  getSelectedUser(ev) {
    // getting user from id passed as parameter
    axios.get('/users/bySlackId/' + this.props.userId)
      .then(function (response) {
        ev.setState({ user: response.data });
        console.log('response from byslack users api', response.data);

      });
  }
  // Get the list of notes for the particular user from user state
  getNotesByUser(ev) {
    axios.get('/notes/byUser/' + this.state.user._id)
      .then(function (response) {
        console.log('response notes by user', response);
        ev.setState({ NotesByUser: response.data });

      });
  }
  showMore() {
    this.state.notesToShow === 3 ? (
      this.setState({ notesToShow: this.state.NotesByUser.length, expanded: true })
    ) : (
        this.setState({ notesToShow: 3, expanded: false })
      )
  }
  // Get the list of projects and logged hours
  getUserProjects(ev) {
    var outside = this;
    let userProjectsDetails = [];
    let userTotalLoggedHours = 0;
    axios.get('/tasks/hoursByProject/' + this.state.user._id)
      .then(function (response) {
        let secondOutside = outside;
        console.log('user projectssssssssssssssss', response);
        ev.setState({ userProjectsFromApi: response.data });
        for (let index = 0; index < response.data.length; index++) {
          userTotalLoggedHours = userTotalLoggedHours + response.data[index].totalLoggedHours;
          axios.get('/projects/' + response.data[index]._id)
            .then(function (response) {
              console.log('did IT', response);
              userProjectsDetails.push(response.data);
              // ev.setState({userTotalLoggedHours: (ev.state.userTotalLoggedHours+response.data.totalLoggedHours)})

            });
        }
      });
    setTimeout(() => {
      console.log('final !!!!!', userProjectsDetails);
      this.setState({
        userProjectsDetails: userProjectsDetails,
        userTotalLoggedHours: userTotalLoggedHours
      });


    }, 900);
  }

  componentWillMount() {
    // let user = this.state.userSelected
    // this.setState({user: this.props.user});
    this.getSelectedUser(this);
    setTimeout(() => {
      this.getNotesByUser(this);

    }, 800);




  }
  componentDidMount() {

    LoadScript('../../assets/raphael/raphael.min.js');
    LoadScript('../../assets/morris.js/morris.min.js');
    LoadScript('../../assets/bootstrap-progressbar/bootstrap-progressbar.min.js');
    LoadScript('../../assets/moment/min/moment.min.js');
    LoadScript('../../assets/bootstrap-daterangepicker/daterangepicker.js');
    // console.log('this.props',this.props.user);
    // console.log('this.props.match.params.name',this.props.match.params.name);
    // console.log('this.props.location',this.props.history);
    // console.log('this.props.location.state',this.props.location.state);
    // // console.log('this.props.location.state.userSelected',this.props.location.state.userSelected);
    console.log('this.state', this.state.user)
    setTimeout(() => {
      this.getUserProjects(this);

    }, 800);


    // testing starts
    setTimeout(() => {
      var generatedLabels = [];
      var generatedData = [];
      var generatedColors = [];
      var allProjects = this.state.userProjectsDetails;
      var allHoursOnProjects = this.state.userProjectsFromApi;
      for (let i in allProjects) {
        //testing if a project has some loggedHours because if not no calculation needed
        if (allHoursOnProjects[i].totalLoggedHours != 0) {
          generatedLabels.push(allProjects[i].projectName);
          //calculating percentage of progress in each project
          var progressPercentage = ((allHoursOnProjects[i].totalLoggedHours) / this.state.userTotalLoggedHours) * 100;
          console.log('percentage calc', progressPercentage);
          //sending data percentage with  limit of two numbers after ","
          generatedData.push(progressPercentage.toFixed(1));
          generatedColors.push(allProjects[i].color);
        }
        else {
          i++;
        }
      }

      const data = {
        labels: generatedLabels,
        datasets: [{
          data: generatedData,
          backgroundColor: generatedColors,
          hoverBackgroundColor: generatedColors
        }]
      };

      this.setState({ dataToSendAsProps: data });
      this.setState({ generatedData: generatedData });
      this.setState({ generatedLabels: generatedLabels });
      console.log('dataToSendAsProps', this.state.dataToSendAsProps)
      console.log('peopsss data generatedData', this.state.generatedData)
      console.log('peopsss data generatedLabels', this.state.generatedLabels)
      console.log('peopsss data userTotalLoggedHours', this.state.userTotalLoggedHours)
    }, 1800);
    setTimeout(() => {
      console.log('all state after 3 s', this.state)
    }, 1200);

    // testing ends here



  }
  render() {
    var options = {
      responsive: true,
      title: {
        display: false,
        position: "top",
        text: "Most spent Time",
        fontSize: 18,
        fontColor: "#111"
      },
      legend: {
        display: false,
        position: "right",
        labels: {
          fontColor: "#333",
          fontSize: 14
        }
      }
    };

    return (
      <div className="right_col" role="main">
        <div className="">
          {/* <div className="page-title">
            <div className="title_left">
              <h3>User Profile</h3>
            </div>

            <div className="title_right">
              <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div className="input-group">
                  <input type="text" className="form-control" placeholder="Search for..." />
                  <span className="input-group-btn">
                    <button className="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div> */}

          <div className="clearfix"></div>

          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="x_panel">
                <div className="x_title">
                  <h2>User Report <small>Activity report</small></h2>
                  <ul className="nav navbar-right panel_toolbox">
                    <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                    </li>
                    <li className="dropdown">
                      <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                      <ul className="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a className="close-link"><i className="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div className="clearfix"></div>
                </div>
                <div className="x_content">
                  <div className="col-md-3 col-sm-3 col-xs-12 profile_left">
                    <div className="profile_img">
                      <div id="crop-avatar">
                        <img className="img-responsive avatar-view" src={this.state.user.imageIsUpdated =="true"?'./uploads/'+this.state.user.image_192:this.state.user.image_192} alt="Avatar" title="Change the avatar" style={{borderRadius: '50%'}} />
                      </div>
                    </div>
                    <h3>{this.state.user.real_name}</h3>

                    <ul className="list-unstyled user_data">
                      <li><i className="fa fa-map-marker user-profile-icon"></i> {this.state.user.email}
                      </li>

                      <li>
                        <i className="fa fa-briefcase user-profile-icon"></i> {this.state.user.title}
                      </li>

                      <li className="m-top-xs">
                        <i className="fa fa-external-link user-profile-icon"></i>
                         {this.state.user.phone}
                      </li>
                    </ul>

                    <a className="btn btn-success"><i className="fa fa-edit m-right-xs"></i>Edit Profile</a>
                    <br />

                    <h4>Skills</h4>
                    <ul className="list-unstyled user_data">
                      <li>
                        <p>Web Applications</p>
                        <div className="progress progress_sm">
                          <div className="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                        </div>
                      </li>
                      <li>
                        <p>Website Design</p>
                        <div className="progress progress_sm">
                          <div className="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                        </div>
                      </li>
                      <li>
                        <p>Automation & Testing</p>
                        <div className="progress progress_sm">
                          <div className="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                        </div>
                      </li>
                      <li>
                        <p>UI / UX</p>
                        <div className="progress progress_sm">
                          <div className="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                        </div>
                      </li>
                    </ul>

                  </div>
                  <div className="col-md-9 col-sm-9 col-xs-12">

                    <div className="profile_title">
                      <div className="col-md-6">
                        <h2>User Activity Report</h2>
                      </div>
                      <div className="col-md-6">
                        <div id="reportrange" className="pull-right" style={{ marginTop: '5px', background: '#fff', cursor: 'pointer', padding: '5px 10px', border: '1px solid #E6E9ED' }}>
                          <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                          <span>December 30, 2014 - January 28, 2015</span> <b className="caret"></b>
                        </div>
                      </div>
                    </div>
                    <div style={{ width: '100%' }}>
                      <Bar data={this.state.dataToSendAsProps} options={options} />
                    </div>

                    <div className="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" className="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Recent Notes</a>
                        </li>
                        <li role="presentation" className=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Projects Worked on</a>
                        </li>
                        <li role="presentation" className=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Other</a>
                        </li>
                      </ul>
                      <div id="myTabContent" className="tab-content">
                        <div role="tabpanel" className="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">


                          <ul className="messages">

                            {
                              this.state.NotesByUser.length != 0 ?
                                this.state.NotesByUser.slice(0, this.state.notesToShow).map((note) => {

                                  return (

                                    <li key={note._id}>
                                      <img src={this.state.user.imageIsUpdated =="true"?'./uploads/'+this.state.user.image_192:this.state.user.image_192} className="avatar" alt="Avatar" />
                                      <div className="message_date">
                                        <h3 className="date text-info"></h3>
                                        <p className="month">{FormatDate(note.createdAt)}</p>
                                      </div>
                                      <div className="message_wrapper">
                                        <h4 className="heading">{note.project.projectName}</h4>
                                        <blockquote className="message">{note.noteMessage}</blockquote>
                                        <br />
                                        <p className="url">
                                          <span className="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                          <i className="fa fa-paperclip"></i> Total logged hours on this project: {note.project.loggedHours}
                                        </p>
                                      </div>
                                    </li>


                                  )
                                })
                                : <h2 style={{ textAlign: 'center' }}>No notes to display!!</h2>
                            }
                            <div className="notes_more_buttons" style={{ borderTop: '1px solid rgb(221, 221, 221)', textAlign: 'center', paddingTop: '5px' }}>
                              <a type="button" className="btn btn-default btn-xs show_more_notes" onClick={this.showMore}>
                                {this.state.expanded ? (
                                  <span>Show less</span>
                                ) : (
                                    <span>Show more</span>
                                  )
                                }
                              </a>
                            </div>
                          </ul>


                        </div>
                        <div role="tabpanel" className="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">


                          <table className="data table table-striped no-margin">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Project Name</th>
                                <th>Role </th>
                                <th className="hidden-phone">Hours Spent</th>
                                <th>Contribution</th>
                              </tr>
                            </thead>
                            <tbody>

                              {
                                this.state.userProjectsDetails.length != 0 ?
                                  this.state.userProjectsDetails.slice(0, this.state.notesToShow).map((project, index) => {
                                    return (
                                      <tr key={project._id}>
                                        <td><div style={{ background: project.color, width: '17px', height: '21px', margin: '0 auto', float: 'left' }}></div></td>
                                        <td>{project.projectName}</td>
                                        <td>Deveint Inc</td>
                                        <td className="hidden-phone">{this.state.userProjectsFromApi[index].totalLoggedHours}</td>
                                        <td className="vertical-align-mid">
                                          <div className="progress">
                                            <div className="progress-bar progress-bar-success" data-transitiongoal={Math.round((((this.state.userProjectsFromApi[index].totalLoggedHours) / project.loggedHours) * 100))} aria-valuenow={Math.round((((this.state.userProjectsFromApi[index].totalLoggedHours) / project.loggedHours) * 100))} style={{width: Math.round((((this.state.userProjectsFromApi[index].totalLoggedHours) / project.loggedHours) * 100))+"%" }} ></div>
                                           
                                          </div>
                                        </td>
                                      </tr>

                                    )
                                  })
                                  : <h2 style={{ textAlign: 'center' }}>No projects to display!!</h2>
                              }
                               

                             

                            </tbody>
                          </table>
                          <strong style={{ textAlign: 'center', float: 'right' }}>Total logged hours: {this.state.userTotalLoggedHours} h. </strong>
                          <div className="notes_more_buttons" style={{ borderTop: '1px solid rgb(221, 221, 221)', textAlign: 'center', paddingTop: '5px' }}>
                              <a type="button" className="btn btn-default btn-xs show_more_notes" onClick={this.showMore}>
                                {this.state.expanded ? (
                                  <span>Show less projects</span>
                                ) : (
                                    <span>Show more Projects</span>
                                  )
                                }
                              </a>
                            </div>

                        </div>
                        <div role="tabpanel" className="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                    photo booth letterpress, commodo enim craft beer mlkshk </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }






}
export default SingleProfileContainer;