import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';

import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate';
import {FormatDateToDate} from '../../common/formatIsoStringDateToDateWithoutTime';


import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';
import TotalLoggedHours from '../../components/totalLoggedHours/totalLoggedHours';
import AllProjectStatsPie from '../../components/allProjectStatsPie/allProjectStatsPie';
import NotesByProject from '../../components/notesByProject/notesByProject';
import ProjectsTimeLineContainer from '../projectsTimeLineContainer/projectsTimeLineContainer';
import AllProjectStatsGraphe from '../../components/AllProjectStatsGraphe/AllProjectStatsGraphe';

// require('https://www.nikabot.com/f140b3875bbd3c6f9c251e348776b7b7f677957c.css?meteor_css_resource=true');
// require('https://www.nikabot.com/7d52e6b088a7910c951c86a2548f057b176f04bd.css?meteor_css_resource=true');

import Modal from 'react-modal';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

import { DateRangePicker } from 'react-date-range';
// var $ = require ('jquery');


const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};


class ProjectOverviewContainer extends React.Component {
    constructor() {
        super();


        this.state = {
            modalIsOpen: false, 
            startDate: '1992-05-21T14:08:01+01:00',
            endDate: '2992-05-21T14:08:01+01:00',
            selectedDate: {start: '1992-05-21T14:08:01+01:00', end: '2992-05-21T14:08:01+01:00'},
            messageFromServer: ''
           
        };
        
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleFilterClick = this.handleFilterClick.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    handleSelect(ranges){

        console.log('handling select ',ranges);
        this.setState({
            startDate: ranges.selection.startDate,
            endDate: ranges.selection.endDate
        });
        
		// {
		// 	selection: {
		// 		startDate: [native Date Object],
		// 		endDate: [native Date Object],
		// 	}
		// }
    }
    handleFilterClick(){
        console.log('state is', this.state);
        // this.forceUpdate();
        // window.location.reload();
        this.setState({
            selectedDate: {
                start: this.state.startDate,
                end: this.state.endDate
            }
        })
        this.closeModal();
    }


    //    componentDidMount(){
    //        $("#reportrange").on("apply.daterangepicker",function(a,b){
    //            console.log("my collected values are "+b.startDate.format("MMMM D, YYYY")+" to "+b.endDate.format("MMMM D, YYYY"))
    //        })

    //    }
    
    
 
    render() {
        const selectionRange = {
			startDate: new Date(),
			endDate: new Date(),
			key: 'selection',
		}
        return (
            <div className="right_col" role="main" >
                <div className="">
                    <div className="page-title">
                        <div className="title_left" style={{ textAlign: 'center', width: '100%' }}>
                        <div className="row">
                            <h3 style={{ textAlign: 'center' }}>Projects Overview</h3>
                            <div className="col-md-4 col-xs-12" style={{float: 'right', marginRight: '5px', marginBottom:'15px'}}>
                                {/* <div id="reportrange"  className="pull-right" style={{marginTop: '5px', background: '#fff', cursor: 'pointer', padding: '5px 10px', border: '1px solid #E6E9ED'}}>
                                  <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                  <span>December 30, 2014 - January 28, 2015</span> <b className="caret"></b>
                                </div> */}

                                <div  className="pull-right" onClick={this.openModal} style={{marginTop: '5px', background: '#fff', cursor: 'pointer', padding: '5px 10px', border: '1px solid #E6E9ED'}}>
                                  <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>
                               {
                                   this.state.selectedDate.start != '1992-05-21T14:08:01+01:00'?
                                   <span>{FormatDateToDate(this.state.selectedDate.start)} to {FormatDateToDate(this.state.selectedDate.end)}  </span> 
                               :
                                  <span> Filter By Date </span> 
                               }<b className="caret"></b>
                                </div>
                                

                              </div>
                        </div>
                        </div>
                    </div>
                     {/* add project modal begins */}
                     {this.state.messageFromServer == '' ?
                                <Modal
                                    isOpen={this.state.modalIsOpen}
                                    onAfterOpen={this.afterOpenModal}
                                    onRequestClose={this.closeModal}
                                    style={customStyles}
                                    contentLabel="Example Modal"
                                    ariaHideApp={false}
                                >


                                    <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                    
                                    <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Filter By Date</h2>
                                    <div className="ln_solid"></div>
                                    <div style={{marginLeft: '10%'}}>
                                    <DateRangePicker
                                    ranges={[selectionRange]}
                                    onChange={this.handleSelect}
                                    
                                />

                                    </div>
                                   
                                   
                                        
                                        <div className="ln_solid"></div>
                                        <div className="form-group" style={{ textAlign: 'center' }}>
                                            <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                                                <button className="btn btn-primary" type="reset" onClick={this.closeModal}>Cancel</button>
                                                <button type="submit" className="btn btn-success" onClick={this.handleFilterClick}>Filter</button>
                                            </div>
                                        </div>

                                
                                 

                                </Modal>
                                :
                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <strong> {this.state.messageFromServer}</strong>
                                </div>
                            }
                            {/* add project modal ends */}
                           

                    <div className="clearfix"></div>

                    <div className="row">
                        <div className="x_content">
                            <TotalLoggedHours selectedDate={this.state.selectedDate} />
                            <AllProjectStatsPie selectedDate={this.state.selectedDate} type="doghnut" />
                            <NotesByProject selectedDate={this.state.selectedDate} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="x_content">
                            {/* <ProjectsTimeLineContainer/> */}
                            <div className="col-md-12 col-sm-12 col-xs-12">
                        <AllProjectStatsGraphe  selectedDate={this.state.selectedDate} type="Bar" />
                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>

        )
    }






}
export default ProjectOverviewContainer;