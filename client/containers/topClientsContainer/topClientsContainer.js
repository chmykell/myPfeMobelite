import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';

import { FormatDate } from '../../common/formatIsoStringDateToDate';
import {FormatDateToDate} from '../../common/formatIsoStringDateToDateWithoutTime';






class TopClientsContainer extends  React.Component{

  constructor(props) {
    super(props);
    this.state = {
        notesList: [],
        notesToShow: 5
       
    };
    this.getNotesList = this.getNotesList.bind(this);
   
 
}



getNotesList(ev) {
  
    axios.get('/notes')
        .then(function (response) {
            ev.setState({ notesList: response.data });
           console.log('note list', response.data)
        });
    
}
componentWillMount(){
  this.getNotesList(this);
}


    render(){
        return(
            <div className="col-md-4 col-sm-12 col-xs-12">
            {/* <div class="x_panel"> */}
            <div>
              <div className="x_title">
                <h2>Recent Notes</h2>
                <ul className="nav navbar-right panel_toolbox">
                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                            </li>
                            {/* <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li> */}
                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                            </li>
                        </ul>
                <div className="clearfix"></div>
              </div>
              <ul className="list-unstyled top_profiles scroll-view">
               
                  {
                    this.state.notesList.reverse().slice(0, this.state.notesToShow).map((note, index) => {
               return (
                <li className="media event">
                  <a className="pull-left border-green profile_thumb user-profile">
                  
                  <img src={note.user.imageIsUpdated =="true"?'./uploads/'+note.user.image_192:note.user.image_192} alt="" />
                  </a>
                  <div className="media-body">
                    <a className="title" href="#">{note.user.first_name + ""+ note.user.last_name}</a>
                   <strong style={{float: 'right'}}> {FormatDate(note.createdAt)}</strong>
                    <p><strong>{note.project.projectName}</strong>  </p>
                    <p> <small>{note.noteMessage}</small>
                    </p>
                  </div>
                </li>
                  )

                })


                }
             
              
              
              </ul>
            </div>
          </div>
          // </div>
        )
    }
}
export default TopClientsContainer;
 