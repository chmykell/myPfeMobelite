import React from 'react';
import axios from 'axios';
import { LoadScript } from '../../common/loadScript';
import Modal from 'react-modal';
import { withRouter, Redirect } from 'react-router';
import '../../assets/select2/dist/css/select2.min.css';
import { FormatDate } from '../../common/formatIsoStringDateToDate'
var querystring = require('querystring');
import AccessDeniedPage from '../../components/AccessDeniedPage/AccessDeniedPage'
import '../../assets/bootstrap/dist/css/bootstrap.min.css';
import '../../assets/font-awesome/css/font-awesome.min.css';
import '../../assets/nprogress/nprogress.css';
import '../../assets/iCheck/skins/flat/green.css';
import '../../assets/datatables.net-bs/css/dataTables.bootstrap.min.css';
import '../../assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css';
import '../../assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css';
import '../../assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
import '../../assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css';
import '../../assets/build/css/custom.min.css';


const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};





class TeamSettingsContainer extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            slackUsersData: [],
            modalIsOpen: false,
            usersData: [],
            messageFromServer: '',
            comparisonResults: '',
            searchFor: '',
            differenceData: [],
            buttonsType: '',
            deletedUsersFromSlack: [],
            dataBaseUser: '',
            user: ''

        };
        this.handleRefreshUsersClick = this.handleRefreshUsersClick.bind(this);
        this.refreshSlackUsers = this.refreshSlackUsers.bind(this);
        this.getUsersFromDataBase = this.getUsersFromDataBase.bind(this);
        this.getUsersFromSlack = this.getUsersFromSlack.bind(this);
        this.insertAllSlackUsers = this.insertAllSlackUsers.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.updateUsers = this.updateUsers.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.compareUSersFromBothSides = this.compareUSersFromBothSides.bind(this);
        this.getSelectedUser = this.getSelectedUser.bind(this);


    } 
    componentDidMount() {
    }
    
    componentWillMount() {
        this.getUsersFromDataBase(this);
        this.getUsersFromSlack(this);
        var userObject = JSON.parse(localStorage.getItem('token'));
        if (userObject != null) {
          this.setState({ user: userObject.user });
        }
        setTimeout(() => {
          this.getSelectedUser(this);
        }, 100);

    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }
    getSelectedUser(ev) {
        // getting user from id passed as parameter
        axios.get('/users/bySlackId/' + this.state.user.id)
          .then(function (response) {
            ev.setState({ dataBaseUser: response.data });
            console.log('response from byslack users api', response.data);
    
          });
      }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    // trigred whenever refresh user button is clicked
    handleRefreshUsersClick() {
        this.refreshSlackUsers(this);
    }
    //get all users from database 
    getUsersFromDataBase(ev) {
        axios.get('/users')
            .then(function (response) {
                console.log('user data', response.data);
                ev.setState({ usersData: response.data });

            });
    }
    //get all userrs from slack to show first in team settings
    getUsersFromSlack(ev) {
        //    token generated from the slack api app
        var stringUrl = JSON.parse(localStorage.getItem('token')).access_token;
        // Getting the list of users from slack api
        var appGeneratedToken = 'xoxp-12121543780-317945634931-347418183556-717485855f9646314262b69194d601fd';
        axios.get('https://slack.com/api/users.list?token=' + appGeneratedToken + '&pretty=1')
            .then(function (response) {
                console.log('response slack list with token::::', response);
                ev.setState({ slackUsersData: response.data.members });

            });
    }

    //update users confirmation
    updateUsers() {
        this.insertAllSlackUsers(this);
    }

   
    insertAllSlackUsers(e) {
        let slackUsers = this.state.differenceData;
        let outside = this;
        for (let i = 0; i < slackUsers.length; i++) {

            //insert new user to database
            axios.post('/users',
                querystring.stringify({
                    first_name: slackUsers[i].profile.first_name,
                    last_name: slackUsers[i].profile.last_name,
                    real_name: slackUsers[i].real_name,
                    email: slackUsers[i].profile.email,
                    role: slackUsers[i].profile.title,
                    slack_id: slackUsers[i].id,
                    team_id: slackUsers[i].team_id,
                    is_admin: slackUsers[i].is_admin,
                    image_192: slackUsers[i].profile.image_192,
                    phone: slackUsers[i].profile.phone,
                    skype: slackUsers[i].profile.skype,
                    title: slackUsers[i].profile.title,
                    tz: slackUsers[i].tz,
                    tz_label: slackUsers[i].tz_label,
                    tz_offset: slackUsers[i].tz_offset
                }), {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }).then(function (response) {
                    console.log('added user', response);
                    outside.setState({ messageFromServer: response.data.message });
                });
            // console.log('i', slackUsers[i]);
        }
    }


    // handle search click to search for users simulating ctrl+f browser search
    handleSearchClick() {
        window.find(this.state.searchFor);
        // this.setState({searchFor: ''})
    }
    handleSearchTextChange(e) {
        if (e.target.name == "search") {
            this.setState({
                searchFor: e.target.value
            });

        }

    }

    //comparing users from slack and databse
    compareUSersFromBothSides() {
        let difference =[];
        let deletedUsersFromSlack= [];
        console.log('I am comparing users ')
        console.log('slack users data', this.state.slackUsersData);
        console.log('database users', this.state.usersData);

        // if(this.state.usersData.length > this.state.slackUsersData.length){

        //     for (let i = 0; i < this.state.usersData.length; i++) {
        //         let exist = false;
        //         for (let j = 0; j < this.state.slackUsersData.length; j++) {
        //             if (this.state.usersData[i].slack_id == this.state.slackUsersData[j].id) {
        //                 console.log('exists', i + this.state.usersData[i].slack_id); 
        //                 exist = true; 
        //             }
                  
        //         }
        //         if(exist == false){
        //             deletedUsersFromSlack.push(this.state.usersData[i]);
        //                 console.log('not found', this.state.usersData[i]);   
        //         }
        //     }
        //     setTimeout(() => {
        //         console.log('differnce tab', deletedUsersFromSlack)
        //         this.setState({deletedUsersFromSlack: deletedUsersFromSlack});
        //     }, 1000);
        // }
        // else if(this.state.usersData.length < this.state.slackUsersData.length){
        //     for (let i = 0; i < this.state.slackUsersData.length; i++) {
        //         let exist = false;
        //         for (let j = 0; j < this.state.usersData.length; j++) {
        //             if (this.state.slackUsersData[i].id == this.state.usersData[j].slack_id) {
        //                 console.log('exists', i + this.state.slackUsersData[i].id); 
        //                 exist = true; 
        //             }
                  
        //         }
        //         if(exist == false){
        //                 difference.push(this.state.slackUsersData[i]);
                        
        //                 console.log('not found', this.state.slackUsersData[i]);   
        //         }
        //     }
            
        //     setTimeout(() => {
        //         console.log('differnce tab', difference)
        //         this.setState({differenceData: difference});
        //     }, 1000);
        // }
        // else{
        //     for (let i = 0; i < this.state.slackUsersData.length; i++) {
        //         let exist = false;
        //         for (let j = 0; j < this.state.usersData.length; j++) {
        //             if (this.state.slackUsersData[i].id == this.state.usersData[j].slack_id) {
        //                 console.log('exists', i + this.state.slackUsersData[i].id); 
        //                 exist = true; 
        //             }
                  
        //         }
        //         if(exist == false){
        //                 difference.push(this.state.slackUsersData[i]);
                        
        //                 console.log('not found', this.state.slackUsersData[i]);   
        //         }
        //     }
            
        //     setTimeout(() => {
        //         console.log('differnce tab', difference)
        //         this.setState({differenceData: difference});
        //     }, 1000);
        //     for (let i = 0; i < this.state.usersData.length; i++) {
        //         let exist = false;
        //         for (let j = 0; j < this.state.slackUsersData.length; j++) {
        //             if (this.state.usersData[i].slack_id == this.state.slackUsersData[j].id) {
        //                 console.log('exists', i + this.state.usersData[i].slack_id); 
        //                 exist = true; 
        //             }
                  
        //         }
        //         if(exist == false){
        //             deletedUsersFromSlack.push(this.state.usersData[i]);
        //                 console.log('not found', this.state.usersData[i]);   
        //         }
        //     }
        //     setTimeout(() => {
        //         console.log('differnce tab', deletedUsersFromSlack)
        //         this.setState({deletedUsersFromSlack: deletedUsersFromSlack});

        //     }, 1000);

        // }
        for (let i = 0; i < this.state.slackUsersData.length; i++) {
            let exist = false;
            for (let j = 0; j < this.state.usersData.length; j++) {
                if (this.state.slackUsersData[i].id == this.state.usersData[j].slack_id) {
                    console.log('exists', i + this.state.slackUsersData[i].id); 
                    exist = true; 
                }
              
            }
            if(exist == false){
                    difference.push(this.state.slackUsersData[i]);
                    
                    console.log('not found', this.state.slackUsersData[i]);   
            }
        }
        
        setTimeout(() => {
            console.log('differnce tab', difference)
            this.setState({differenceData: difference});
        }, 1000);
        for (let i = 0; i < this.state.usersData.length; i++) {
            let exist = false;
            for (let j = 0; j < this.state.slackUsersData.length; j++) {
                if (this.state.usersData[i].slack_id == this.state.slackUsersData[j].id) {
                    console.log('exists', i + this.state.usersData[i].slack_id); 
                    exist = true; 
                }
              
            }
            if(exist == false){
                deletedUsersFromSlack.push(this.state.usersData[i]);
                    console.log('not found', this.state.usersData[i]);   
            }
        }
        setTimeout(() => {
            console.log('differnce tab', deletedUsersFromSlack)
            this.setState({deletedUsersFromSlack: deletedUsersFromSlack});

        }, 1000);


      
    }


    // calculating users and comparing them and refresh the list of users from slack 
    refreshSlackUsers(ev) {
        // this.getUsersFromDataBase(this);
        // this.getUsersFromSlack(this);
        var numberSlackUsers = this.state.slackUsersData.length;
        var numberAppUsers = this.state.usersData.length;
        console.log('1 222', numberAppUsers, numberSlackUsers);
        var comparingUsers = numberSlackUsers - numberAppUsers;
        // if (comparingUsers > 0) {
        //     // compare slackUsersData with appUsersData and adding users to add in state 
        //     this.compareUSersFromBothSides();
        //     this.setState({ comparisonResults: comparingUsers + " new Slack user(s) not registred ",
        //                     buttonsType: 'update' });
        //     this.openModal();
        // } else if (comparingUsers < 0) {
        //     // comparing slacUsers with appUsers and saving state data to delete
        //     this.compareUSersFromBothSides();
        //     this.setState({ comparisonResults: comparingUsers + " deleted user(s) from Slack  ",
        //                     buttonsType: 'close' });
        //     this.openModal();
        // }else if(comparingUsers == 0){
        //       // comparing slacUsers with appUsers and saving state data to delete
        //       this.compareUSersFromBothSides();
        //       this.setState({ comparisonResults: comparingUsers + " comparing data ... ",
        //                       buttonsType: 'close' });

        //                       setTimeout(() => {
        //                           if(this.state.differenceData.length == 0 && this.state.deletedUsersFromSlack.length ==0){
        //                             this.setState({ comparisonResults: " DataBase is up to date . ",
        //                             buttonsType: 'close' });
        //                           }else{
        //                             this.setState({ comparisonResults: "Slack users not registred in database",
        //                             buttonsType: 'update' });
        //                           }
        //                       }, 1050);
        //       this.openModal();
        // } 
        // else {
        //     //nothing to do except setting state for array of users to empty
        //     this.compareUSersFromBothSides();
        //     this.setState({ comparisonResults: "Databse is up to date!",
        //                         buttonsType: 'close' });
        //     this.openModal();
        // }
        this.compareUSersFromBothSides();
        this.setState({ comparisonResults:  <i className="fa fa-spinner fa-spin fa-3x"></i>,
                        buttonsType: 'close' });

                        setTimeout(() => {
                            if(this.state.differenceData.length == 0 && this.state.deletedUsersFromSlack.length ==0){
                              this.setState({ comparisonResults: " DataBase is up to date . ",
                              buttonsType: 'close' });
                            }else if(this.state.differenceData.length == 0){
                                this.setState({ comparisonResults: "All slack users are registred in databse.",
                                buttonsType: 'close' });
                            }
                            else{
                              this.setState({ comparisonResults: this.state.differenceData.length + "  Slack users not registred in database",
                              buttonsType: 'update' });
                            }
                        }, 1500);
        this.openModal();
    } 
    openUserProfile(slackUser) {
        // console.log('user is', slackUser);
        this.props.history.push('/profile/' + slackUser.id);
        // console.log('user is', this.props.history);

    }
    render() {
        if(this.state.dataBaseUser.is_admin =="true"){
            return (
                <div className="right_col" role="main">
                    <div className="">
                        <div className="page-title">
                            <div className="title_left">
                                <h3>Team Settings</h3>
                            </div>
    
                            <div className="title_right">
                                <div className="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Search for..." name="search" id="search" onChange={this.handleSearchTextChange} defaultValue={this.state.searchFor} />
                                        <span className="input-group-btn">
                                            <button className="btn btn-default" type="button" onClick={this.handleSearchClick}>Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div className="clearfix"></div>
    
                        <div className="row">
    
    
    
    
    
                            <div className="x_content">
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="x_panel">
                                        <div className="x_title">
                                            {/* <button type="button" classNameName="btn btn-info btn-sm">Add User </button> */}
                                            {/* <h4 style={{textAlign: 'center'}}>TeamOverview</h4> */}
                                            <button type="button" className="btn btn-round btn-info" onClick={this.handleRefreshUsersClick} style={{ textAlign: 'center', marginLeft: '27%' }}><i className="fa fa-refresh" style={{ marginRight: '10px' }}></i>Update Users from slack</button>
    
    
                                            <ul className="nav navbar-right panel_toolbox">
                                                <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                                                </li>
                                                {/* <li className="dropdown">
                                                                        <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                                                                        <ul className="dropdown-menu" role="menu">
                                                                            <li><a href="#">Settings 1</a>
                                                                            </li>
                                                                            <li><a href="#">Settings 2</a>
                                                                            </li>
                                                                        </ul>
                                                                    </li> */}
                                                <li><a className="close-link"><i className="fa fa-close"></i></a>
                                                </li>
                                            </ul>
                                            <div className="clearfix"></div>
                                        </div>
                                        <div className="x_content">
    
                                            <div className="row">
                                                <div className="col-md-12">
    
    
                                                    {/* modal goes here */}
                                                    {this.state.messageFromServer == '' ?
                                                        <Modal
                                                            isOpen={this.state.modalIsOpen}
                                                            onAfterOpen={this.afterOpenModal}
                                                            onRequestClose={this.closeModal}
                                                            style={customStyles}
                                                            contentLabel="Example Modal"
                                                            ariaHideApp={false}
                                                        >
    
    
                                                            <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
                                                            <h2 style={{ textAlign: 'center' }}> <span className="fa fa-plus" style={{ textAlign: 'center', marginRight: '10px' }} ></span> Storing Slack users to App</h2>
                                                            <div className="ln_solid"></div>
    
    
                                                            <div className="form-group">
                                                                <h2 style={{ textAlign: 'center', color: '#349a0e' }}>{this.state.comparisonResults} </h2>
                                                                {this.state.differenceData.map((user,index)=> {
                                                                return(<h4 key={index} style={{textAlign: 'center'}} >User :  {user.profile.real_name}</h4>)
                                                            })}
                                                            </div>
                                                            <div className="ln_solid"></div>
                                                            {this.state.deletedUsersFromSlack.length != 0?
                                                              
                                                              <div className="form-group">
                                                              <h4 style={{color: 'red', textAlign: 'center'}}>Deleted users from slack :</h4>
                                                                  {this.state.deletedUsersFromSlack.map((user,index)=> {
                                                                  return(<h4 key={index} style={{textAlign: 'center'}}>User :  {user.first_name + " " +user.last_name}</h4>)
                                                              })}
                                                              </div>
                                                              :<div className="ln_solid"></div>
                                                            }
                                                          
    
                                                            <div className="ln_solid"></div>
                                                            
                                                            <div className="form-group" style={{ textAlign: 'center' }}>
                                                                <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">
    
                                                                    <button className="btn btn-primary" onClick={this.closeModal}>cancel</button>
                                                                    {this.state.buttonsType =='update'? <button onClick={this.updateUsers} className="btn btn-success">Add users</button>
                                                                    :<h5>to check changes on slack</h5>
                                                                    }
                                                                    
                                                                </div>
                                                            </div>
    
    
    
                                                        </Modal>
                                                        :
                                                        <div className="alert alert-success alert-dismissible fade in" role="alert">
                                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <strong> {this.state.messageFromServer}</strong>
                                                        </div>
                                                    }
                                                    {/* modal ends here */}
    
    
    
                                                    {
                                                        this.state.slackUsersData.map((slackUser) => {
                                                            return (
    
                                                                <div className="col-md-4 col-sm-4 col-xs-12 profile_details" key={slackUser.id}>
                                                                    <div className="well profile_view">
                                                                        <div className="col-sm-12">
                                                                            <h4 className="brief"><i>{slackUser.profile.title}</i></h4>
                                                                            <div className="left col-xs-7">
                                                                                <h2>{slackUser.profile.real_name}</h2>
                                                                                <p><strong>Email: </strong> {slackUser.profile.email} </p>
                                                                                <ul className="list-unstyled">
                                                                                    <li><i className="fa fa-building"></i> skype:{slackUser.profile.skype} </li>
                                                                                    <li><i className="fa fa-phone"></i> Phone #: {slackUser.profile.phone}</li>
                                                                                </ul>
                                                                            </div>
                                                                            <div className="right col-xs-5 text-center">
                                                                                <img src={slackUser.profile.image_192} alt="" className="img-circle img-responsive" />
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-12 bottom text-center">
                                                                            <div className="col-xs-12 col-sm-6 emphasis">
                                                                                <p className="ratings">
                                                                                    <a>4.0</a>
                                                                                    <a href="#"><span className="fa fa-star"></span></a>
                                                                                    <a href="#"><span className="fa fa-star"></span></a>
                                                                                    <a href="#"><span className="fa fa-star"></span></a>
                                                                                    <a href="#"><span className="fa fa-star"></span></a>
                                                                                    <a href="#"><span className="fa fa-star-o"></span></a>
                                                                                </p> 
                                                                            </div>
                                                                            <div className="col-xs-12 col-sm-6 emphasis">
                                                                                <button type="button" className="btn btn-success btn-xs"> <i className="fa fa-user">
                                                                                </i> <i className="fa fa-comments-o"></i> </button>
                                                                                <button type="button" className="btn btn-primary btn-xs" onClick={() => this.openUserProfile(slackUser)}>
                                                                                    <i className="fa fa-user"> </i> View Profile
                                                                                 </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                            )
                                                        })
                                                    }
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    
    
    
                            </div>
                        </div>
                    </div>
                </div>
    
            );
        }
        else{
            return(
                <AccessDeniedPage />
            );
        }

           
        }
       
    }







export default withRouter(TeamSettingsContainer);