import React from 'react';
import { Doughnut, Pie } from 'react-chartjs-2';
import axios from 'axios';
import { withRouter, Redirect } from 'react-router';

class SingleProjectStatsContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            projectsData: [],
            dataToSendAsProps: [],
            totalLoggedHours: 0,
            generatedData: [],
            generatedLabels: [],
            expanded: false,
            projectsToShow: 5,
            selectedProject: []
        };

        

    }
    componentWillMount() {
       
        // setting data to send as props to stat component
       setTimeout(() => {
        var generatedLabels = [];
        var generatedData = [];
        var generatedColors = [];
        var allProjects = this.props.project;
        console.log('propssssssssssssss', this.props.project)
        
                generatedLabels.push(this.props.project.projectName);
                //calculating percentage of progress in each project
                var progressPercentage = ((this.props.project.loggedHours) / (this.props.project.estimatedDuration)) * 100;
                console.log('percentage calc', progressPercentage);
                //sending data percentage with  limit of two numbers after ","
                generatedData.push(progressPercentage.toFixed(0));
                generatedColors.push(this.props.project.color);

                generatedLabels.push('reamaining!')
                generatedData.push((100 - progressPercentage.toFixed(0)));
                generatedColors.push("#f7f7f7");

        

        const data = {
            labels: generatedLabels,
            datasets: [{
                data: generatedData,
                backgroundColor: generatedColors,
                hoverBackgroundColor: generatedColors
            }]
    };
    setTimeout(() => {
        this.setState({ dataToSendAsProps: data });
        this.setState({ generatedData: generatedData });
        this.setState({ generatedLabels: generatedLabels });
        console.log('peopsss data tosend', this.state.dataToSendAsProps)
        console.log('propssssssssssssss', this.props.project)
        this.setState({ selectedProject: this.props.project });

    }, 500);
       }, 200);



          
            
        


    }
    dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
  
    render() {
        var options = {
            responsive: true,
            title: {
                display: false,
                position: "top",
                text: "Active Projects",
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: false,
                position: "right",
                labels: {
                    fontColor: "#333",
                    fontSize: 14
                }
            }
        };
        return (
            <div className="col-md-6 col-sm-12 col-xs-12">
                <div className="x_panel">
                    <div className="x_title">
                        {/* <button type="button" className="btn btn-info btn-sm">Add User </button> */}
                        <span> Project stats</span>
                        <ul className="nav navbar-right panel_toolbox">
                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                            </li>
                            {/* <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li> */}
                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                    <div className="x_content">

                        <div className="col-md-7 col-sm-12 col-xs-8 no-padding" style={{marginLeft: '20%'}}>
                            <div style={{ marginTop: '10%' }}>
                                <Pie data={this.state.dataToSendAsProps} options={options} />
                            </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div className="table-responsive" style={{ overflow: 'visible' }}>
                                <table cellPadding="1" cellSpacing="1" className="table table-condensed table-striped pie_chart_table" style={{ marginBottom: '0 !important' }}>
                                    <thead>
                                        <th style={{ padding: '0 10px 5px 5px' }}>Project</th>
                                        <th><i className="fa fa-clock-o" data-toggle="simple_tooltip" title="Total logged hours" style={{ padding: '0 10px 5px 5px' }}></i></th>
                                        <th><i className="fa fa-tachometer" data-toggle="simple_tooltip" title="Estimated duration" style={{ padding: '0 10px 5px 5px' }}></i></th>
                                        <th><i className="fa fa-user" data-toggle="simple_tooltip" title="Number of team members" style={{ padding: '0 10px 5px 5px' }}></i></th>
                                        <th>&nbsp;</th>
                                    </thead>
                                    <tbody>
                                                <tr key={this.state.selectedProject._id} className="highlight_pie">
                                                    <td>
                                                        
                                                            <i className="fa fa-square" style={{ color: this.state.selectedProject.color, marginRight: '4px' }}></i>{this.state.selectedProject.projectName}
                                                        
                                                    </td>
                                                    <td style={{ width: '10%' }}>
                                                        {this.state.selectedProject.loggedHours}
                                                    </td>
                                                    <td style={{ width: '10%' }}>
                                                        {this.state.selectedProject.estimatedDuration}
                                                    </td>
                                                   
                                                    <td style={{ width: '10%' }}>
                                                        911
                                           </td>
                                                  
                                                </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(SingleProjectStatsContainer);