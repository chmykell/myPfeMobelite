import React from 'react';
import { Doughnut, Pie } from 'react-chartjs-2';
import axios from 'axios';
import { withRouter, Redirect } from 'react-router';
import { FormatDate } from '../../common/formatIsoStringDateToDate'



class ProjectSummaryContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedProject: []
        };

    }
    componentWillMount(){
        setTimeout(() => {
            this.setState({selectedProject: this.props.project});
            console.log('projectttttttt', this.state.selectedProject)
        }, 100);
    }


    render() {
        return (
            <div className="col-md-3 col-sm-12 col-xs-12 text-center" style={{fontSize: '115%'}}>
                <div className="x_panel">
                    {/* <div className="x_title">
                        <span> Project Summary</span>
                        <ul className="nav navbar-right panel_toolbox">
                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                            </li>
                            
                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div className="clearfix"></div>
                    </div> */}
                    <div className="x_content" >
                        <div className="small" style={{ textTransform: 'uppercase', lineHeight: '30px', fontWeight: '600' }}>Project Summary</div>
                        <div>
                            <div style={{ marginTop: '30px', fontSize: '18px' }}>
                                <i className="fa fa-male"></i><i className="fa fa-male"></i><i className="fa fa-male"></i>
                            </div>
                            <h1 style={{ marginTop: '10px' }}>{this.state.selectedProject.loggedHours}</h1>
                            <small className="font-bold">Total hours</small>
                        </div>
                        <div className="small m-t-xl">
                            <i className="fa fa-calendar"></i> Start date: {FormatDate(this.state.selectedProject.createdAt)}<br />
                            <i className="fa fa-clock-o"></i>Estimated Duration: {this.state.selectedProject.estimatedDuration} <br />
                            <i className="fa fa-coffee"></i> Time Off Hours:  {this.state.selectedProject.timeOffHours} <br />
                            <i className="fa fa-users"></i> Client : {this.state.selectedProject.client != null && this.state.selectedProject.client.firstName +" "+ this.state.selectedProject.client.lastName }<br />
                            <i className="fa fa-tasks"></i> Type :   {this.state.selectedProject.type}<br />
                            Color :   <i className="fa fa-battery-full" style={{color: this.state.selectedProject.color}}></i> <br />
                </div>
                    </div>
                </div>
            </div>


        )
    }
}
export default withRouter(ProjectSummaryContainer);
