import React from 'react';
import axios from 'axios';



class UsersPerProjectTimeLine extends React.Component {


    render() {
        return (
            <div className="userscolumn no-padding">
                <div className="row user show-grid">
                    <a className="user_link" href="/demo/user/john">
                        <div className="avatar" style={{ backgroundImage: "url('../../imgs/img.jpg')" }}></div>
                    </a>
                    <div className="name_container">
                        <div className="name">
                            <a className="user_link" href="/demo/user/john">
                                John
        </a>
                        </div>
                        <div className="input-group-btn property_select_container" style={{ fontSize: 'inherit !important' }}>
                            <a className="property_trigger" data-toggle="dropdown" type="button" aria-expanded="false">
                                <small className="font-bold" style={{ color: '#3498db' }}><span className="properties_string">Development</span> <span className="caret"></span></small>
                            </a>
                            <ul className="dropdown-menu property_selector">
                                <li>
                                    <a href="#" data-id="563898e4e4b0c3d64132e30f">
                                        <i className="fa fa-square fa-fw" style={{ color: '#62cb31' }}></i>
                                        &nbsp;Design
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="56389908e4b0c3d64132e311">
                                        <i className="fa fa-square fa-fw" style={{ color: '#3498db' }}></i>
                                        &nbsp;Development
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="56389915e4b0c3d64132e312">
                                        <i className="fa fa-square fa-fw" style={{ color: '#ffb606' }}></i>
                                        &nbsp;Marketing
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="5638992ce4b0c3d64132e313">
                                        <i className="fa fa-square fa-fw" style={{ color: '#e74c3c' }}></i>
                                        &nbsp;User testing
            </a>
                                </li>
                                <li className="divider"></li>
                                <li><a href="#" data-edit="true"><i className="fa fa-plus-circle fa-fw"></i>&nbsp; Add/edit roles</a></li>
                            </ul>
                        </div>
                        <div className="input-group-btn property_select_container" style={{ fontSize: 'inherit !important' }}>
                            <a className="property_trigger" data-toggle="dropdown" type="button" aria-expanded="false">
                                <small className="font-bold font-trans"><span className="properties_string"> groups</span> <span className="caret"></span></small>
                            </a>
                            <ul className="dropdown-menu property_selector">
                                <li><a href="#" data-edit="true"><i className="fa fa-plus-circle fa-fw"></i>&nbsp; Add/edit groups</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="row user show-grid">
                    <a className="user_link" href="/demo/user/pedro.cardoso">
                        <div className="avatar" style={{ backgroundImage: "url('../../imgs/img.jpg')" }}></div>
                    </a>
                    <div className="name_container">
                        <div className="name">
                            <a className="user_link" href="/demo/user/pedro.cardoso">
                                Pedro Cardoso
        </a>
                        </div>
                        <div className="input-group-btn property_select_container" style={{ fontSize: 'inherit !important' }}>
                            <a className="property_trigger" data-toggle="dropdown" type="button" aria-expanded="false">
                                <small className="font-bold font-trans"><span className="properties_string">&lt;select role&gt;</span> <span className="caret"></span></small>
                            </a>
                            <ul className="dropdown-menu property_selector">
                                <li>
                                    <a href="#" data-id="563898e4e4b0c3d64132e30f">
                                        <i className="fa fa-square fa-fw" style={{ color: '#62cb31' }}></i>
                                        &nbsp;Design
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="56389908e4b0c3d64132e311">
                                        <i className="fa fa-square fa-fw" style={{ color: '#3498db' }}></i>
                                        &nbsp;Development
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="56389915e4b0c3d64132e312">
                                        <i className="fa fa-square fa-fw" style={{ color: '#ffb606' }}></i>
                                        &nbsp;Marketing
            </a>
                                </li>
                                <li>
                                    <a href="#" data-id="5638992ce4b0c3d64132e313">
                                        <i className="fa fa-square fa-fw" style={{ color: '#e74c3c' }}></i>
                                        &nbsp;User testing
            </a>
                                </li>
                                <li className="divider"></li>
                                <li><a href="#" data-edit="true"><i className="fa fa-plus-circle fa-fw"></i>&nbsp; Add/edit roles</a></li>
                            </ul>
                        </div>
                        <div className="input-group-btn property_select_container" style={{ fontSize: 'inherit !important' }}>
                            <a className="property_trigger" data-toggle="dropdown" type="button" aria-expanded="false">
                                <small className="font-bold font-trans"><span className="properties_string"> groups</span> <span className="caret"></span></small>
                            </a>
                            <ul className="dropdown-menu property_selector">
                                <li><a href="#" data-edit="true"><i className="fa fa-plus-circle fa-fw"></i>&nbsp; Add/edit groups</a></li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
}


export default UsersPerProjectTimeLine;