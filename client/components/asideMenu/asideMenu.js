import React from 'react';
import { LoadScript } from '../../common/loadScript';
import axios from 'axios';
class AsideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      dataBaseUser: '' 

    }
    this.handleClickAllProjects = this.handleClickAllProjects.bind(this);
    this.handleClickHome = this.handleClickHome.bind(this);
    this.logOut = this.logOut.bind(this);
    this.getSelectedUser = this.getSelectedUser.bind(this);
  }
  //this handles the click event on manage all projects link

  handleClickAllProjects() {
    this.props.history.push('/allProjects');
  }
  //this handles the click event on home title or logo
  handleClickHome() {
    this.props.history.push('/home');
  }
  getSelectedUser(ev) {
    // getting user from id passed as parameter
    axios.get('/users/bySlackId/' + this.state.user.id)
      .then(function (response) {
        ev.setState({ dataBaseUser: response.data });
        console.log('response from byslack users api', response.data);

      });
  }
  componentWillMount() {
    var userObject = JSON.parse(localStorage.getItem('token'));
    if (userObject != null) {
      this.setState({ user: userObject.user });
    }
    setTimeout(() => {
      this.getSelectedUser(this);
    }, 100);


  }
  componentDidMount() {

    // LoadScript('../../assets/jquery/dist/jquery.min.js');
    // LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
    // LoadScript('../../assets/fastclick/lib/fastclick.js');
    // LoadScript('../../assets/nprogress/nprogress.js');
    // LoadScript('../../assets/build/js/custom.min.js');
  }
  logOut() {
    localStorage.removeItem('token');
    window.location.reload();
  }

  render() {
    return (
      <div className="col-md-3 left_col menu_fixed">
        <div className="left_col scroll-view">
          <div className="navbar nav_title" style={{ border: 0 }}>
            <a href="#home" className="site_title" style={{ paddingLeft: 0 }} onClick={this.handleClickHome}><i className="fa fa-home" style={{ marginLeft: 16 }}></i> <span>TeamBoard</span></a>
          </div>
          <div className="clearfix"></div>
          <div className="profile clearfix">
            <div className="profile_pic">
              <img src={this.state.dataBaseUser.imageIsUpdated =="true"?'./uploads/'+this.state.dataBaseUser.image_192:this.state.dataBaseUser.image_192} alt="..." className="img-circle profile_img" />
            </div>
            <div className="profile_info">
              <span>Welcome,</span>
              <h2>{this.state.user.name != null && this.state.user.name}</h2>
            </div>
          </div>


          <br />
          <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
            <div className="menu_section">
              <h3>General</h3>
              <ul className="nav side-menu">
                <li><a href="#myTasks"><i className="fa fa-tasks"></i> My Tasks</a></li>
                {this.state.dataBaseUser.is_admin == "true" && <li><a href="#allTasks"><i className="fa fa-tasks"></i> All Tasks</a></li>}
                <li><a><i className="fa fa-bar-chart-o"></i> Reports <span className="fa fa-chevron-down"></span></a>
                  <ul className="nav child_menu">
                    <li><a href="#teamOverview">Team Overview</a></li>
                    <li><a href="#projectsOverview">Projects Overview</a></li>
                  </ul>
                </li>
                <li><a><i className="fa fa-edit"></i> Manage <span className="fa fa-chevron-down"></span></a>
                  <ul className="nav child_menu">
                    {this.state.dataBaseUser.is_admin == "true" && <li><a href="#team">Team Members</a></li>}
                    <li><a href="#allClients">Clients</a></li>
                    <li><a href="#allProjects" onClick={this.handleClickAllProjects}>All Projects</a></li>
                  </ul>
                </li>
                <li><a><i className="fa fa-desktop"></i> Updates </a>

                </li>
                <li><a><i className="fa fa-cogs"></i> Settings <span className="fa fa-chevron-down"></span></a>
                  <ul className="nav child_menu">
                    <li><a href="#mySettings">My Settings</a></li>
                    {this.state.dataBaseUser.is_admin == "true" && <li><a href="#settings">Team Settings</a></li>}
                    <li><a href="#login" onClick={this.logOut}>Log out</a></li>
                  </ul>
                </li>
              </ul> 
            </div>


          </div>

          <div className="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span className="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span className="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span className="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
              <span className="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
        </div>
      </div>
    )
  }
}


export default AsideMenu;