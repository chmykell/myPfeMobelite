import React from 'react';

import { LoadScript } from '../../common/loadScript';

class MyDatePicker extends React.Component {
    constructor() {
        super();
        this.state = { selectedDate: '' };

    }
    componentWillMount(){
        LoadScript('../../assets/initialiseMyDateRangePicker.js');
    }

    render() {
        return (
            <div id="reportrange" className="pull-right" style={{ background: '#fff', cursor: 'pointer', padding: '5px 10px', border: '1px solid #ccc', width: '100%' }}>
                <i className="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b className="caret"></b>
            </div>
        )
    }
}


export default MyDatePicker;