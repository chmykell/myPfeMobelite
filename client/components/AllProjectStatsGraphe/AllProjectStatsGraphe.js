import React from 'react';
import { Doughnut, Pie, Bar, Polar, Bubble, Line, Radar } from 'react-chartjs-2';
import axios from 'axios';
import { withRouter, Redirect } from 'react-router';

class AllProjectStatsGraphe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectsData: [],
            dataToSendAsProps: [],
            totalLoggedHours: 0,
            generatedData: [],
            generatedLabels: [],
            expanded: false,
            projectsToShow: 5,
            projectByDateSelected: [],
            listIdsOfActiveProjects: [],
            filteredTasksWS: [],
            typeGrpahe: 'pie'
        };

        this.getProjects = this.getProjects.bind(this);
        this.getProjectsTotalLoggedHours = this.getProjectsTotalLoggedHours.bind(this);
        this.showMore = this.showMore.bind(this);
        this.getListProjectsBtListIds = this.getListProjectsBtListIds.bind(this);
        this.isEqual = this.isEqual.bind(this);
    }
   
    getFilteredTasksHours(ev){
        var totActiveProject=0;
        var totalLoggedHours=0;
        var listIdsOfActiveProjects = [];
        
        axios.get('/tasks/hoursByProject/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
        .then(function (response) {
            console.log('hours by project response', response)
            for (let i in response.data) {
                //testing if a project has some loggedHours because if not no calculation needed
                if (response.data[i].totalLoggedHours != 0) {
                    totActiveProject++;
                    totalLoggedHours = totalLoggedHours + response.data[i].totalLoggedHours;
                    listIdsOfActiveProjects.push(response.data[i]._id);
                }
                else {
                    i++;
                }
            }
            setTimeout(() => {
                ev.setState({ totActiveProject: totActiveProject,
                    totalLoggedHours: totalLoggedHours,
                    listIdsOfActiveProjects: listIdsOfActiveProjects,
                    filteredTasksWS: response.data
                    

                    });

            }, 500);
        });
    }
    getListProjectsBtListIds(ev){
       
        var listFilteredProjects = [];
        console.log('listtstttttttttttttttttttttttt idsssssssss:::::::', this.state.listIdsOfActiveProjects);
        for (let index = 0; index < this.state.listIdsOfActiveProjects.length; index++) {
            for (let j = 0; j < this.state.projectsData.length; j++) {
                if(this.state.listIdsOfActiveProjects[index] == this.state.projectsData[j]._id){

                    listFilteredProjects.push(this.state.projectsData[j]);
                }
                
            }
        }
        setTimeout(() => {
            console.log('listFilteredProjects', listFilteredProjects);
            console.log('list state', this.state);

            this.setState({projectByDateSelected: listFilteredProjects});
        }, 700);

    }
    componentWillReceiveProps(nextProps) {
        if(this.props.selectedDate.start != nextProps.selectedDate.start && this.props.selectedDate.end != nextProps.selectedDate.end) // Check if it's a new user, you can also use some unique, like the ID
        {
            // this.getProjectsTotalLoggedHours(this);

            this.setState({filteredTasksWS : []})
            this.getFilteredTasksHours(this);
           
          //  this.getProjects(this);
          //we have to get the project ids and search them in the database
          
            setTimeout(() => {
                this.getListProjectsBtListIds(this);

              
            }, 1000);
        // setting data to send as props to stat component

        setTimeout(() => {
          console.log('thiss state project selected equals to', this.state.projectByDateSelected);
            var allProjects = this.state.projectByDateSelected;
            var generatedLabels = [];
            var generatedData = [];
            var generatedColors = [];
            
            if(this.state.projectByDateSelected.length > 0){
                for (let i in this.state.projectByDateSelected) {
                
                   
                       
                                     //testing if a project has some loggedHours because if not no calculation needed
                            if (this.state.projectByDateSelected[i].loggedHours != 0) {
                                generatedLabels.push(this.state.projectByDateSelected[i].projectName);
                                //calculating percentage of progress in each project
                                var progressPercentage = ((this.state.filteredTasksWS[i].totalLoggedHours) / this.state.totalLoggedHours) * 100;
                                console.log('percentage calc', progressPercentage);
                                //sending data percentage with  limit of two numbers after ","
                                generatedData.push(progressPercentage.toFixed(1));
                                generatedColors.push(this.state.projectByDateSelected[i].color);
                            }
                           

                          
                          else {
                            i++;
                        
                        
                    }

            }
            const data = {
                labels: generatedLabels,
                datasets: [{
                    data: generatedData,
                    backgroundColor: generatedColors,
                    hoverBackgroundColor: generatedColors
                }]
            };

            this.setState({ dataToSendAsProps: data });
            this.setState({ generatedData: generatedData });
            this.setState({ generatedLabels: generatedLabels });
            this.setState({ projectsData: this.state.projectByDateSelected });
           
            
            console.log('peopsss data tosend', this.state.dataToSendAsProps)

            }
           
           
        }, 1900);

        }
    } 
    componentWillMount() {
        this.setState({typeGrpahe: this.props.type});
        this.getProjectsTotalLoggedHours(this);
        this.getProjects(this);
        // setting data to send as props to stat component
        setTimeout(() => {
            var generatedLabels = [];
            var generatedData = [];
            var generatedColors = [];
            var allProjects = this.state.projectsData;
            for (let i in allProjects) {
                //testing if a project has some loggedHours because if not no calculation needed
                if (allProjects[i].loggedHours != 0) {
                    generatedLabels.push(allProjects[i].projectName);
                    //calculating percentage of progress in each project
                    var progressPercentage = ((allProjects[i].loggedHours) / this.state.totalLoggedHours) * 100;
                    console.log('percentage calc', progressPercentage);
                    //sending data percentage with  limit of two numbers after ","
                    generatedData.push(progressPercentage.toFixed(1));
                    generatedColors.push(allProjects[i].color);
                }
                else {
                    i++;
                }



            }

            const data = {
                labels: generatedLabels,
                datasets: [{
                    data: generatedData,
                    backgroundColor: generatedColors,
                    hoverBackgroundColor: generatedColors
                }]
            };

            this.setState({ dataToSendAsProps: data });
            this.setState({ generatedData: generatedData });
            this.setState({ generatedLabels: generatedLabels });
            console.log('peopsss data tosend', this.state.dataToSendAsProps)
        }, 1000);


    }
    dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
    showMore() {
        this.state.projectsToShow === 5 ? (
          this.setState({ projectsToShow: this.state.projectsData.length, expanded: true })
          ) : (
            this.setState({ projectsToShow: 5, expanded: false })
          )
        }

    getProjects(ev) {
      
        axios.get('/projects/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
            .then(function (response) {
                ev.setState({ projectsData: response.data });
                console.log('responseProjects', response.data)
            });
        
    }
    openProjectDetails(project) {
        // console.log('user is', slackUser);
        this.props.history.push('/project/' + project._id);
        // console.log('user is', this.props.history);
    
    }

    getProjectsTotalLoggedHours(ev) {
        
        axios.get('/projects/loggedHours/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
            .then(function (response) {
                ev.setState({ totalLoggedHours: response.data[0].totalLoggedHours });
                console.log('reponse is sisisi', response.data);


            });
        
    }
    // comparing two arrays begins
     isEqual(value, other) {

        // Get the value type
        var type = Object.prototype.toString.call(value);
    
        // If the two objects are not the same type, return false
        if (type !== Object.prototype.toString.call(other)) return false;
    
        // If items are not an object or array, return false
        if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;
    
        // Compare the length of the length of the two items
        var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
        var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
        if (valueLen !== otherLen) return false;
    
        // Compare two items
        var compare = function (item1, item2) {
    
            // Get the object type
            var itemType = Object.prototype.toString.call(item1);
    
        
    
                // If the two items are not the same type, return false
                if (itemType !== Object.prototype.toString.call(item2)) return false;
    
                // Else if it's a function, convert to a string and compare
                // Otherwise, just compare
                if (itemType === '[object Function]') {
                    if (item1.toString() !== item2.toString()) return false;
                } else {
                    if (item1 !== item2) return false;
                }
    
            
        };
    
        // Compare properties
        if (type === '[object Array]') {
            for (var i = 0; i < valueLen; i++) {
                if (compare(value[i], other[i]) === false) return false;
            }
        } else {
            for (var key in value) {
                if (value.hasOwnProperty(key)) {
                    if (compare(value[key], other[key]) === false) return false;
                }
            }
        }
    
        // If nothing failed, return true
        return true;
    
    }
    // comparing two arrays ends
    

    render() {
        var options = {
            responsive: true,
            title: {
                display: false,
                position: "top",
                text: "Active Projects",
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: false,
                position: "right",
                labels: {
                    fontColor: "#333",
                    fontSize: 14
                }
            }
        };
        return (
            
                <div className="x_panel">
                    <div className="x_title">
                        {/* <button type="button" className="btn btn-info btn-sm">Add User </button> */}
                        <span>All Projects stats</span>
                        <ul className="nav navbar-right panel_toolbox">
                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                            </li>
                            {/* <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li> */}
                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                    <div className="x_content">

                        <div className="col-lg-12 col-md-12 col-sm-12 no-padding" >
                            <div style={{ marginTop: '10%' }}>
                            {/* {this.props.type == "bar"?<Bar data={this.state.dataToSendAsProps} options={options} />: <Doughnut data={this.state.dataToSendAsProps} options={options} />} */}
                        {this.props.type == "Bar"  &&  <Bar data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Doughnut" &&  <Doughnut data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Pie"  &&  <Pie data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Polar"  &&  <Polar data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Line"  &&  <Line data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Radar"  &&  <Radar data={this.state.dataToSendAsProps} options={options} /> }
                        {this.props.type == "Bubble"  &&  <Bubble data={this.state.dataToSendAsProps} options={options} />  }
                            </div>
                        </div>
                      
                    </div>
                </div>
            
        )
    }
}


export default withRouter(AllProjectStatsGraphe);