import React from 'react';
import axios from 'axios';


class TotalLoggedHours extends React.Component {
    constructor(props){
        super(props);
         this.state={
             totalLoggedHours: 0,
             totActiveProject: 0,
             timeOffHours: 0,
             totUsers: 0

            };
            this.getFilteredTasksHours = this.getFilteredTasksHours.bind(this);
            this.getProjects = this.getProjects.bind(this);
            this.getProjectsTotalLoggedHours = this.getProjectsTotalLoggedHours.bind(this);
       
      }
      componentWillMount(){
          this.getProjectsTotalLoggedHours(this);
          this.getProjects(this);
          this.getUsers(this);
      }
      componentWillReceiveProps(nextProps) {
        if(this.props.selectedDate.start != nextProps.selectedDate.start) // Check if it's a new user, you can also use some unique, like the ID
        {
            // this.getProjectsTotalLoggedHours(this);
            this.getFilteredTasksHours(this);
            this.getProjects(this);
        }
    } 
    getFilteredTasksHours(ev){
        var totActiveProject=0;
        var totalLoggedHours=0;
        axios.get('/tasks/hoursByProject/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
        .then(function (response) {
            console.log('hours by project response', response)
            for (let i in response.data) {
                //testing if a project has some loggedHours because if not no calculation needed
                if (response.data[i].totalLoggedHours != 0) {
                    totActiveProject++;
                    totalLoggedHours = totalLoggedHours + response.data[i].totalLoggedHours;
                  //  totLoggedHours=totLoggedHours+ response.data[i].loggedHours
                }
                else {
                    i++;
                }
            }
            setTimeout(() => {
                ev.setState({ totActiveProject: response.data.length,
                    totalLoggedHours: totalLoggedHours});
            }, 400);
        });
    }
      
        getProjectsTotalLoggedHours(ev){
           
                console.log('this is where the errrurururu is', this.props.selectedDate.startDate, this.props.selectedDate)
            axios.get('/projects/loggedHours/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
            .then(function (response) {
                console.log('totalLoggedHours response', response)
                ev.setState({ totalLoggedHours: response.data[0].totalLoggedHours });
                ev.setState({ timeOffHours: response.data[0].timeOffHours });
                console.log('totalLoggedHours', response)
            });
       
        }
        getProjects(ev){ 
            var totActiveProject=0;
            // var totLoggedHours= 0;
           
                axios.get('/projects/'+this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
                .then(function (response) {
                    // ev.setState({ projectsData: response.data });
                    // console.log('responseProjects', response.data)
                    for (let i in response.data) {
                        //testing if a project has some loggedHours because if not no calculation needed
                        if (response.data[i].loggedHours != 0) {
                            totActiveProject++;
                          //  totLoggedHours=totLoggedHours+ response.data[i].loggedHours
                        }
                        else {
                            i++;
                        }
                    }
                    setTimeout(() => {
                        ev.setState({ totActiveProject: totActiveProject});
                    }, 400);
                }
            );
           
          
           
        }
        getUsers(ev){
            var totUsers=0;
            axios.get('/users')
            .then(function (response) {
                // ev.setState({ projectsData: response.data });
                // console.log('responseProjects', response.data)
                for (let i in response.data) {
                    //testing if a project has some loggedHours because if not no calculation needed
                   totUsers++;
                }
                setTimeout(() => {
                    ev.setState({ totUsers: totUsers });
                }, 400);
            }
        );
           
        }
      

      render(){
        return (
            <div className="col-md-3 col-sm-12 col-xs-12">
            <div className="x_panel">
                <div className="x_title">
                {/* <button type="button" className="btn btn-info btn-sm">Add User </button> */}
               <span>Hours Logged</span>
                    <ul className="nav navbar-right panel_toolbox">
                        <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                        </li>
                        {/* <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li> */}
                        <li><a className="close-link"><i className="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div className="clearfix"></div>
                </div>
                <div className="x_content">
                   
                <div className="hr-line-dashed"></div>
          <div className="row m-t-md">
              <div className="col-lg-6 col-md-6 col-sm-6">
                <h3 className="font-bold" style={{marginRight:'5px !important'}}><i className="fa fa fa-clock-o"></i> {this.state.totalLoggedHours} </h3>
                Hours logged
              </div>

              <div className="col-lg-6 col-md-6 col-sm-6">
                <h3 className="font-bold" style={{marginRight:'5px !important'}}><i className="fa fa-ban"></i> {this.state.timeOffHours}</h3>
                Time Off hours
              </div>

          </div>
          <div className="row m-t-md">
              <div className="col-lg-6 col-md-6 col-sm-6">
                <h3 className="font-bold" style={{marginRight:'5px !important'}}><i className="fa fa-user"></i> {this.state.totUsers}</h3>
                People tracked
              </div>

              <div className="col-lg-6 col-md-6 col-sm-6">
                <h3 className="font-bold" style={{marginRight:'5px !important'}}><i className="fa fa-folder-o"></i> {this.state.totActiveProject}</h3>
                Projects active
              </div>

          </div>

                </div>
            </div>
        </div>
        )
    }
}


export default TotalLoggedHours;