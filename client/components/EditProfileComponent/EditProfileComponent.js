import React from 'react';
import { withRouter, Redirect } from 'react-router';
import axios from 'axios';

import Modal from 'react-modal';

var querystring = require('querystring');
var validator = require("email-validator");


const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};


class EditProfileComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: this.props.modalIsOpen,
            messageFromServer: '',
            user: '',
            whatIdo: '',
            skype: '',
            phone: '',
            email: '',
            first_name: '',
            last_name: '',
            image: '',
            selectedFile: '',
            newFilemName: '',
            imageToDisplay: '',
            imageIsUpdated: ''
        };
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleEditProfileClick = this.handleEditProfileClick.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleFileInputChange = this.handleFileInputChange.bind(this);
        this.updateUser = this.updateUser.bind(this);


    }
    componentWillMount(){
        this.setState({user: this.props.user})
    }
    componentDidMount(){
        this.setState({
            first_name: this.state.user.first_name,
            last_name: this.state.user.last_name,
            email: this.state.user.email,
            phone: this.state.user.phone,
            skype: this.state.user.skype,
            whatIdo: this.state.user.role,
            image: this.state.user.image_192,
            imageIsUpdated: this.state.user.imageIsUpdated
        })
        setTimeout(() => {
            let imageToDisplay ="./uploads/"+this.state.image;
            if(this.state.imageIsUpdated == "true"){
                this.setState({imageToDisplay: imageToDisplay})
                console.log('image ', imageToDisplay)
            }else{
                this.setState({imageToDisplay: this.state.image});
                console.log('image ', imageToDisplay)
            }
        }, 100);
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }
    //file input handle changes
    handleFileInputChange(event){
        console.log('event ', event.target);
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({image: e.target.result});
                console.log('state', e.target.result)
            };
            reader.readAsDataURL(event.target.files[0]);
            console.log('file[0)',event.target.files[0])
            this.setState({selectedFile: event.target.files[0]});
        }
       
        
    }
    handleEditProfileClick(e){
        e.preventDefault();
        console.log('edited the profile', this.state);
        let formData = new FormData();
        
      
      formData.append('selectedFile', this.state.selectedFile);

      axios.post('/upload', formData)
        .then((result) => {
          // access results...
          console.log('results from post /', result);
          console.log('results from post /', result.data);
          this.setState({image: result.data,
                        imageIsUpdated: true})
          this.updateUser(this);

        });
    }
    updateUser(e){
        axios.put('/users/edit/' + e.state.user._id,
        querystring.stringify({
            _id: e.state.user._id,
            first_name: e.state.first_name,
            last_name: e.state.last_name,
            phone: e.state.phone,
            skype: e.state.skype,
            role: e.state.whatIdo,
            image: e.state.image,
            imageIsUpdated: e.state.imageIsUpdated
        }), {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).then(function (response) {
            
            e.setState({
                messageFromServer: response.data,
                modalIsOpen: false
            });

        });
        console.log('updating user');
    }
    
    handleTextChange(e) {
        if (e.target.name == "first_name") {
            this.setState({
                first_name: e.target.value
            });
        }
       
        if (e.target.name == "email") {
            
        let validEmail = e.target.value;
            if (validator.validate(validEmail)) {
                this.setState({
                    email: e.target.value,
                    validEmail: true
                });
            } else {
                this.setState({
                    validEmail: false
                });
            }

        }
        if (e.target.name == "last_name") {
            this.setState({
                last_name: e.target.value
            });
        }
        if (e.target.name == "skype") {
            this.setState({
                skype: e.target.value
            });
        }
        if (e.target.name == "phone") {
            this.setState({
                phone: e.target.value
            });
        }
        if (e.target.name == "whatIdo") {
            this.setState({
                whatIdo: e.target.value
            });
        }

    }
   
    render() {
       
        return (
              <div>
              {this.state.messageFromServer == '' ?
              <Modal
                  isOpen={this.state.modalIsOpen}
                  onAfterOpen={this.afterOpenModal}
                  onRequestClose={this.props.handleCloseModalClick}
                  style={customStyles}
                  contentLabel="Example Modal"
                  ariaHideApp={false}
              >


                  <i className="fa fa-close" style={{ float: 'right' }} onClick={this.props.handleCloseModalClick}></i>
                  <h2 style={{ textAlign: 'center' }}>
                     <span className="fa fa-pencil" 
                            style={{ textAlign: 'center', marginRight: '10px' }} ></span> Edit your Profile</h2>
                  <div className="ln_solid"></div>
                  <form onSubmit={this.handleEditProfileClick} id="demo-form2" data-parsley-validate className="form-horizontal form-label-left">

 <div className="profile_img" style={{marginBottom: '2%'}}>
                      <div id="crop-avatar">
                        <img className="img-responsive avatar-view"
                             src={this.state.imageToDisplay}
                             alt="Avatar" 
                             title="Change the avatar"
                             style={{borderRadius: '50%', width: '25%', margin: '0 auto'}}
                             onClick={() =>this.fileInput.click()} />
                        <input style={{ display: 'none'}} 
                               type="file" 
                               onChange={this.handleFileInputChange}
                               ref={fileInput => this.fileInput= fileInput} />
                      </div>
                    </div>
                      <div className="form-group">
                          <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="first-name" style={{textAlign: 'center'}}>First Name <span className="required">*</span>
                          </label>
                          <div className="col-md-5 col-sm-5 col-xs-12">
                              <input type="text" id="first-name" name="first_name" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.user.first_name}/>
                          </div>
                      </div>
                      <div className="form-group">
                          <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="last-name" style={{textAlign: 'center'}}>Last Name <span className="required">*</span>
                          </label>
                          <div className="col-md-5 col-sm-5 col-xs-12">
                              <input type="text" id="last-name" name="last_name" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.user.last_name} />
                          </div>
                      </div>
                      <div className="form-group">
                          <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="whatIdo" style={{textAlign: 'center'}}>What I do <span className="required">*</span>
                          </label>
                          <div className="col-md-5 col-sm-5 col-xs-12">
                              <input type="text" id="whatIdo" name="whatIdo" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} placeholder='ex - Web Developer' defaultValue={this.state.user.role} />
                          </div>
                      </div>
                      <div className="form-group">
                          <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="phone" style={{textAlign: 'center'}}>Phone number <span className="required">*</span>
                          </label>
                          <div className="col-md-5 col-sm-5 col-xs-12">
                              <input type="number" id="phone" name="phone" required="required" className="form-control col-md-7 col-xs-12" onChange={this.handleTextChange} defaultValue={this.state.user.phone}/>
                          </div>
                      </div>
                      <div className="form-group">
                          <label className="control-label col-md-3 col-sm-3 col-xs-12" htmlFor="skype" style={{textAlign: 'center'}}>Skype <span className="required">*</span>
                          </label>
                          <div className="col-md-5 col-sm-5 col-xs-12">
                              <input type="text"
                                     id="skype"
                                     name="skype"
                                     required="required" 
                                     className="form-control col-md-7 col-xs-12" 
                                     onChange={this.handleTextChange} 
                                     placeholder="your pseudo" 
                                     defaultValue={this.state.user.skype}/>
                          </div>
                      </div>
                     
                     

                  
                      <div className="ln_solid"></div>
                      <div className="form-group" style={{ textAlign: 'center' }}>
                          <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                              <button className="btn btn-primary" type="reset">Reset</button>
                              <button type="submit" className="btn btn-success">Save Changes</button>
                          </div>
                      </div>

                  </form>

              </Modal>
              :
              <div className="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong> {this.state.messageFromServer}</strong>
              </div>
          }
          </div>

          
        )
    }
}


export default withRouter(EditProfileComponent);