import React from 'react';


import axios from 'axios';
import { LoadScript } from '../../common/loadScript';
import Modal from 'react-modal';
import { withRouter, Redirect } from 'react-router';




class TopNaviguation extends React.Component {
  constructor() {
    super();
    this.state = {
      user: '',
      notification: true,
      channelHistory: [],
      expanded: false,
      messagesToShow: 5,
      usersFromSlack: [],
      slackIds: [],
      newMessages: ''
      
    }

    this.logOut = this.logOut.bind(this);
    this.notifRemind = this.notifRemind.bind(this);
    this.getTeamBoardChannelHistory = this.getTeamBoardChannelHistory.bind(this);
    this.showMore = this.showMore.bind(this);
    this.convertTimeStampToDate = this.convertTimeStampToDate.bind(this);
    this.getSelectedUser = this.getSelectedUser.bind(this);
    this.getAllUsersByIds = this.getAllUsersByIds.bind(this);

  }

  convertTimeStampToDate(ts) {

    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(ts * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
    
  }

  showMore() {
    this.state.messagesToShow === 5 ? (
      this.setState({ messagesToShow: this.state.channelHistory.length, expanded: true })
      ) : (
        this.setState({ messagesToShow: 5, expanded: false })
      )
    }
  componentWillMount(){
    var userObject = JSON.parse(localStorage.getItem('token'));
    if(userObject != null){
      this.setState({user: userObject.user});
    }
    this.getTeamBoardChannelHistory(this);
  

  }
  componentDidMount() {
    let slackIDs =[];
    LoadScript('../../assets/jquery/dist/jquery.min.js');
    LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
    // LoadScript('../../assets/fastclick/lib/fastclick.js');
    // LoadScript('../../assets/nprogress/nprogress.js');
    // LoadScript('../../assets/build/js/custom.min.js');
   this.notifRemind(this);
      // console.log('test seected user',this.getSelectedUser("U9BU2GRNZ").then((user)=> user.real_name));
   
    setTimeout(() => {
      console.log('final state', this.state);
      for (let index = 0; index < this.state.channelHistory.length; index++) {
       slackIDs.push(this.state.channelHistory[index].user);
        
      };
    }, 1000);
    setTimeout(() => {
      this.setState({slackIds: slackIDs});
      console.log('slack idsss:', this.state.slackIds)
      this.getAllUsersByIds(this);
    }, 1500);
    setTimeout(() => {
      console.log('state userfrom slaack', this.state.usersFromSlack);
      console.log('state userfrom slaack reversed', this.state.usersFromSlack);
      
    }, 2000);

    
  }


  getAllUsersByIds(ev){
    for (let index = 0; index < this.state.slackIds.length; index++) {
     this.getSelectedUser(this.state.slackIds[index]);
      
    }
  }



  // getSelectedUser(userId) {
  //   // getting user from id passed as parameter
  //   let outside = this;
  //   let result ;
  //  return  axios.get('/users/bySlackId/' + userId)
  //     .then(function (response) {
  //      result = response.data.real_name;
  //      return response.data;
       

  //     }).then(result => { console.log("this is the rssjsfdfhdkjfjdhsfdshf",result); return result; });
      
  // }
  getSelectedUser(userId) {
    // getting user from id passed as parameter
    let outside = this;
    let result ;
     axios.get('/users/bySlackId/' + userId)
      .then(function (response) {
       result = response.data.real_name;
       outside.state.usersFromSlack.push(response.data);
      //  return response.data;
       

      });
      
  }
  getTeamBoardChannelHistory(ev){
    axios.get('https://slack.com/api/channels.history?token=xoxp-12121543780-317945634931-347418183556-717485855f9646314262b69194d601fd&channel=C9CSTNHP1&pretty=1')
    .then(function (response) {
        console.log('response is', response);
        if((response.data.messages.length - ev.state.channelHistory.length) > 0){
          ev.setState({ channelHistory: response.data.messages,
                        newMessages: (response.data.messages.length - ev.state.channelHistory.length) });

        }
        else{
          ev.setState({ channelHistory: response.data.messages});
        }
         

    }); 
  }

  notifRemind(ev){
    setTimeout(() => {
      for (let index = 0; index < 5; index++) {
        setTimeout(() => {
          ev.setState({notification: !this.state.notification});
          console.log('blinkingggggggg', this.state.notification)
          
        }, 1000);
    }
    }, 500);
  }
  logOut() {
    localStorage.removeItem('token');

    window.location.reload();
  }
  openUserProfile(slackUser) {
    // console.log('user is', slackUser);
    this.props.history.push('/profile/' + slackUser.id);
    // console.log('user is', this.props.history);

}

  render() {
    
    return (
      <div className="top_nav">
        <div className="nav_menu ">
          <nav>
            <div className="nav toggle">
              <a id="menu_toggle"><i className="fa fa-bars"></i></a>
            </div>

            <ul className="nav navbar-nav navbar-right">
              <li className="">
                <a href="javascript:;" className="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style={{ float: 'right' }}>
                  <img src={this.state.user.imageIsUpdated =="true"?'./uploads/'+this.state.user.image_192:this.state.user.image_192} alt="" />{this.state.user.name != null && this.state.user.name}
                      <span className=" fa fa-angle-down"></span>
                </a>
                <ul className="dropdown-menu dropdown-usermenu pull-right">
    {this.state.user != null && <li><a href="javascript:;"  onClick={() => this.openUserProfile(this.state.user)}><i className="fa fa-edit pull-right"></i>  My Profile</a></li>}
      <li><a href="#mySettings"><i className="fa fa-cogs pull-right"></i> MySettings</a></li>
                  <li><a href="javascript:;"><i className="fa fa-info pull-right"></i> Help</a></li>
                  <li><a href="#login" onClick={this.logOut}><i className="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
              </li>

              <li role="presentation" className="dropdown">
                <a href="javascript:;" className="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i className="fa fa-envelope-o"></i>
                  <span className="badge bg-green">{this.state.newMessages}</span>
                </a>
                <ul id="menu1" className="dropdown-menu list-unstyled msg_list" role="menu">
                {this.state.channelHistory.slice(0, this.state.messagesToShow).map((message, index) => {
                                            return (
                 <li key={index}>
                    <a>
                                            <span className="image">{this.state.usersFromSlack[index] && <img src={this.state.usersFromSlack[index].imageIsUpdated =="true"?'./uploads/'+this.state.usersFromSlack[index].image_192:this.state.usersFromSlack[index].image_192}  alt="Profile Image" />}</span>
                      <span>
                      {/* {this.getSelectedUser(message.user)} */}
                        <span>{this.state.usersFromSlack[index] &&  this.state.usersFromSlack[index].real_name}</span>
                        <span className="time">{this.convertTimeStampToDate(parseFloat(message.ts))}</span>
                      </span>
                      <span className="message">
                        {message.text}
                          </span>
                    </a>
                  </li>
                    )

                  })


                  }
                
                
                
                  <li>
                  <div className="notes_more_buttons" style={{ borderTop: '1px solid rgb(221, 221, 221)', textAlign: 'center', paddingTop: '5px', width:'40%', margin: '0 auto' }}>
                                    <a type="button" className="btn btn-default btn-xs show_more_notes" onClick={this.showMore}>
                                        {this.state.expanded ? (
                                            <span>Show less</span>
                                        ) : (
                                                <span>Show more</span>
                                            )
                                        }
                                    </a>
                                </div>
                  </li>
                  <li>
                    <div className="text-center">
                      <a href="https://mobelite-team.slack.com/messages/" target="_blank">
                        <strong>See All messages  </strong>
                        <i className="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>

                </ul>
              </li>
            {this.state.notification && <h1 style={{color: 'red'}}>Please log your tasks :)</h1>}  
            </ul>
          </nav>
        </div>
      </div>

    )
  }
}
export default withRouter(TopNaviguation);










































