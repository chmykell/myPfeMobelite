import React from 'react';
import Modal from 'react-modal';

const customStyles = {
    content: {
        top: '51%',
        left: '50%',
        right: 'auto',
        width: '60%',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: '1px solid rgb(28, 185, 232)',
        borderRadius: 26
    }
};

class DeleteAlert extends React.Component {
   
    constructor(){
        super();
        this.state = {
            modalIsOpen: false
        };


        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            messageFromServer: ''
        });
    }

      render(){
        return (
            <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Example Modal"
            ariaHideApp={false}
        >


            <i className="fa fa-close" style={{ float: 'right' }} onClick={this.closeModal}></i>
            <h3>Do you want to delete this record ??</h3>

             <div className="ln_solid"></div>
                <div className="form-group" style={{ textAlign: 'center' }}>
                    <div className="col-md-7 col-sm-6 col-xs-12 col-md-offset-3">

                        <button className="btn btn-primary" type="reset">Confirm</button>
                        <button  className="btn btn-success">Cancel</button>
                    </div>
                </div>

        </Modal>
        )
    }
}


export default DeleteAlert;