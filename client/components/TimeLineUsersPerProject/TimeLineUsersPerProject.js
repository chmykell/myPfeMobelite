import React from 'react';
import axios from 'axios';





class TimeLineUsersPerProject extends React.Component {


    render() {
        return (
       
            <div class="gridcolumn no-padding">

                    <div class="dategrid">
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-17">
      <div class="datelabel">
        <div>
          <div><span>Fri</span> <span>17 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small weekend" style={{width:'6.666666666666667%'}} rel="2015-07-18">
      <div class="datelabel">
        <div>
          <div><span>Sat</span> <span>18 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small weekend" style={{width:'6.666666666666667%'}} rel="2015-07-19">
      <div class="datelabel">
        <div>
          <div><span>Sun</span> <span>19 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-20">
      <div class="datelabel">
        <div>
          <div><span>Mon</span> <span>20 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-21">
      <div class="datelabel">
        <div>
          <div><span>Tue</span> <span>21 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-22">
      <div class="datelabel">
        <div>
          <div><span>Wed</span> <span>22 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-23">
      <div class="datelabel">
        <div>
          <div><span>Thu</span> <span>23 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-24">
      <div class="datelabel">
        <div>
          <div><span>Fri</span> <span>24 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small weekend" style={{width:'6.666666666666667%'}} rel="2015-07-25">
      <div class="datelabel">
        <div>
          <div><span>Sat</span> <span>25 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small weekend" style={{width:'6.666666666666667%'}} rel="2015-07-26">
      <div class="datelabel">
        <div>
          <div><span>Sun</span> <span>26 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-27">
      <div class="datelabel">
        <div>
          <div><span>Mon</span> <span>27 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-28">
      <div class="datelabel">
        <div>
          <div><span>Tue</span> <span>28 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-29">
      <div class="datelabel">
        <div>
          <div><span>Wed</span> <span>29 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-30">
      <div class="datelabel">
        <div>
          <div><span>Thu</span> <span>30 Jul</span></div>
        </div>
      </div>
    </div>
    
    <div class="marker small" style={{width:'6.666666666666667%'}} rel="2015-07-31">
      <div class="datelabel">
        <div>
          <div><span>Fri</span> <span>31 Jul</span></div>
        </div>
      </div>
    </div>
    
  </div>
                      <div class="row show-grid">
    <div class="timeblock allocation_full projectblock" data-date="July 17, 2015" style={{width: '6.666666666666667%', marginLeft: '0%', borderBottomColor: '#3498db'}}>
        <div class="day_block" data-avatar="/images/users/john.png" style={{width:'100%'}}>
        </div>
      <span class="small timeblock_label">
          <i class="fa fa-clock-o"></i> 8h
      </span>
    </div>
    <div class="timeblock allocation_full projectblock" data-date="July 27, 2015" style={{width: '33.33333333333333%', marginLeft: '13.333333333333334%', borderBottomColor: '#3498db'}}>    
        <div class="day_block" data-avatar="/images/users/john.png" style={{width: '100%'}}>
        </div>
      <span class="small timeblock_label">
          <i class="fa fa-clock-o"></i> 8h
      </span>
    </div>
  </div>
                      <div class="row show-grid">
    <div class="timeblock allocation_full projectblock" data-date="July 23, 2015" style={{width: '13.333333333333334%', marginLeft: '40%', borderBottomColor: '#34495e'}}>
        <div class="day_block" data-avatar="/images/users/pcardoso.png" style={{width: '50%'}}>
        </div>
        <div class="day_block" data-avatar="/images/users/pcardoso.png" style={{width: '50%'}}>
        </div>
      <span class="small timeblock_label">
          <i class="fa fa-clock-o"></i> 8h
      </span>
    </div>
    <div class="timeblock allocation_full projectblock" data-date="July 27, 2015" style={{width: '33.33333333333333%', marginLeft: '13.333333333333334%', borderBottomColor: '#34495e'}}>
        <div class="day_block" data-avatar="/images/users/pcardoso.png" style={{width: '100%'}}>   
        </div>
        <div class="day_block" data-avatar="/images/users/pcardoso.png" style={{width: '100%'}}>      
        </div>
       <span class="small timeblock_label">
          <i class="fa fa-clock-o"></i> 8h
      </span>
    </div>
  </div>
                               </div>

        )
    }
}


export default TimeLineUsersPerProject;