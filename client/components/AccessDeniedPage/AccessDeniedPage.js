import React from 'react';



class AccessDeniedPage extends React.Component {
    constructor(){
        super();
        
       
      }

      render(){
        return (
            <div className="col-md-12">
            <div className="col-middle">
              <div className="text-center text-center">
                <h1 className="error-number">403</h1>
                <h2>Access denied</h2>
                <p>Full authentication is required to access this resource. <a href="#">Report this?</a>
                </p>
                <div className="mid_center">
                  <h3>Search</h3>
                  <form>
                    <div className="col-xs-12 form-group pull-right top_search">
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="Search for..."/>
                        <span className="input-group-btn">
                                <button className="btn btn-default" type="button">Go!</button>
                            </span>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        )
    }
}


export default AccessDeniedPage;