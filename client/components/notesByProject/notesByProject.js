import React from 'react';

import axios from 'axios';
import { FormatDate } from '../../common/formatIsoStringDateToDate'
class NotesByProject extends React.Component {
    constructor() {
        super();
        this.state = {
            NotesByProject: [],
            notesToShow: 3,
            expanded: false
        };
        this.showMore = this.showMore.bind(this);
        this.getNotesByDate = this.getNotesByDate.bind(this);


    }
    showMore() {
        this.state.notesToShow === 3 ? (
          this.setState({ notesToShow: this.state.NotesByProject.length, expanded: true })
          ) : (
            this.setState({ notesToShow: 3, expanded: false })
          )
        }
    componentWillMount() {
        this.getNotes(this);
    }
    componentDidMount() {
        console.log('notes data', this.state.NotesByProject);
    } 
    componentWillReceiveProps(nextProps) {
        if(this.props.selectedDate.start != nextProps.selectedDate.start && this.props.selectedDate.end != nextProps.selectedDate.end) // Check if it's a new user, you can also use some unique, like the ID
        {
            this.getNotesByDate(this);
        }
    }
    getNotesByDate(ev){
       
        axios.get('/notes/' +this.props.selectedDate.start +'/'+ this.props.selectedDate.end)
        .then(function (response) {
            console.log('response', response);
            ev.setState({ NotesByProject: response.data });

        });
    }
    getNotes(ev) {
        axios.get('/notes')
            .then(function (response) {
                console.log('response', response);
                ev.setState({ NotesByProject: response.data });

            });


    }

    render() {
        return (
            <div className="col-md-3 col-sm-12 col-xs-12">
                <div className="x_panel">
                    <div className="x_title">
                        {/* <button type="button" className="btn btn-info btn-sm">Add User </button> */}
                        <span>Notes</span>
                        <ul className="nav navbar-right panel_toolbox">
                            <li><a className="collapse-link"><i className="fa fa-chevron-up"></i></a>
                            </li>
                            {/* <li className="dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i className="fa fa-wrench"></i></a>
                            <ul className="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li> */}
                            <li><a className="close-link"><i className="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                    <div className="x_content">
                        <div>
                            {
                                this.state.NotesByProject.slice(0, this.state.notesToShow).map((note) => {

                                    return (

                                        <div className="notes_day" key={note._id}>
                                            <small className="pull-right text-muted" style={{ fontWeight: 'bold' }}>{FormatDate(note.createdAt)}</small>
                                            <i className="fa fa-folder-o"></i> <a style={{color: '#20bd9e'}}>{note.project.projectName}</a><br />
                                            <div style={{ marginLeft: '20px' }}>
                                                <b>{note.user.first_name}:</b> {note.noteMessage}
                                            </div>
                                        </div>


                                    )
                                })
                            }


                            <div className="notes_more_buttons" style={{ borderTop: '1px solid rgb(221, 221, 221)', textAlign: 'center', paddingTop: '5px' }}>
                            <a type="button" className="btn btn-default btn-xs show_more_notes" onClick={this.showMore}>
                                {this.state.expanded ? (
                                    <span>Show less</span>
                                ) : (
                                        <span>Show more</span>
                                    )
                                }
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}


export default NotesByProject;