import React from 'react';

import {LoadScript} from '../../common/loadScript';

class MenuOptions extends React.Component {
    constructor() {
        super();
        // this.state = { selectedDate: '' };

    }
    componentDidMount() {
        LoadScript('../../assets/jquery/dist/jquery.min.js');
        LoadScript('../../assets/bootstrap/dist/js/bootstrap.min.js');
    }
   

    render() {
        return (
            <div style={{position:'absolute', display:'inline-block'}} class="">
             <ul class="dropdown-menu" style={{marginLeft: '-160px'}}>
                <li><a href="#" className="action_edit" rel="5628f476e4b099d4b9d90163"><i className="fa fa-fw fa-pencil" style={{marginRight: '10px'}} aria-hidden="true"></i>Edit</a></li>
                <li><a href="#" className="action_merge" rel="5628f476e4b099d4b9d90163"><i className="fa fa-fw fa-code-fork" style={{marginRight: '10px'}}></i>Merge</a></li>
                <li><a href="#" className="action_archive" rel="5628f476e4b099d4b9d90163"><i className="fa fa-fw fa-archive" style={{marginRight: '10px'}}></i>View</a></li>
                <li><a href="#" className="action_delete" rel="5628f476e4b099d4b9d90163"><i className="fa fa-fw fa-trash-o" style={{marginRight: '10px'}}></i>Delete</a></li>
            </ul>
            <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i></button>
           
        </div>
        )
    }
}


export default MenuOptions;