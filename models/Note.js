var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var noteSchema = new Schema({
 user: { type: Schema.Types.ObjectId, ref: 'User' },
 createdAt: Date,
 noteMessage: String,
 project: { type: Schema.Types.ObjectId, ref: 'Project' },

});
module.exports = mongoose.model('Note', noteSchema); 