var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var projectSchema = new Schema({
  projectName: String,
  loggedHours: Number,
  timeOffHours: Number,
  //team: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  type: String,
  client: { type: Schema.Types.ObjectId, ref: 'Client' },
  createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: Date,
  archivedAt: Date,
  upDatedAt: Date,
  startDate: Date,
  estimatedDuration: Number,
  notes: [{ type: Schema.Types.ObjectId, ref: 'Note' }],
  projectStatus: Boolean,
  color: String
 
});
module.exports = mongoose.model('Project', projectSchema);