var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var taskSchema = new Schema({
  project: { type: Schema.Types.ObjectId, ref: 'Project' },
  createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: Date,
  archivedAt: Date,
  upDatedAt: Date,
  startDate: Object,
  // startDate: {
  //   date: Number,
  //   hours: Number,
  //   minutes: Number,
  //   months: Number,
  //   years: Number
  // },
  hours: Number,
  notes: { type: Schema.Types.ObjectId, ref: 'Note' }
  
});
module.exports = mongoose.model('Task', taskSchema);