var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var clientSchema = new Schema({
 firstName: String,
 lastName: String,
 email: String,
 phoneNumber: String,
 country: String,
 createdAt: Date

});
module.exports = mongoose.model('Client', clientSchema);