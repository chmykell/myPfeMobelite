var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
  slack_id: String,
  first_name: String,
  last_name: String,
  real_name: String,
  email: String,
  role: String,
  is_admin: String,
  loggedHours: Number,
  image_192: String,
  phone: String,
  skype: String,
  projects: [{ type: Schema.Types.ObjectId, ref: 'Project' }],
  offWorkHours: Number,
  createdAt: Date,
  title: String,
  team_id: String,
  tz: String,
  tz_label: String,
  tz_offset: String,
  password: String,
  token: String,
  imageIsUpdated: String
});
module.exports = mongoose.model('User', userSchema);

